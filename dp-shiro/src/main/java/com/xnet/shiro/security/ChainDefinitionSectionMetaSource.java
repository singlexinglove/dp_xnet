package com.xnet.shiro.security;

import com.xnet.shiro.entity.authorize.ModuleEntity;
import com.xnet.shiro.manager.authorize.ModuleManager;
import com.xnet.common.entity.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.config.Ini;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 产生责任链，确定每个url的访问权限
 *
 * @author Xingyc
 * @date 2018-1-11 11:59:58
 */
public class ChainDefinitionSectionMetaSource implements FactoryBean<Ini.Section> {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ModuleManager moduleManager;
	
	// 静态资源访问权限
	private String filterChainDefinitions = null;

	public Ini.Section getObject() throws Exception {
		Ini ini = new Ini();
		// 加载默认的url
		ini.load(filterChainDefinitions);
		Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);
		List<ModuleEntity> lists = moduleManager.listMenu(new Query());
		for(ModuleEntity menu : lists) {
			String permKey = menu.getModuleId();
			String permUrl = menu.getUrlAddress();
			if(StringUtils.isNotEmpty(permKey) && StringUtils.isNotEmpty(permUrl)) {
				String permission = "perms[" + permKey + "]";
				section.put(permUrl + "", permission);
				log.info("初始化权限:{}={}", permUrl, permission);
			}
		}
		// 所有资源的访问权限，必须放在最后
		section.put("/**", "authc");
		return section;
	}

	/**
	 * 通过filterChainDefinitions对默认的url过滤定义
	 * @param filterChainDefinitions
	 */
	public void setFilterChainDefinitions(String filterChainDefinitions) {
		this.filterChainDefinitions = filterChainDefinitions;
	}

	public Class<?> getObjectType() {
		return this.getClass();
	}

	public boolean isSingleton() {
		return false;
	}
}
