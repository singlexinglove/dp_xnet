package com.xnet.shiro.controller.base;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.CodeRuleEntity;
import com.xnet.shiro.service.base.CodeRuleService;

/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
@RestController
@RequestMapping("/basemanage/codeRule")
public class CodeRuleController extends AbstractController {
	
	@Autowired
	private CodeRuleService codeRuleService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<CodeRuleEntity> list(@RequestBody Map<String, Object> params) {
		return codeRuleService.listCodeRule(params);
	}
		
	/**
	 * 新增
	 * @param codeRule
	 * @return
	 */
	@SysLog("新增编号规则表")
	@RequestMapping("/save")
	public R save(@RequestBody CodeRuleEntity codeRule) {
		return codeRuleService.saveCodeRule(codeRule);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody String id) {
		return codeRuleService.getCodeRuleById(id);
	}
	
	/**
	 * 修改
	 * @param codeRule
	 * @return
	 */
	@SysLog("修改编号规则表")
	@RequestMapping("/update")
	public R update(@RequestBody CodeRuleEntity codeRule) {
		return codeRuleService.updateCodeRule(codeRule);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除编号规则表")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody String[] id) {
		return codeRuleService.batchRemove(id);
	}
	
}
