package com.xnet.shiro.controller.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.xnet.common.entity.TreeGridEntity;
import com.xnet.common.utils.JSONUtils;
import com.xnet.common.utils.TreeGridJson;
import org.apache.shiro.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.DataItemDetailEntity;
import com.xnet.shiro.service.base.DataItemDetailService;

/**
 * 数据字典明细控制器
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月08日 下午1:49:58
 */
@RestController
@RequestMapping("/basemanage/dataItemDetail")
public class DataItemDetailController extends AbstractController {
	
	@Autowired
	private DataItemDetailService dataItemDetailService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<DataItemDetailEntity> list(@RequestBody Map<String, Object> params) {
		return dataItemDetailService.listDataItemDetail(params);
	}
		
	/**
	 * 新增
	 * @param dataItemDetail
	 * @return
	 */
	@SysLog("新增")
	@RequestMapping("/save")
	public R save(@RequestBody DataItemDetailEntity dataItemDetail) {
		return dataItemDetailService.saveDataItemDetail(dataItemDetail);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody String id) {
		return dataItemDetailService.getDataItemDetailById(id);
	}

	/**
	 * 根据字典类型获取对应的明细列表
	 * @param itemId
	 * @return
	 */
	private  List<DataItemDetailEntity> getListById(String itemId)
	{
		return dataItemDetailService.getListByItemId(itemId);
	}
	
	/**
	 * 修改
	 * @param dataItemDetail
	 * @return
	 */
	@SysLog("修改")
	@RequestMapping("/update")
	public R update(@RequestBody DataItemDetailEntity dataItemDetail) {
		return dataItemDetailService.updateDataItemDetail(dataItemDetail);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody String[] id) {
		return dataItemDetailService.batchRemove(id);
	}

	/**
	 * 获取字典明细列表
	 * @param enCode
	 * @return
	 */
	@RequestMapping("/itemList")
	public List<DataItemDetailEntity> getDataItemListJson(String enCode)
	{
       return dataItemDetailService.getDataItemList(enCode);
	}

	@RequestMapping("/treeList")
	public Object getTreeListJson(@RequestBody Map<String, Object> params) {
		if(params.isEmpty()) return StringUtils.EMPTY_STRING;
		String itemId = params.get("itemId")==null?null:params.get("itemId").toString();
		if(org.apache.commons.lang.StringUtils.isBlank(itemId)) return StringUtils.EMPTY_STRING;
		String condition= params.get("condition")==null?null:params.get("condition").toString();
		String keyword= params.get("keyword")==null?null:params.get("keyword").toString();
		List<DataItemDetailEntity> dataItemDetailEntityList= getListById(itemId);
		List<TreeGridEntity> treeList = new ArrayList<>() ;
		for (DataItemDetailEntity item : dataItemDetailEntityList)
		{
			TreeGridEntity tree = new TreeGridEntity();
			String itemDetailId=item.getItemDetailId();
			boolean hasChildren = dataItemDetailEntityList.stream().anyMatch(x->x.getParentId().equals(itemDetailId));
			tree.setId(itemDetailId);
			tree.setHasChildren(hasChildren);
			tree.setParentId(item.getParentId());
			tree.setExpanded(true);
			tree.setEntityJson(JSONUtils.beanToJson(item,"yyyy-MM-dd") );
			treeList.add(tree);
		}
		String jsonText= TreeGridJson.TreeJson(treeList);
		return  JSONObject.parse(jsonText);

	}
}
