package com.xnet.shiro.controller.authorize;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.xnet.common.entity.TreeEntity;
import com.xnet.common.entity.TreeGridEntity;
import com.xnet.common.utils.JSONUtils;
import com.xnet.common.utils.TreeGridJson;
import com.xnet.common.utils.TreeNodeUtils;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.authorize.ModuleEntity;
import com.xnet.shiro.service.authorize.ModuleService;

/**
 * 系统功能表
 *
 * @author xyc
 * @email
 * @url http://www.abcd.com
 * @date 2018年9月07日 上午10:31:35
 */
@RestController
@RequestMapping("/sys/menu")
public class ModuleController extends AbstractController {

    @Autowired
    private ModuleService moduleService;

    //region 数据查询

    /**
     * 用户菜单
     *
     * @return
     */
    @RequestMapping("/user")
    public R user() {

        return moduleService.listUserMenu(getUserId());
    }


    /**
     * 列表
     *
     * @param params
     * @return
     */
    @RequestMapping("/list")
    public Page<ModuleEntity> list(@RequestBody Map<String, Object> params) {

        return moduleService.listModule(params);
    }

    /**
     * 树形列表
     *
     * @return
     */
    @RequestMapping("/listTree")
    public List<TreeEntity> listTree() {

        List<ModuleEntity> data = moduleService.listAllModule();
        List<TreeEntity> treeList = new ArrayList<>();
        for (ModuleEntity item : data) {
            TreeEntity tree = new TreeEntity();
            boolean hasChildren = data.stream().anyMatch(x -> x.getParentId().equals(item.getModuleId()));
            tree.setId(item.getModuleId());
            tree.setText(item.getFullName());
            tree.setValue(item.getModuleId());
            tree.setIsExpand(true);
            tree.setComplete(true);
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            tree.setImg(item.getIcon());
            treeList.add(tree);
        }
        //创建结果列表
        List<TreeEntity> newTreeList = new ArrayList<>();
        TreeNodeUtils.convertToTree(treeList, "0", newTreeList);
        return newTreeList;

    }

    /**
     * 根据id查询详情
     *
     * @param id
     * @return
     */
    @RequestMapping("/info")
    public R getById(@RequestBody String id) {

        return moduleService.getModuleById(id);
    }

    //endregion


    //region 数据操作

    /**
     * 新增保存
     *
     * @param map
     * @return
     */
    @SysLog("新增功能菜单")
    @RequestMapping("/save")
    public R save(@RequestBody Map<String,Object> map) {
        String moduleJson=map.get("module").toString();
        ModuleEntity module=new ModuleEntity();
        module=(ModuleEntity)JSONUtils.jsonToBean(moduleJson,module);
        //增加创建人id及默认可用和非删除标记
        module.setCreateUserId(getUserId());
        module.setCreateUserName(getUserAccount());
        module.setEnabledMark(1);
        module.setDeleteMark(0);
        String  moduleButtonListJson=map.get("moduleButtonListJson").toString();
        String  moduleColumnListJson=map.get("moduleColumnListJson").toString();
        List<ModuleButtonEntity> moduleButtonList = (List<ModuleButtonEntity>) JSONUtils.jsonToArray(moduleButtonListJson, ModuleButtonEntity.class);
        List<ModuleColumnEntity> moduleColumnList = (List<ModuleColumnEntity>) JSONUtils.jsonToArray(moduleColumnListJson, ModuleColumnEntity.class);
        return moduleService.saveModule(module, moduleButtonList, moduleColumnList);
    }

    /**
     * 修改保存
     *
     * @param map
     * @return
     */
    @SysLog("修改功能菜单")
    @RequestMapping("/update")
    public R update(@RequestBody Map<String,Object> map) {

        String moduleJson=map.get("module").toString();
        ModuleEntity module=new ModuleEntity();
        module=(ModuleEntity)JSONUtils.jsonToBean(moduleJson,module);
        //增加修改人id
        module.setModifyUserId(getUserId());
        module.setModifyUserName(getUserAccount());
        String  moduleButtonListJson=map.get("moduleButtonListJson").toString();
        String  moduleColumnListJson=map.get("moduleColumnListJson").toString();
        List<ModuleButtonEntity> moduleButtonList = (List<ModuleButtonEntity>) JSONUtils.jsonToArray(moduleButtonListJson, ModuleButtonEntity.class);
        List<ModuleColumnEntity> moduleColumnList = (List<ModuleColumnEntity>) JSONUtils.jsonToArray(moduleColumnListJson, ModuleColumnEntity.class);
        return moduleService.saveModule(module, moduleButtonList, moduleColumnList);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @SysLog("删除功能菜单")
    @RequestMapping("/remove")
    public R batchRemove(@RequestBody String[] id) {

        return moduleService.batchRemove(id);
    }
    //endregion
}
