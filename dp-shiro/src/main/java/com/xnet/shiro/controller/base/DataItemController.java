package com.xnet.shiro.controller.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import com.xnet.common.entity.TreeEntity;
import com.xnet.common.entity.TreeGridEntity;
import com.xnet.common.utils.JSONUtils;
import com.xnet.common.utils.TreeGridJson;
import com.xnet.common.utils.TreeNodeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.DataItemEntity;
import com.xnet.shiro.service.base.DataItemService;

/**
 * 数据字典分类表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月13日 下午3:14:19
 */
@RestController
@RequestMapping("/basemanage/dataItem")
public class DataItemController extends AbstractController {
	
	@Autowired
	private DataItemService dataItemService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<DataItemEntity> list(@RequestBody Map<String, Object> params) {
		return dataItemService.listDataItem(params);
	}

	/**
	 * 加载所有列表
	 * @return
	 */
	private  List<DataItemEntity> listAll()
	{
		return dataItemService.listAll();
	}

	/**
	 * 获取组合树形列表
	 * @param value
	 * @return
	 */
	@RequestMapping("/treelist")
	public List<TreeEntity> treelist(String value) {
		List<DataItemEntity> allList=listAll();
		List<DataItemEntity> filterList = new ArrayList<>();
		if(!StringUtils.isBlank(value))
		{
			filterList=allList.stream().filter(x->x.getItemName().contains(value)).collect(Collectors.toList());
		}else {
			filterList = allList;
		}
		List<TreeEntity> treeList = convertToTreeList(filterList);
		List<TreeEntity> relationTreeList = new ArrayList<>();
		if (!StringUtils.isBlank(value)) {
			List<TreeEntity> allTreeList = convertToTreeList(allList);//获取所有符合条件的树形列表
			TreeNodeUtils.getWholeTreeChain(allTreeList, treeList, relationTreeList);
		} else {
			relationTreeList = treeList;
		}
		List<TreeEntity> newTreeList = new ArrayList<>();
		TreeNodeUtils.convertToTree(relationTreeList, "0", newTreeList);
		return newTreeList;
	}
	/**
	 * 转换实体为树形结构
	 *
	 * @param itemList
	 * @return
	 */
	private List<TreeEntity> convertToTreeList(List<DataItemEntity> itemList) {

		List<TreeEntity> treeList = new ArrayList<>();
		for (DataItemEntity item : itemList) {
			TreeEntity tree = new TreeEntity();
			String itemId=item.getItemId();
			boolean hasChildren = itemList.stream().anyMatch(x -> x.getParentId().equals(itemId));
			tree.setId(itemId);
			tree.setText(item.getItemName());
			tree.setValue(item.getItemCode());
			tree.setIsExpand(true);
			tree.setComplete(true);
			tree.setHasChildren(hasChildren);
			tree.setParentId(item.getParentId());
			tree.setAttribute("isTree");
			tree.setAttributeValue(item.getIsTree()==null?"0":item.getIsTree().toString());
			treeList.add(tree);
		}
		return treeList;
	}

	@RequestMapping("/treeGridList")
	public Object getTreeListJson(@RequestBody Map<String, Object> params) {
		String keyword= org.apache.shiro.util.StringUtils.EMPTY_STRING;
		if(params.get("keyword")!=null){
			keyword=params.get("keyword").toString();
		};
		String filterKeyword=keyword;
		List<DataItemEntity> allList=listAll();
		List<DataItemEntity> filterList = new ArrayList<>();
		if(!StringUtils.isBlank(filterKeyword))
		{
			filterList=allList.stream().filter(x->x.getItemName().contains(filterKeyword)).collect(Collectors.toList());
			//目前此处存在bug 必须将符合条件的结果向上递归找到其对应的父类，然后去重，重新转换组合

		}else {
			filterList = allList;
		}
		List<TreeGridEntity> treeList = new ArrayList<>() ;

		for(DataItemEntity item:filterList)
		{
			TreeGridEntity tree = new TreeGridEntity();
			String itemId=item.getItemId();
			boolean hasChildren = filterList.stream().anyMatch(x->x.getParentId().equals(itemId));
			tree.setId(itemId);
			tree.setHasChildren(hasChildren);
			tree.setParentId(item.getParentId());
			tree.setExpanded(true);
			tree.setEntityJson(JSONUtils.beanToJson(item,"yyyy-MM-dd"));
			treeList.add(tree);
		}
		String jsonText= TreeGridJson.TreeJson(treeList);
		return  JSONObject.parse(jsonText);
	}

	/**
	 * 新增
	 * @param dataItem
	 * @return
	 */
	@SysLog("新增数据字典分类表")
	@RequestMapping("/save")
	public R save(@RequestBody DataItemEntity dataItem) {
		return dataItemService.saveDataItem(dataItem);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody String id) {
		return dataItemService.getDataItemById(id);
	}
	
	/**
	 * 修改
	 * @param dataItem
	 * @return
	 */
	@SysLog("修改数据字典分类表")
	@RequestMapping("/update")
	public R update(@RequestBody DataItemEntity dataItem) {
		return dataItemService.updateDataItem(dataItem);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除数据字典分类表")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody String[] id) {
		return dataItemService.batchRemove(id);
	}


	
}
