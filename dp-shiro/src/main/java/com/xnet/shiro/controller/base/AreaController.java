package com.xnet.shiro.controller.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.xnet.common.entity.TreeEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.AreaEntity;
import com.xnet.shiro.service.base.AreaService;
/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
@RestController
@RequestMapping("/basemanage/area")
public class AreaController extends AbstractController {
	
	@Autowired
	private AreaService areaService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<AreaEntity> list(@RequestBody Map<String, Object> params) {
		return areaService.listArea(params);
	}
		
	/**
	 * 新增
	 * @param area
	 * @return
	 */
	@SysLog("新增行政区域表")
	@RequestMapping("/save")
	public R save(@RequestBody AreaEntity area) {
		return areaService.saveArea(area);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody String id) {
		return areaService.getAreaById(id);
	}
	
	/**
	 * 修改
	 * @param area
	 * @return
	 */
	@SysLog("修改行政区域表")
	@RequestMapping("/update")
	public R update(@RequestBody AreaEntity area) {
		return areaService.updateArea(area);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除行政区域表")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody String[] id) {
		return areaService.batchRemove(id);
	}

	/**
	 * 根据父类id获取区划列表
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/itemList")
	public List<AreaEntity> getAreaListByParentId(String parentId, String keyword)
	{
		if(StringUtils.isBlank(parentId)) {
			parentId="0";
		}
		return areaService.getAreaListByParentId(parentId,keyword);
	}

	/**
	 * 获取组合树形列表
	 * @param value
	 * @return
	 */
	@RequestMapping("/treelist")
	public List<TreeEntity> treelist(String value) {

		String parentId = value == null ? "0" : value;
		List<AreaEntity> areaEntities=getAreaListByParentId(parentId,org.apache.shiro.util.StringUtils.EMPTY_STRING);
		List<TreeEntity> treeList = new ArrayList<>();
		if(areaEntities.size()>0)
		{
			for(AreaEntity item : areaEntities)
			{
				String areaId=item.getAreaId();
				List<AreaEntity> childList=getAreaListByParentId(areaId,org.apache.shiro.util.StringUtils.EMPTY_STRING);
				boolean hasChildren =!childList.isEmpty();
				TreeEntity tree = new TreeEntity();
				tree.setId(areaId);
				tree.setText(item.getAreaName());
				tree.setValue(areaId);
				tree.setIsExpand(false);
				tree.setHasChildren(hasChildren);
				treeList.add(tree);
			}

		}
		return treeList;
	}
	
}
