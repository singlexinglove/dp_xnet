package com.xnet.shiro.controller.authorize;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.xnet.common.entity.TreeEntity;
import com.xnet.common.entity.TreeGridEntity;
import com.xnet.common.utils.JSONUtils;
import com.xnet.common.utils.TreeGridJson;
import com.xnet.common.utils.TreeNodeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.service.authorize.ModuleButtonService;

/**
 * 功能按钮表
 *
 * @author xyc
 * @email
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
@RestController
@RequestMapping("/authorizeManage/moduleButton")
public class ModuleButtonController extends AbstractController {

    @Autowired
    private ModuleButtonService moduleButtonService;

    /**
     * 列表
     *
     * @param params
     * @return
     */
    @RequestMapping("/list")
    public Page<ModuleButtonEntity> list(@RequestBody Map<String, Object> params) {

        return moduleButtonService.listModuleButton(params);
    }

    /**
     * 列表
     *
     * @param moduleId
     * @return
     */
    @RequestMapping("/listModuleId")
    public List<ModuleButtonEntity> listModuleId(String moduleId) {

        if (StringUtils.isBlank(moduleId)) {
            return new ArrayList<>();
        }
        return moduleButtonService.listModuleId(moduleId);
    }

    /**
     * 表格树列表
     *
     * @return
     */
    @RequestMapping("/listGridTree")
    public Object listGridTree(String moduleId) {

        List<ModuleButtonEntity> data = moduleButtonService.listModuleId(moduleId);

        List<TreeGridEntity> treeList = new ArrayList<>();
        for (ModuleButtonEntity item : data) {
            TreeGridEntity tree = new TreeGridEntity();
            boolean hasChildren = data.stream().anyMatch(x -> x.getParentId().equals(item.getModuleButtonId()));
            tree.setId(item.getModuleButtonId());
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            tree.setExpanded(true);
            tree.setEntityJson(JSONUtils.beanToJson(item, "yyyy-MM-dd"));
            treeList.add(tree);
        }

        String jsonText = TreeGridJson.TreeJson(treeList);
        return JSONObject.parse(jsonText);
    }

    /**
     * 将json格式化成树形json
     *
     * @return
     */
    @RequestMapping("/getListTreeGridJson")
    public Object getListTreeGridJson(String json) {

        List<TreeGridEntity> treeList = new ArrayList<>();
        if (StringUtils.isBlank(json)) return treeList;
        List<ModuleButtonEntity> resultList = new ArrayList<>();
        resultList = (List<ModuleButtonEntity>) JSONUtils.jsonToArray(json, ModuleButtonEntity.class);

        for (ModuleButtonEntity item : resultList) {
            TreeGridEntity tree = new TreeGridEntity();
            boolean hasChildren = resultList.stream().anyMatch(x -> x.getParentId().equals(item.getModuleButtonId()));
            tree.setId(item.getModuleButtonId());
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            tree.setExpanded(true);
            tree.setEntityJson(JSONUtils.beanToJson(item, "yyyy-MM-dd"));
            treeList.add(tree);
        }

        String jsonText = TreeGridJson.TreeJson(treeList);
        return JSONObject.parse(jsonText);
    }

    @RequestMapping("/getListTreeJson")
    public List<TreeEntity> getListTreeJson(String json) {

        List<TreeEntity> treeList = new ArrayList<>();
        if (StringUtils.isBlank(json)) return treeList;
        List<ModuleButtonEntity> resultList = new ArrayList<>();
        resultList = (List<ModuleButtonEntity>) JSONUtils.jsonToArray(json, ModuleButtonEntity.class);
        for (ModuleButtonEntity item : resultList) {
            TreeEntity tree = new TreeEntity();
            boolean hasChildren = resultList.stream().anyMatch(x -> x.getParentId().equals(item.getModuleId()));
            tree.setId(item.getModuleButtonId());
            tree.setText(item.getFullName());
            tree.setValue(item.getModuleId());
            tree.setIsExpand(true);
            tree.setComplete(true);
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            tree.setImg(item.getIcon());
            treeList.add(tree);
        }
        //创建结果列表
        List<TreeEntity> newTreeList = new ArrayList<>();
        TreeNodeUtils.convertToTree(treeList, "0", newTreeList);
        return newTreeList;
    }


    /**
     * 新增
     *
     * @param moduleButton
     * @return
     */
    @SysLog("新增功能按钮表")
    @RequestMapping("/save")
    public R save(@RequestBody ModuleButtonEntity moduleButton) {

        return moduleButtonService.saveModuleButton(moduleButton);
    }

    /**
     * 根据id查询详情
     *
     * @param id
     * @return
     */
    @RequestMapping("/info")
    public R getById(@RequestBody String id) {

        return moduleButtonService.getModuleButtonById(id);
    }

    /**
     * 修改
     *
     * @param moduleButton
     * @return
     */
    @SysLog("修改功能按钮表")
    @RequestMapping("/update")
    public R update(@RequestBody ModuleButtonEntity moduleButton) {

        return moduleButtonService.updateModuleButton(moduleButton);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @SysLog("删除功能按钮表")
    @RequestMapping("/remove")
    public R batchRemove(@RequestBody String[] id) {

        return moduleButtonService.batchRemove(id);
    }


    /**
     * 按钮复制
     *
     * @param map
     * @return
     */
    @SysLog("复制功能按钮表")
    @RequestMapping("/copyForm")
    public R copyForm(@RequestBody Map<String, Object> map) {

        String moduleButtonId = map.get("keyValue").toString();
        String moduleId = map.get("moduleId").toString();

        ModuleButtonEntity moduleButtonEntity = moduleButtonService.getEntityById(moduleButtonId);
        if(moduleButtonEntity==null)
        {
            return R.error(500,"请先保存后，再执行该操作");
        }
        moduleButtonEntity.setModuleId(moduleId);

        return moduleButtonService.saveModuleButton(moduleButtonEntity);

    }

}
