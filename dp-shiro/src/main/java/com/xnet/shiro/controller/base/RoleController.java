package com.xnet.shiro.controller.base;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.constant.SystemConstant;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xnet.shiro.service.base.RoleService;

import java.util.List;
import java.util.Map;

/**
 * 系统角色
 *
 * @author Xingyc
 * @date 2018-1-11 14:46:40
 */
@RestController
@RequestMapping("/basemanage/role")
public class RoleController extends AbstractController {

	@Autowired
	private RoleService roleService;
	
	/**
	 * 所有列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<RoleEntity> list(@RequestBody Map<String, Object> params) {
		if(getUserAccount() != SystemConstant.IS_ADMIN) {
			params.put("createUserId", getUserId());
		}
		return roleService.listRole(params);
	}

	/**
	 * 角色列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/listForRole")
	public Page<RoleEntity> listForRole(@RequestBody Map<String, Object> params) {
		if(getUserAccount() != SystemConstant.IS_ADMIN) {
			params.put("createUserId", getUserId());
		}
		params.put("category", SystemConstant.RoleCategoryTypeEnum.ROLE.getValue());
		return roleService.listRole(params);
	}

	/**
	 * 岗位列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/listForPost")
	public Page<RoleEntity> listForPost(@RequestBody Map<String, Object> params) {
		if(getUserAccount() != SystemConstant.IS_ADMIN) {
			params.put("createUserId", getUserId());
		}
		params.put("category", SystemConstant.RoleCategoryTypeEnum.POST.getValue());
		return roleService.listRole(params);
	}

	/**
	 * 职位列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/listForJob")
	public Page<RoleEntity> listForJob(@RequestBody Map<String, Object> params) {
		if(getUserAccount() != SystemConstant.IS_ADMIN) {
			params.put("createUserId", getUserId());
		}
		params.put("category", SystemConstant.RoleCategoryTypeEnum.JOB.getValue());
		return roleService.listRole(params);
	}

	/**
	 * 用户组列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/listForUserGroup")
	public Page<RoleEntity> listForUserGroup(@RequestBody Map<String, Object> params) {
		if(getUserAccount() != SystemConstant.IS_ADMIN) {
			params.put("createUserId", getUserId());
		}
		params.put("category", SystemConstant.RoleCategoryTypeEnum.USER_GROUP.getValue());
		return roleService.listRole(params);
	}

	
	/**
	 * 用户角色
	 * @return
	 */
//	@RequestMapping("/select")
//	public R listRole() {
//		return roleService.listRole();
//	}
	
	/**
	 * 新增角色
	 * @param role
	 * @return
	 */
	@SysLog("新增角色")
	@RequestMapping("/save")
	public R saveRole(@RequestBody RoleEntity role) {
		role.setCreateUserId(getUserId());
		return roleService.saveRole(role);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getRoleById(@RequestBody String id) {
		return roleService.getRoleById(id);
	}
	
	/**
	 * 修改角色
	 * @param role
	 * @return
	 */
	@SysLog("修改角色")
	@RequestMapping("/update")
	public R updateRole(@RequestBody RoleEntity role) {
		return roleService.updateRole(role);
	}
	
	/**
	 * 批量删除
	 * @param id
	 * @return
	 */
	@SysLog("删除角色")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody String[] id) {
		return roleService.batchRemove(id);
	}
	
	/**
	 * 操作权限
	 * @param role
	 * @return
	 */
	@SysLog("操作权限")
	@RequestMapping("/authorize/opt")
	public R updateRoleOptAuthorization(@RequestBody RoleEntity role) {
		return roleService.updateRoleOptAuthorization(role);
	}
	
	/**
	 * 数据权限
	 * @param role
	 * @return
	 */
	@SysLog("数据权限")
	@RequestMapping("/authorize/data")
	public R updateRoleDataAuthorization(@RequestBody RoleEntity role) {
		return roleService.updateRoleDataAuthorization(role);
	}

	/**
	 * 获取下拉列表项目
	 * @return
	 */
	@RequestMapping("/selectItemList")
	public List<RoleEntity> getSelectItemListJson(String category,String organizeId)
	{
		return roleService.getSelectItemList(category,organizeId);
	}
	
}
