package com.xnet.shiro.controller.base;

import com.alibaba.fastjson.JSONObject;
import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.R;
import com.xnet.common.entity.TreeEntity;
import com.xnet.common.entity.TreeGridEntity;
import com.xnet.common.utils.JSONUtils;
import com.xnet.common.utils.ShiroUtils;
import com.xnet.common.utils.TreeGridJson;
import com.xnet.common.utils.TreeNodeUtils;
import com.xnet.shiro.cache.base.OrganizeCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xnet.shiro.entity.base.OrganizeEntity;
import com.xnet.shiro.service.base.OrganizeService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 组织机构
 *
 * @author Xingyc
 * @date 2018-1-11 14:50:01
 */
@RestController
@RequestMapping("/basemanage/organize")
public class OrganizeController extends AbstractController {



    @Autowired
    private OrganizeService organizeService;

//    @Autowired
//    private OrganizeCache organizeCache;

    /**
     * 机构列表
     *
     * @return
     */
    @RequestMapping("/list")
    public Object list() {
        List<OrganizeEntity> data=organizeService.listOrganize();
        List<TreeGridEntity> treeList = new ArrayList<>() ;
        for (OrganizeEntity item : data)
        {
            TreeGridEntity tree = new TreeGridEntity();
            boolean hasChildren = data.stream().anyMatch(x->x.getParentId().equals(item.getOrganizeId()));
            tree.setId(item.getOrganizeId());
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            tree.setExpanded(true);
            tree.setEntityJson(JSONUtils.beanToJson(item,"yyyy-MM-dd") );
            treeList.add(tree);
        }

        String jsonText= TreeGridJson.TreeJson(treeList);
        return  JSONObject.parse(jsonText);
    }

    @RequestMapping("/treelist")
    public List<TreeEntity> treelist(String keyword) {

        List<OrganizeEntity> allList = organizeService.getListTree(null);
        List<OrganizeEntity> filterList = new ArrayList<>();
        if (!org.apache.commons.lang.StringUtils.isBlank(keyword)) {
            filterList = allList.stream().filter(x -> x.getFullName().contains(keyword)).collect(Collectors.toList());
        } else {
            filterList = allList;
        }
        List<TreeEntity> treeList = convertToTreeList(filterList);
        List<TreeEntity> relationTreeList = new ArrayList<>();
        if (keyword != null && !keyword.isEmpty()) {
            List<TreeEntity> allTreeList = convertToTreeList(allList);//获取所有符合条件的树形列表
            TreeNodeUtils.getWholeTreeChain(allTreeList, treeList, relationTreeList);
        } else {
            relationTreeList = treeList;
        }
        List<TreeEntity> newTreeList = new ArrayList<>();
        TreeNodeUtils.convertToTree(relationTreeList, "0", newTreeList);
        return newTreeList;
    }

    /**
     * 转换实体为树形结构
     *
     * @param itemList
     * @return
     */
    private List<TreeEntity> convertToTreeList(List<OrganizeEntity> itemList) {

        List<TreeEntity> treeList = new ArrayList<>();
        for (OrganizeEntity item : itemList) {
            TreeEntity tree = new TreeEntity();
            boolean hasChildren = itemList.stream().filter(x -> x.getParentId().equals(item.getOrganizeId())).count() > 0;
            tree.setId(item.getOrganizeId());
            tree.setText(item.getFullName());
            tree.setValue(item.getOrganizeId());
            tree.setIsExpand(true);
            tree.setComplete(true);
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            treeList.add(tree);
        }
        return treeList;
    }


    /**
     * 树形机构列表，机构新增、编辑
     *
     * @return
     */
    @RequestMapping("/select")
    public List<OrganizeEntity> select() {

        return organizeService.listOrgTree();
    }



    /**
     * 新增机构
     *
     * @param entity
     * @return
     */
    @SysLog("新增机构")
    @RequestMapping("/save")
    public R save(@RequestBody OrganizeEntity entity) {

        return organizeService.saveOrg(entity);
    }

    /**
     * 根据id查询机构详情
     *
     * @param orgId
     * @return
     */
    @RequestMapping("/info")
    public R info(@RequestBody String orgId) {

        return organizeService.getOrg(orgId);
    }

    /**
     * 修改机构
     *
     * @param org
     * @return
     */
    @SysLog("修改机构")
    @RequestMapping("/update")
    public R update(@RequestBody OrganizeEntity org) {

        return organizeService.updateOrg(org);
    }

    /**
     * 删除机构
     *
     * @param id
     * @return
     */
    @SysLog("删除机构")
    @RequestMapping("/remove")
    public R batchRemove(@RequestBody String[] id) {

        return organizeService.bactchRemoveOrg(id);
    }

}
