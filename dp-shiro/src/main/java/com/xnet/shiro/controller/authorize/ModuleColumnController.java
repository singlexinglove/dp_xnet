package com.xnet.shiro.controller.authorize;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.xnet.common.entity.TreeEntity;
import com.xnet.common.utils.JSONUtils;
import com.xnet.common.utils.TreeNodeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.controller.AbstractController;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import com.xnet.shiro.service.authorize.ModuleColumnService;

/**
 * 功能表格列表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
@RestController
@RequestMapping("/authorizeManage/moduleColumn")
public class ModuleColumnController extends AbstractController {
	
	@Autowired
	private ModuleColumnService moduleColumnService;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<ModuleColumnEntity> list(@RequestBody Map<String, Object> params) {
		return moduleColumnService.listModuleColumn(params);
	}

	/**
	 * 列表
	 * @param moduleId
	 * @return
	 */
	@RequestMapping("/listModuleId")
	public List<ModuleColumnEntity> listModuleId(String moduleId) {
		if(StringUtils.isBlank(moduleId))
		{
			return  new ArrayList<>();
		}
		return moduleColumnService.listModuleId(moduleId);
	}

	@RequestMapping("/getListJson")
	public List<ModuleColumnEntity>  getListJson(String json) {
		List<ModuleColumnEntity> resultList=new ArrayList<>();
		if(StringUtils.isBlank(json)) return resultList;
		resultList=	(List<ModuleColumnEntity>) JSONUtils.jsonToArray(json,ModuleColumnEntity.class);
		return resultList;
	}


	/**
	 * 新增
	 * @param moduleColumn
	 * @return
	 */
	@SysLog("新增功能表格列表")
	@RequestMapping("/save")
	public R save(@RequestBody ModuleColumnEntity moduleColumn) {
		return moduleColumnService.saveModuleColumn(moduleColumn);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody String id) {
		return moduleColumnService.getModuleColumnById(id);
	}
	
	/**
	 * 修改
	 * @param moduleColumn
	 * @return
	 */
	@SysLog("修改功能表格列表")
	@RequestMapping("/update")
	public R update(@RequestBody ModuleColumnEntity moduleColumn) {
		return moduleColumnService.updateModuleColumn(moduleColumn);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除功能表格列表")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody String[] id) {
		return moduleColumnService.batchRemove(id);
	}
	
}
