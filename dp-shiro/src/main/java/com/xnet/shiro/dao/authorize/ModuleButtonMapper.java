package com.xnet.shiro.dao.authorize;

import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;

import java.util.List;

/**
 * 功能按钮表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
@MapperScan
public interface ModuleButtonMapper extends BaseMapper<ModuleButtonEntity> {
    List<ModuleButtonEntity> listModuleId(String moduleId);

    /**
     * 根据菜单id删除
     * @param moduleId
     * @return
     */
    int deleteModuleId(String moduleId);

    /**
     *批量插入
     * @return
     */
    int batchInsert(List<ModuleButtonEntity> moduleButtonEntityList);


    /**
     * 所有列表
     * @return
     */
    List<ModuleButtonEntity> listAll();
}
