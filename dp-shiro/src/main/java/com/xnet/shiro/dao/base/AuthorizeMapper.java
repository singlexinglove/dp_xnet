package com.xnet.shiro.dao.base;

import com.xnet.shiro.entity.authorize.AuthorizeEntity;
import com.xnet.common.dao.BaseMapper;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 系统角色与菜单关系
 *
 * @author Xingyc
 * @date 2018-1-9 17:51:02
 */
@MapperScan
public interface AuthorizeMapper extends BaseMapper<AuthorizeEntity> {

	int batchRemoveByMenuId(String[] id);
	
	int batchRemoveByRoleId(String[] id);
	
	List<String> listMenuId(String id);
	
}
