package com.xnet.shiro.dao.base;

import com.xnet.shiro.entity.base.RoleEntity;
import com.xnet.common.dao.BaseMapper;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 系统角色
 *
 * @author Xingyc
 * @date 2018-1-8 11:58:26
 */
@MapperScan
public interface RoleMapper extends BaseMapper<RoleEntity> {
	
	List<String> listUserRoles(String userId);
	
}
