package com.xnet.shiro.dao.base;

import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.base.AreaEntity;

import java.util.List;

/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
@MapperScan
public interface AreaMapper extends BaseMapper<AreaEntity> {
	List<AreaEntity> getAreaListByParentId(String parentId);
}
