package com.xnet.shiro.dao.base;

import com.xnet.common.dao.BaseMapper;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.UserEntity;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 系统用户dao
 *
 * @author Xingyc
 * @date 2018-1-5 11:08:27
 */
@MapperScan
public interface UserMapper extends BaseMapper<UserEntity> {

	UserEntity getByUserName(String username);
	
	List<String> listAllMenuId(String userId);
	
	List<String> listAllOrgId(String userId);
	
	int updatePswdByUser(Query query);
	
	int updateUserStatus(Query query);
	
	int updatePswd(UserEntity user);

	List<UserEntity> listForUser();

	List<UserEntity> getListByDepartmentId(String departmentId);
}
