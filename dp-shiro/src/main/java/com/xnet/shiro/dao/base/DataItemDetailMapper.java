package com.xnet.shiro.dao.base;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.base.DataItemDetailEntity;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 数据字典明细
 *
 * @author Xingyc
 * @date 2018-1-8 11:58:26
 */
@MapperScan
public interface DataItemDetailMapper extends BaseMapper<DataItemDetailEntity> {

    List<DataItemDetailEntity>  getDataItemList();

    List<DataItemDetailEntity> getListByItemId(@Param("itemId") String itemId);
}
