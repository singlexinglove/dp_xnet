package com.xnet.shiro.dao.authorize;

import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.authorize.ModuleEntity;

import java.util.List;

/**
 * 系统功能表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月07日 上午10:31:35
 */
@MapperScan
public interface ModuleMapper extends BaseMapper<ModuleEntity> {
    List<ModuleEntity> listParentId(String parentId);

    List<ModuleEntity> listNotButton();

    List<String> listUserPerms(String userId);

    int countMenuChildren(String parentId);
}
