package com.xnet.shiro.dao.base;

import com.xnet.common.dao.BaseMapper;
import org.mybatis.spring.annotation.MapperScan;
import com.xnet.shiro.entity.base.OrganizeEntity;

import java.util.List;
/**
 * 组织架构
 *
 * @author Xingyc
 * @date 2018-1-11 11:50:45
 */
@MapperScan
public interface OrganizeMapper extends BaseMapper<OrganizeEntity> {

	int countOrgChildren(String parentId);

	List<OrganizeEntity> getList();
	
}
