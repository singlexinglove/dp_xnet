package com.xnet.shiro.dao.authorize;


import com.xnet.shiro.entity.authorize.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PermissionMapper{

    /**
     * 获取权限列表
     * 1=菜单
     * 2=按钮
     * 3=数据列
     * @param objectId
     * @param itemType
     * @return
     */
    List<AuthorizeEntity> listByType(@Param("objectId") String objectId, @Param("itemType") int itemType);

    /**
     *数据权限
     * @param objectId
     * @return
     */
    List<AuthorizeDataEntity> listAuthorizeData(@Param("objectId") String objectId);

    /**
     *获取登录用户可分配的菜单列表
     * @param userId
     * @return
     */
    List<ModuleEntity> listModuleByUser(@Param("userId") String userId);

    /**
     *获取登录用户可分配的菜单按钮列表
     * @param userId
     * @return
     */
    List<ModuleButtonEntity> listModuleButtonByUser(@Param("userId") String userId);

    /**
     *获取登录用户可分配的数据列数据列表
     * @param userId
     * @return
     */
    List<ModuleColumnEntity> listModuleColumnByUser(@Param("userId") String userId);



    /**
     * 删除权限
     * @param objectId
     * @return
     */
    int removeAuthorize(@Param("objectId") String objectId);

    /**
     * 删除数据权限
     * @param objectId
     * @return
     */
    int removeAuthorizeData(@Param("objectId") String objectId);


    /**
     * 新增保存权限
     * @param authorizeEntity
     * @return
     */
    int saveAuthorize(AuthorizeEntity authorizeEntity);

    /**
     * 新增保存数据权限
     * @param authorizeDataEntity
     * @return
     */
    int saveAuthorizeData(AuthorizeDataEntity authorizeDataEntity);


}
