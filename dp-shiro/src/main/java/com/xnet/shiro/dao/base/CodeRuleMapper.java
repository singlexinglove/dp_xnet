package com.xnet.shiro.dao.base;

import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.base.CodeRuleEntity;

/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
@MapperScan
public interface CodeRuleMapper extends BaseMapper<CodeRuleEntity> {
	
}
