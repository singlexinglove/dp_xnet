package com.xnet.shiro.dao.base;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.base.DataItemEntity;
import org.mybatis.spring.annotation.MapperScan;

/**
 * 数据字典分类
 *
 * @author Xingyc
 * @date 2018-1-8 11:58:26
 */
@MapperScan
public interface DataItemMapper extends BaseMapper<DataItemEntity> {
	
//	List<String> listUserRoles(String userId);
	
}
