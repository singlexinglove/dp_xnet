package com.xnet.shiro.dao.base;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.base.UserRelationEntity;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 用户与角色关系
 *
 * @author Xingyc
 * @date 2018-1-9 16:57:38
 */
@MapperScan
public interface UserRelationMapper extends BaseMapper<UserRelationEntity> {

	List<String> listUserRoleId(String userId);
	
	int batchRemoveByUserId(String[] id);
	
	int batchRemoveByRoleId(String[] id);

	int batchRemoveByObjectId(@Param("objectId") String objectId ,@Param("isDefault") int isDefault);

	int removeByUserId(@Param("userId") String userId ,@Param("isDefault") int isDefault);

	List<UserRelationEntity> listMember(@Param("objectId") String objectId);

	List<UserRelationEntity> listObject(@Param("userId") String userId);


}
