package com.xnet.shiro.dao.base;

import com.xnet.common.dao.BaseMapper;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.DepartmentEntity;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 部门dao
 *
 * @author Xingyc
 * @date 2018-1-24 10:50:56
 */
@MapperScan
public interface DepartmentMapper extends BaseMapper<DepartmentEntity> {

    /**
     * 根据机构Id及过滤条件来获取部门列表
     * @param query
     * @return
     */
    List<DepartmentEntity> getList(Query query);

    /**
     * 获取所有部门列表
     * @param
     * @return
     */
    List<DepartmentEntity> listAll();
}
