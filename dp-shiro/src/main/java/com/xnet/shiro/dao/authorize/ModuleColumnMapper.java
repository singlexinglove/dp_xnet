package com.xnet.shiro.dao.authorize;

import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.dao.BaseMapper;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;

import java.util.List;

/**
 * 功能表格列表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
@MapperScan
public interface ModuleColumnMapper extends BaseMapper<ModuleColumnEntity> {
    List<ModuleColumnEntity> listModuleId(String moduleId);
    /**
     * 根据菜单id删除
     * @param moduleId
     * @return
     */
    int deleteModuleId(String moduleId);

    /**
     *批量插入
     * @return
     */
    int batchInsert(List<ModuleColumnEntity> moduleColumnEntityList);

    /**
     * 所有列表
     * @return
     */
    List<ModuleColumnEntity> listAll();

}
