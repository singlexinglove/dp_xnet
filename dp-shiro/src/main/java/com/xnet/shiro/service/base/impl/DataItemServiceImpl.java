package com.xnet.shiro.service.base.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.xnet.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.base.DataItemEntity;
import com.xnet.shiro.manager.base.DataItemManager;
import com.xnet.shiro.service.base.DataItemService;

/**
 * 数据字典分类表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月13日 下午3:14:19
 */
@Service("dataItemService")
public class DataItemServiceImpl  extends BaseServiceImpl<DataItemEntity> implements DataItemService {

	@Autowired
	private DataItemManager dataItemManager;

	@Override
	public Page<DataItemEntity> listDataItem(Map<String, Object> params) {
		Query query = new Query(params);
		Page<DataItemEntity> page = new Page<>(query);
		dataItemManager.listDataItem(page, query);
		return page;
	}

	@Override
	public R saveDataItem(DataItemEntity entity) {
		create(entity);
		int count = dataItemManager.saveDataItem(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R getDataItemById(String id) {
		DataItemEntity dataItem = dataItemManager.getDataItemById(id);
		return CommonUtils.msg(dataItem);
	}

	@Override
	public R updateDataItem(DataItemEntity entity) {
		modify(entity);
		int count = dataItemManager.updateDataItem(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = dataItemManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	@Override
	public List<DataItemEntity> listAll()
	{
		return  dataItemManager.listAll();
	}

	/**
	 * 创建默认方法
	 *
	 * @param obj
	 */
	@Override
	public void create(DataItemEntity obj) {
		super.create(obj);
		obj.setItemId(UUID.randomUUID().toString());
		obj.setDeleteMark(0);
		obj.setEnabledMark(1);
	}

	/**
	 * 修改默认方法
	 *
	 * @param obj
	 */
	@Override
	public void modify(DataItemEntity obj) {
		super.modify(obj);
	}
}
