package com.xnet.shiro.service.base;

import com.xnet.base.service.BaseService;

import java.util.List;
import java.util.Map;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.DataItemEntity;

/**
 * 数据字典分类表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月13日 下午3:14:19
 */
public interface DataItemService extends BaseService<DataItemEntity> {

	Page<DataItemEntity> listDataItem(Map<String, Object> params);
	
	R saveDataItem(DataItemEntity dataItem);
	
	R getDataItemById(String id);
	
	R updateDataItem(DataItemEntity dataItem);
	
	R batchRemove(String[] id);

	public List<DataItemEntity> listAll();

	
}
