package com.xnet.shiro.service.base.impl;

import com.xnet.common.entity.TreeEntity;
import com.xnet.shiro.entity.base.DepartmentEntity;
import com.xnet.shiro.entity.base.OrganizeEntity;
import com.xnet.shiro.manager.base.DepartmentManager;
import com.xnet.shiro.service.base.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 部门服务层
 *
 * @author Xingyc
 * @date 2018-1-24 14:11:00
 */
@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentManager departmentManager;

    @Override
    public List<DepartmentEntity> getListTree(String organizeId, String keyword) {

        return departmentManager.getListByOrganize(organizeId, keyword);
    }

    /**
     * 转换实体为树形结构
     *
     * @param orgItemList
     * @return
     */
    public List<TreeEntity> convertOrganizeToTreeList(List<OrganizeEntity> orgItemList, List<DepartmentEntity> deptItemList, boolean isLeafNode) {

        List<TreeEntity> treeList = new ArrayList<>();
        //转换机构节点
        for (OrganizeEntity item : orgItemList) {
            TreeEntity tree = new TreeEntity();
            boolean hasChildren = orgItemList.stream().filter(x -> x.getParentId().equals(item.getOrganizeId())).count() > 0;
            if (!hasChildren) {
                //如果机构为叶子节点 则判断其下是否挂有部门节点
                hasChildren = deptItemList.stream().filter(x -> x.getOrganizeId().equals(item.getOrganizeId())).count() > 0;
            }
            tree.setId(item.getOrganizeId());
            tree.setText(item.getFullName());
            tree.setValue(item.getOrganizeId());
            tree.setIsExpand(true);
            tree.setComplete(true);
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId());
            //增加属性
            tree.setAttribute("Sort");
            tree.setAttributeValue("Organize");
            tree.setLevel(-1);
            treeList.add(tree);
        }
        //转换部门节点
        for (DepartmentEntity item : deptItemList) {
            TreeEntity tree = new TreeEntity();
            boolean hasChildren = true;
            if (isLeafNode) {
                hasChildren = deptItemList.stream().filter(x -> x.getParentId().equals(item.getDepartmentId())).count() > 0;
            }
            tree.setId(item.getDepartmentId());
            tree.setText(item.getFullName());
            tree.setValue(item.getDepartmentId());
            tree.setIsExpand(true);
            tree.setComplete(true);
            tree.setHasChildren(hasChildren);
            tree.setParentId(item.getParentId().equals("0") ? item.getOrganizeId() : item.getParentId());
            //增加属性
            tree.setAttribute("Sort");
            tree.setAttributeValue("Department");
            treeList.add(tree);
        }


        return treeList;
    }

    @Override
    public Page<DepartmentEntity> listDepartment(Map<String, Object> params) {
        Query query = new Query(params);
        Page<DepartmentEntity> page = new Page<>(query);
        departmentManager.listDepartment(page, query);
        return page;
    }


    /**
     * 通过组织机构获取对应的部门列表(全部列表)
     * @param organizeId
     * @param keyword
     * @return
     */
    @Override
    public List<DepartmentEntity> listDepartment(String organizeId, String keyword)
    {
         return departmentManager.listDepartment(organizeId,keyword);
    }

    @Override
    public R saveDepartment(DepartmentEntity entity) {
        entity.setDepartmentId(UUID.randomUUID().toString());
        int count = departmentManager.saveDepartment(entity);
        return CommonUtils.msg(count);
    }

    @Override
    public R getDepartmentById(String id) {
        DepartmentEntity department = departmentManager.getDepartmentById(id);
        return CommonUtils.msg(department);
    }

    @Override
    public DepartmentEntity getDepartmentEntityById(String id){
        return departmentManager.getDepartmentById(id);
    }

    @Override
    public R updateDepartment(DepartmentEntity department) {
        int count = departmentManager.updateDepartment(department);
        return CommonUtils.msg(count);
    }

    @Override
    public R batchRemove(String[] id) {
        int count = departmentManager.batchRemove(id);
        return CommonUtils.msg(id, count);
    }

    /**
     * 获取所有部门列表
     * @param
     * @return
     */
    @Override
    public  List<DepartmentEntity> listAll(){
      return  departmentManager.listAll();
    }
}
