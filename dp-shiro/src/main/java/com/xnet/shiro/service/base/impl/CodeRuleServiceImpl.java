package com.xnet.shiro.service.base.impl;

import java.util.Map;
import java.util.UUID;

import com.xnet.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.base.CodeRuleEntity;
import com.xnet.shiro.manager.base.CodeRuleManager;
import com.xnet.shiro.service.base.CodeRuleService;

/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
@Service("codeRuleService")
public class CodeRuleServiceImpl  extends BaseServiceImpl<CodeRuleEntity> implements CodeRuleService {

	@Autowired
	private CodeRuleManager codeRuleManager;

	@Override
	public Page<CodeRuleEntity> listCodeRule(Map<String, Object> params) {
		Query query = new Query(params);
		Page<CodeRuleEntity> page = new Page<>(query);
		codeRuleManager.listCodeRule(page, query);
		return page;
	}

	@Override
	public R saveCodeRule(CodeRuleEntity entity) {
		create(entity);
		int count = codeRuleManager.saveCodeRule(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R getCodeRuleById(String id) {
		CodeRuleEntity codeRule = codeRuleManager.getCodeRuleById(id);
		return CommonUtils.msg(codeRule);
	}

	@Override
	public R updateCodeRule(CodeRuleEntity entity) {
		modify(entity);
		int count = codeRuleManager.updateCodeRule(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = codeRuleManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}
	/**
	 * 创建默认方法
	 *
	 * @param obj
	 */
	@Override
	public void create(CodeRuleEntity obj) {
		super.create(obj);
		obj.setRuleId(UUID.randomUUID().toString());
		obj.setDeleteMark(0);
		obj.setEnabledMark(1);
	}

	/**
	 * 修改默认方法
	 *
	 * @param obj
	 */
	@Override
	public void modify(CodeRuleEntity obj) {
		super.modify(obj);
	}
}
