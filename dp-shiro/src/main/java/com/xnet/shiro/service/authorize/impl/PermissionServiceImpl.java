package com.xnet.shiro.service.authorize.impl;

import com.xnet.common.constant.SystemConstant;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.common.utils.ShiroUtils;
import com.xnet.shiro.entity.authorize.*;
import com.xnet.shiro.entity.base.UserRelationEntity;
import com.xnet.shiro.manager.authorize.ModuleButtonManager;
import com.xnet.shiro.manager.authorize.ModuleColumnManager;
import com.xnet.shiro.manager.authorize.ModuleManager;
import com.xnet.shiro.manager.authorize.PermissionManager;
import com.xnet.shiro.service.authorize.PermissionService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionManager permissionManager;

    @Autowired
    private ModuleManager moduleManager;

    @Autowired
    private ModuleButtonManager moduleButtonManager;

    @Autowired
    private ModuleColumnManager moduleColumnManager;

    /**
     * 菜单列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeEntity> listModule(String objectId) {

        return permissionManager.listModule(objectId);
    }

    /**
     * 菜单按钮列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeEntity> listModuleButton(String objectId) {

        return permissionManager.listModuleButton(objectId);
    }

    /**
     * 菜单表格列列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeEntity> listModuleColumn(String objectId) {

        return permissionManager.listModuleColumn(objectId);
    }

    /**
     * 数据权限
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeDataEntity> listAuthorizeData(String objectId) {

        return permissionManager.listAuthorizeData(objectId);
    }

    /**
     * 获取登录用户可分配的菜单列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<ModuleEntity> listModuleByUser(String userId) {

        if (SystemConstant.IS_ADMIN.equals(ShiroUtils.getUserId())) {
            return moduleManager.listAll();
        }
        return permissionManager.listModuleByUser(userId);
    }

    /**
     * 获取登录用户可分配的菜单按钮列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<ModuleButtonEntity> listModuleButtonByUser(String userId) {

        if (SystemConstant.IS_ADMIN.equals(ShiroUtils.getUserId())) {
            return moduleButtonManager.listAll();
        }
        return permissionManager.listModuleButtonByUser(userId);
    }

    /**
     * 获取登录用户可分配的数据列数据列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<ModuleColumnEntity> listModuleColumnByUser(String userId) {

        if (SystemConstant.IS_ADMIN.equals(ShiroUtils.getUserId())) {
            return moduleColumnManager.listAll();
        }
        return permissionManager.listModuleColumnByUser(userId);
    }

    /**
     * 获取成员列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<UserRelationEntity> listMember(String objectId) {
        return permissionManager.listMember(objectId);
    }

    /**
     * 获取对象列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<UserRelationEntity> listObject(String userId) {
        return permissionManager.listObject(userId);
    }

    /**
     * 保存权限
     *
     * @param authorizeType
     * @param objectId
     * @param moduleIds
     * @param moduleButtonIds
     * @param moduleColumnIds
     * @param authorizeDataList
     * @return
     */
    @Override
    public R saveAuthorize(SystemConstant.AuthorizeCategoryEnum authorizeType, String objectId, String[] moduleIds, String[] moduleButtonIds, String[] moduleColumnIds, List<AuthorizeDataEntity> authorizeDataList) {

        int count = permissionManager.saveAuthorize(authorizeType, objectId, moduleIds, moduleButtonIds, moduleColumnIds, authorizeDataList);
        return CommonUtils.msg(count);
    }

    /**
     * 保存成员用户分配
     * @param authorizeType
     * @param objectId
     * @param userIds
     * @return
     */
    @Override
    public R saveMember(SystemConstant.AuthorizeCategoryEnum authorizeType, String objectId, String userIds)
    {
        String[] arrayUserId = userIds.split(",");
        int count = permissionManager.saveMember(authorizeType,objectId,arrayUserId);
        return CommonUtils.msg(count);
    }
}
