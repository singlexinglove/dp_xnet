package com.xnet.shiro.service.authorize;

import java.util.List;
import java.util.Map;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import com.xnet.shiro.entity.authorize.ModuleEntity;

/**
 * 系统功能表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月07日 上午10:31:35
 */
public interface ModuleService {


	R listUserMenu(String userId);

	List<ModuleEntity> listMenu(Map<String, Object> params);

	Page<ModuleEntity> listModule(Map<String, Object> params);

	List<ModuleEntity> listAllModule();
	
	R saveModule(ModuleEntity moduleEntity, List<ModuleButtonEntity> moduleButtonList, List<ModuleColumnEntity> moduleColumnList) ;
	
	R getModuleById(String id);
	
	R updateModule(ModuleEntity module);
	
	R batchRemove(String[] id);
	
}
