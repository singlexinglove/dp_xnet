package com.xnet.shiro.service.base;

import com.xnet.base.service.BaseService;
import com.xnet.common.entity.R;
import com.xnet.shiro.cache.CacheService;
import com.xnet.shiro.entity.base.OrganizeEntity;

import java.util.List;

/**
 * 组织机构
 *
 * @author Xingyc
 * @date 2018-1-11 11:52:35
 */
public interface OrganizeService extends CacheService<OrganizeEntity>,BaseService<OrganizeEntity> {

	List<OrganizeEntity> listOrganize();
	
	List<OrganizeEntity> listOrgTree();
	
	R saveOrg(OrganizeEntity org);
	
	R getOrg(String orgId);
	
	R updateOrg(OrganizeEntity org);
	
	R bactchRemoveOrg(String[] id);

	List<OrganizeEntity> getListTree(String keyword) ;

	OrganizeEntity getOrganizeEnitiyById(String organizeId);
}
