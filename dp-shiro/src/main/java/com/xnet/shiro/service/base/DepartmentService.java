package com.xnet.shiro.service.base;

import com.xnet.common.entity.TreeEntity;
import com.xnet.shiro.entity.base.DepartmentEntity;
import com.xnet.shiro.entity.base.OrganizeEntity;
import java.util.Map;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import java.util.List;

/**
 * 部门服务层接口
 *
 * @author Xingyc
 * @date 2018-1-11 11:52:35
 */
public interface DepartmentService {

    List<DepartmentEntity> getListTree(String organizeId,String keyword) ;


    /**
     * 转换实体为树形结构
     *
     * @param orgItemList
     * @param deptItemList
     * @param isLeafNode 是否设置部门为叶子节点即无下属子节点
     * @return
     */
    List<TreeEntity> convertOrganizeToTreeList(List<OrganizeEntity> orgItemList, List<DepartmentEntity> deptItemList,boolean isLeafNode);

    Page<DepartmentEntity> listDepartment(Map<String, Object> params);

    List<DepartmentEntity> listDepartment(String organizeId, String keyword);

    R saveDepartment(DepartmentEntity department);

    R getDepartmentById(String id);

    R updateDepartment(DepartmentEntity department);

    R batchRemove(String[] id);

    /**
     * 获取所有部门列表
     * @param
     * @return
     */
    List<DepartmentEntity> listAll();

    DepartmentEntity getDepartmentEntityById(String id);

}
