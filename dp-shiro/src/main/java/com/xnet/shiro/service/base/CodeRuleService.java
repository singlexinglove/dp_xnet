package com.xnet.shiro.service.base;

import com.xnet.base.service.BaseService;
import java.util.Map;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.CodeRuleEntity;

/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
public interface CodeRuleService extends BaseService<CodeRuleEntity> {

	Page<CodeRuleEntity> listCodeRule(Map<String, Object> params);
	
	R saveCodeRule(CodeRuleEntity codeRule);
	
	R getCodeRuleById(String id);
	
	R updateCodeRule(CodeRuleEntity codeRule);
	
	R batchRemove(String[] id);
	
}
