package com.xnet.shiro.service.base.impl;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.base.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xnet.shiro.manager.base.RoleManager;
import com.xnet.shiro.service.base.RoleService;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 系统角色
 *
 * @author Xingyc
 * @date 2018-1-11 11:58:40
 */
@Service("sysRoleService")
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleManager roleManager;

	@Override
	public Page<RoleEntity> listRole(Map<String, Object> params) {
		Query query = new Query(params);
		Page<RoleEntity> page = new Page<>(query);
		roleManager.listRole(page, query);
		return page;
	}

	@Override
	public R saveRole(RoleEntity entity) {
		entity.setRoleId(UUID.randomUUID().toString());
		entity.setCreateDate(new Date());
		int count = roleManager.saveRole(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R getRoleById(String id) {
		RoleEntity role = roleManager.getRoleById(id);
		return CommonUtils.msg(role);
	}

	@Override
	public 	RoleEntity getRoleEntiyById(String id)
	{
        return roleManager.getRoleById(id);
	}

	@Override
	public R updateRole(RoleEntity entity) {
		entity.setModifyDate(new Date());
		int count = roleManager.updateRole(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = roleManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

//	@Override
//	public R listRole() {
//		List<RoleEntity> roleList = roleManager.listRole();
//		return CommonUtils.msgNotCheckNull(roleList);
//	}

	@Override
	public R updateRoleOptAuthorization(RoleEntity role) {
		int count = roleManager.updateRoleOptAuthorization(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R updateRoleDataAuthorization(RoleEntity role) {
		int count = roleManager.updateRoleDataAuthorization(role);
		return CommonUtils.msg(count);
	}

	/**
	 * 获取下拉列表项目
	 * @return
	 */
	@Override
	public List<RoleEntity> getSelectItemList(String category,String organizeId)
	{
		Query query = new Query();
		query.put("category", category);
		query.put("organizeId", organizeId);
		return roleManager.listRole(query);
	}


	
}
