package com.xnet.shiro.service.base.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import com.xnet.base.service.impl.BaseServiceImpl;
import com.xnet.common.utils.ChineseCharacterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.base.DataItemDetailEntity;
import com.xnet.shiro.manager.base.DataItemDetailManager;
import com.xnet.shiro.service.base.DataItemDetailService;

/**
 * 数据字典明细
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月08日 下午1:49:58
 */
@Service("dataItemDetailService")
public class DataItemDetailServiceImpl extends BaseServiceImpl<DataItemDetailEntity> implements DataItemDetailService {

	@Autowired
	private DataItemDetailManager dataItemDetailManager;

	@Override
	public Page<DataItemDetailEntity> listDataItemDetail(Map<String, Object> params) {
		Query query = new Query(params);
		Page<DataItemDetailEntity> page = new Page<>(query);
		dataItemDetailManager.listDataItemDetail(page, query);
		return page;
	}

	@Override
	public R saveDataItemDetail(DataItemDetailEntity entity) {
		create(entity);
		int count = dataItemDetailManager.saveDataItemDetail(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R getDataItemDetailById(String id) {
		DataItemDetailEntity dataItemDetail = dataItemDetailManager.getDataItemDetailById(id);
		return CommonUtils.msg(dataItemDetail);
	}

	@Override
	public R updateDataItemDetail(DataItemDetailEntity entity) {
		modify(entity);
		int count = dataItemDetailManager.updateDataItemDetail(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = dataItemDetailManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	//获取字典项目
	public List<DataItemDetailEntity> getDataItemList(String  enCode)
	{
		return dataItemDetailManager.getDataItemList().stream().filter(x->x.getEnCode().equals(enCode)).collect(Collectors.toList());
	}

	/**
	 * 根据id获取对应的明细字典
	 * @param itemId
	 * @return
	 */
	@Override
	public List<DataItemDetailEntity> getListByItemId(String itemId)
	{
		return dataItemDetailManager.getListByItemId(itemId);
	}

	/**
	 * 创建默认方法
	 *
	 * @param obj
	 */
	@Override
	public void create(DataItemDetailEntity obj) {
		super.create(obj);
		obj.setItemDetailId(UUID.randomUUID().toString());
		obj.setSimpleSpelling(ChineseCharacterUtils.convertHanzi2Pinyin(obj.getItemName(),true));
		obj.setDeleteMark(0);
		obj.setEnabledMark(1);
	}

	/**
	 * 修改默认方法
	 *
	 * @param obj
	 */
	@Override
	public void modify(DataItemDetailEntity obj) {
		obj.setSimpleSpelling(ChineseCharacterUtils.convertHanzi2Pinyin(obj.getItemName(),true));
		super.modify(obj);
		//obj.setAreaId(obj.getAreaCode());
	}
}
