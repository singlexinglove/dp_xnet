package com.xnet.shiro.service.base.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import com.xnet.base.service.impl.BaseServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.base.AreaEntity;
import com.xnet.shiro.manager.base.AreaManager;
import com.xnet.shiro.service.base.AreaService;

/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
@Service("areaService")
public class AreaServiceImpl extends BaseServiceImpl<AreaEntity> implements AreaService {

	@Autowired
	private AreaManager areaManager;

	@Override
	public Page<AreaEntity> listArea(Map<String, Object> params) {
		Query query = new Query(params);
		Page<AreaEntity> page = new Page<>(query);
		areaManager.listArea(page, query);
		return page;
	}

	@Override
	public R saveArea(AreaEntity entity) {
		if(StringUtils.isBlank(entity.getParentId()))
		{
			entity.setParentId("0");
		}
		create(entity);
		int count = areaManager.saveArea(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R getAreaById(String id) {
		AreaEntity area = areaManager.getAreaById(id);
		return CommonUtils.msg(area);
	}

	@Override
	public R updateArea(AreaEntity entity) {
		modify(entity);
		int count = areaManager.updateArea(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = areaManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	//根据父类id获取区划列表
	public List<AreaEntity> getAreaListByParentId(String parentId,String keyword)
	{
		List<AreaEntity> areaList= areaManager.getAreaListByParentId(parentId);
		if(areaList==null||areaList.isEmpty()) return areaList;

		if(StringUtils.isBlank(keyword))
		{
			return areaList.stream().filter(x->x.getParentId().equals(parentId)).collect(Collectors.toList());
		}
		else
		{
			return areaList.stream().filter(x->x.getParentId().equals(parentId)&&x.getAreaName().contains(keyword)).collect(Collectors.toList());
		}
	}


	/**
	 * 创建默认方法
	 *
	 * @param obj
	 */
	@Override
	public void create(AreaEntity obj) {
		super.create(obj);
		obj.setAreaId(obj.getAreaCode());
		obj.setDeleteMark(0);
		obj.setEnabledMark(1);
	}

	/**
	 * 修改默认方法
	 *
	 * @param obj
	 */
	@Override
	public void modify(AreaEntity obj) {
		super.modify(obj);
		//obj.setAreaId(obj.getAreaCode());
	}
}
