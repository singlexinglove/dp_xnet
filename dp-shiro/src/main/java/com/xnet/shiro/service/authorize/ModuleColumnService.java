package com.xnet.shiro.service.authorize;

import java.util.List;
import java.util.Map;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;

/**
 * 功能表格列表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
public interface ModuleColumnService {

	Page<ModuleColumnEntity> listModuleColumn(Map<String, Object> params);
	
	R saveModuleColumn(ModuleColumnEntity moduleColumn);
	
	R getModuleColumnById(String id);
	
	R updateModuleColumn(ModuleColumnEntity moduleColumn);
	
	R batchRemove(String[] id);

	List<ModuleColumnEntity> listModuleId(String moduleId);

	List<ModuleColumnEntity> listAll();
}
