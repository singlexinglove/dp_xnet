package com.xnet.shiro.service.base;

import java.util.List;
import java.util.Map;

import com.xnet.base.service.BaseService;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.AreaEntity;

/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
public interface AreaService extends BaseService<AreaEntity> {

	Page<AreaEntity> listArea(Map<String, Object> params);
	
	R saveArea(AreaEntity area);
	
	R getAreaById(String id);
	
	R updateArea(AreaEntity area);
	
	R batchRemove(String[] id);

	//根据父类id获取区划列表
	List<AreaEntity> getAreaListByParentId(String parentId,String keyword);
	
}
