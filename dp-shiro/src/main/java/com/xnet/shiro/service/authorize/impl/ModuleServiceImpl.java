package com.xnet.shiro.service.authorize.impl;

import java.util.List;
import java.util.Map;

import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.authorize.ModuleEntity;
import com.xnet.shiro.manager.authorize.ModuleManager;
import com.xnet.shiro.service.authorize.ModuleService;

/**
 * 系统功能表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月07日 上午10:31:35
 */
@Service("moduleService")
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	private ModuleManager moduleManager;


	@Override
	public R listUserMenu(String userId) {
		List<ModuleEntity> menuList = moduleManager.listUserMenu(userId);
		return R.ok().put("menuList", menuList);
	}

	@Override
	public List<ModuleEntity> listMenu(Map<String, Object> params) {
		Query query = new Query(params);
		List<ModuleEntity> menuList = moduleManager.listMenu(query);
		return menuList;
	}

	@Override
	public Page<ModuleEntity> listModule(Map<String, Object> params) {
		Query query = new Query(params);
		Page<ModuleEntity> page = new Page<>(query);
		moduleManager.listModule(page, query);
		return page;
	}

	@Override
	public List<ModuleEntity> listAllModule()
	{
		return moduleManager.listMenu(null);

	}


	@Override
	public R saveModule(ModuleEntity moduleEntity, List<ModuleButtonEntity> moduleButtonList, List<ModuleColumnEntity> moduleColumnList) {
		int count = moduleManager.saveModule(moduleEntity,moduleButtonList,moduleColumnList);
		return CommonUtils.msg(count);
	}

	@Override
	public R getModuleById(String id) {
		ModuleEntity module = moduleManager.getModuleById(id);
		return CommonUtils.msg(module);
	}

	@Override
	public R updateModule(ModuleEntity module) {
		int count = moduleManager.updateModule(module);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = moduleManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

}
