package com.xnet.shiro.service.base;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.RoleEntity;

import java.util.List;
import java.util.Map;

/**
 * 系统角色
 *
 * @author Xingyc 
 * @date 2017年8月12日 上午12:40:42
 */
public interface RoleService {

	Page<RoleEntity> listRole(Map<String, Object> params);
	
	R saveRole(RoleEntity role);
	
	R getRoleById(String id);

	RoleEntity getRoleEntiyById(String id);
	
	R updateRole(RoleEntity role);
	
	R batchRemove(String[] id);
	
//	R listRole();
	
	R updateRoleOptAuthorization(RoleEntity role);
	
	R updateRoleDataAuthorization(RoleEntity role);

	/**
	 * 获取下拉列表项目
	 * @return
	 */
	List<RoleEntity> getSelectItemList(String category,String organizeId);
	
}
