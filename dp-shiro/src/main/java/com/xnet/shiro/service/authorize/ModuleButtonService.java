package com.xnet.shiro.service.authorize;

import java.util.List;
import java.util.Map;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;

/**
 * 功能按钮表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
public interface ModuleButtonService {

	Page<ModuleButtonEntity> listModuleButton(Map<String, Object> params);
	
	R saveModuleButton(ModuleButtonEntity moduleButton);
	
	R getModuleButtonById(String id);
	
	R updateModuleButton(ModuleButtonEntity moduleButton);
	
	R batchRemove(String[] id);

	List<ModuleButtonEntity> listModuleId(String moduleId);

	ModuleButtonEntity getEntityById(String moduleButtonId);

	List<ModuleButtonEntity> listAll();
}
