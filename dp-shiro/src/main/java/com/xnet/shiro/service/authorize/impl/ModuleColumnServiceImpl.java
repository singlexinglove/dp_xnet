package com.xnet.shiro.service.authorize.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import com.xnet.shiro.manager.authorize.ModuleColumnManager;
import com.xnet.shiro.service.authorize.ModuleColumnService;

/**
 * 功能表格列表
 *
 * @author xyc
 * @email
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
@Service("moduleColumnService")
public class ModuleColumnServiceImpl implements ModuleColumnService {

    @Autowired
    private ModuleColumnManager moduleColumnManager;

    @Override
    public Page<ModuleColumnEntity> listModuleColumn(Map<String, Object> params) {

        Query query = new Query(params);
        Page<ModuleColumnEntity> page = new Page<>(query);
        moduleColumnManager.listModuleColumn(page, query);
        return page;
    }

    @Override
    public R saveModuleColumn(ModuleColumnEntity entity) {
        entity.setModuleColumnId(UUID.randomUUID().toString());
        int count = moduleColumnManager.saveModuleColumn(entity);
        return CommonUtils.msg(count);
    }

    @Override
    public R getModuleColumnById(String id) {

        ModuleColumnEntity moduleColumn = moduleColumnManager.getModuleColumnById(id);
        return CommonUtils.msg(moduleColumn);
    }

    @Override
    public R updateModuleColumn(ModuleColumnEntity moduleColumn) {

        int count = moduleColumnManager.updateModuleColumn(moduleColumn);
        return CommonUtils.msg(count);
    }

    @Override
    public R batchRemove(String[] id) {

        int count = moduleColumnManager.batchRemove(id);
        return CommonUtils.msg(id, count);
    }

    /**
     * 根据菜单id获取其对应的列表
     *
     * @param moduleId
     * @return
     */
    @Override
    public List<ModuleColumnEntity> listModuleId(String moduleId) {

        return moduleColumnManager.listModuleId(moduleId);
    }

    /**
     * 获取所有列表
     * @return
     */
    @Override
    public List<ModuleColumnEntity> listAll() {

        return moduleColumnManager.listAll();
    }

}
