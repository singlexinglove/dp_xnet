package com.xnet.shiro.service.base;

import java.util.List;
import java.util.Map;

import com.xnet.base.service.BaseService;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.shiro.entity.base.DataItemDetailEntity;

/**
 * 数据字典明细
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月08日 下午1:49:58
 */
public interface DataItemDetailService extends BaseService<DataItemDetailEntity> {

	Page<DataItemDetailEntity> listDataItemDetail(Map<String, Object> params);
	
	R saveDataItemDetail(DataItemDetailEntity dataItemDetail);
	
	R getDataItemDetailById(String id);
	
	R updateDataItemDetail(DataItemDetailEntity dataItemDetail);
	
	R batchRemove(String[] id);

	List<DataItemDetailEntity> getDataItemList(String  enCode);

	/**
	 * 根据id获取对应的明细字典
	 * @param itemId
	 * @return
	 */
	List<DataItemDetailEntity> getListByItemId(String itemId);
}
