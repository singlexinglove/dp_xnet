package com.xnet.shiro.service.base;

import com.xnet.base.service.BaseService;
import com.xnet.common.entity.*;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author Xingyc
 * @date 2018-1-6 18:52:19
 */
public interface UserService extends BaseService<UserEntity> {

	Page<UserEntity> listUser(Map<String, Object> params);
	
	R saveUser(UserEntity user);
	
	R getUserById(String userId);
	
	R updateUser(UserEntity user);
	
	R batchRemove(String[] id);
	
	R listUserPerms(String userId);
	
	R updatePswdByUser(UserEntity user);
	
	R updateUserEnable(String[] id);
	
	R updateUserDisable(String[] id);
	
	R updatePswd(UserEntity user);

	List<TreeEntity> getOrganizeDepartmentUserTreelist();

	List<UserEntity> getListByDepartmentId(String departmentId);

	List<UserEntity> listAllUser();
}
