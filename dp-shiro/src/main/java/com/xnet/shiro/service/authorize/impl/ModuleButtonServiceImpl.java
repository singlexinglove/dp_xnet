package com.xnet.shiro.service.authorize.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.manager.authorize.ModuleButtonManager;
import com.xnet.shiro.service.authorize.ModuleButtonService;

/**
 * 功能按钮表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
@Service("moduleButtonService")
public class ModuleButtonServiceImpl implements ModuleButtonService {

	@Autowired
	private ModuleButtonManager moduleButtonManager;

	@Override
	public Page<ModuleButtonEntity> listModuleButton(Map<String, Object> params) {
		Query query = new Query(params);
		Page<ModuleButtonEntity> page = new Page<>(query);
		moduleButtonManager.listModuleButton(page, query);
		return page;
	}

	@Override
	public R saveModuleButton(ModuleButtonEntity entity) {
		entity.setModuleButtonId(UUID.randomUUID().toString());
		int count = moduleButtonManager.saveModuleButton(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R getModuleButtonById(String id) {
		ModuleButtonEntity moduleButton = moduleButtonManager.getModuleButtonById(id);
		return CommonUtils.msg(moduleButton);
	}

	@Override
	public R updateModuleButton(ModuleButtonEntity moduleButton) {
		int count = moduleButtonManager.updateModuleButton(moduleButton);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = moduleButtonManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	/**
	 * 根据菜单id获取其对应的按钮列表
	 * @param moduleId
	 * @return
	 */
	@Override
	public List<ModuleButtonEntity> listModuleId(String moduleId)
	{
	   return	moduleButtonManager.listModuleId(moduleId);
	}


	/**
	 * 根据主键获取对应的实体
	 * @param moduleButtonId
	 * @return
	 */
	@Override
	public ModuleButtonEntity getEntityById(String moduleButtonId)
	{
		return moduleButtonManager.getModuleButtonById(moduleButtonId);
	}


	/**
	 * 显示所有列表
	 * @return
	 */
	@Override
	public	List<ModuleButtonEntity> listAll()
	{
      return moduleButtonManager.listAll();
	}
}
