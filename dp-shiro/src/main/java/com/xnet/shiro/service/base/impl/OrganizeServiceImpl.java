package com.xnet.shiro.service.base.impl;

import com.alibaba.fastjson.JSONObject;
import com.xnet.base.service.impl.BaseServiceImpl;
import com.xnet.common.entity.TreeGridEntity;
import com.xnet.common.utils.*;
import com.xnet.shiro.cache.base.OrganizeCache;
import com.xnet.shiro.manager.base.OrganizeManager;
import com.xnet.common.constant.MsgConstant;
import com.xnet.common.entity.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xnet.shiro.entity.base.OrganizeEntity;
import com.xnet.shiro.service.base.OrganizeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 组织机构
 *
 * @author Xingyc
 * @date 2018-1-11 11:42:55
 */
@Service("sysOrgService")
public class OrganizeServiceImpl extends BaseServiceImpl<OrganizeEntity> implements OrganizeService {

    @Autowired
    private OrganizeManager organizeManager;

    @Autowired
    private OrganizeCache organizeCache;


    //region ==数据查询==

    @Override
    public List<OrganizeEntity> listOrganize() {
        return organizeCache.listCache();
    }

    @Override
    public List<OrganizeEntity> listOrgTree() {

        List<OrganizeEntity> orgList = organizeManager.listOrg();
        OrganizeEntity org = new OrganizeEntity();
        org.setOrganizeId("0");
        org.setFullName("一级机构");
        org.setParentId("-1");
//		org.setOpen(true);
        orgList.add(org);
        return orgList;
    }

    @Override
    public List<OrganizeEntity> getListTree(String keyword) {
        List<OrganizeEntity> resultTree = organizeManager.GetListByNature(keyword);
        if (keyword != null && !keyword.isEmpty()) {
            resultTree.stream().filter(x -> x.getFullName().contains(keyword)).collect(Collectors.toList());
        }
        return resultTree;
    }

    @Override
    public OrganizeEntity getOrganizeEnitiyById(String organizeId) {

        return organizeManager.getOrg(organizeId);
    }

    //endregion

    //region ==数据操作==

    @Override
    public R saveOrg(OrganizeEntity entity) {
        create(entity);
        int count = organizeManager.saveOrg(entity);
        if(count>0)
        {
            organizeCache.updateCache(organizeCache.cacheKey);
        }
        return CommonUtils.msg(count);
    }

    @Override
    public R getOrg(String orgId) {
        OrganizeEntity org = organizeManager.getOrg(orgId);
        return CommonUtils.msg(org);
    }

    @Override
    public R updateOrg(OrganizeEntity entity) {
        modify(entity);
        int count = organizeManager.updateOrg(entity);
        if(count>0)
        {
            organizeCache.updateCache(organizeCache.cacheKey);
        }
        return CommonUtils.msg(count);
    }

    @Override
    public R bactchRemoveOrg(String[] id) {

        boolean children = organizeManager.hasChildren(id);
        if (children) {
            return R.error(MsgConstant.MSG_HAS_CHILD);
        }
        int count = organizeManager.bactchRemoveOrg(id);
        if(count>0)
        {
            organizeCache.updateCache(organizeCache.cacheKey);
        }
        return CommonUtils.msg(id, count);
    }
    //endregion

    /**
     * 数据库加载列表
     *
     * @return
     */
    @Override
    public List<OrganizeEntity> list() {

        return organizeManager.listOrg();
    }

    /**
     * 实体创建初始化
     *
     * @param entity
     */
    @Override
    public void create(OrganizeEntity entity) {
        super.create(entity);
        entity.setDeleteMark(0);
        entity.setEnabledMark(1);
        entity.setOrganizeId(UUID.randomUUID().toString());
    }

    /**
     * 实体修改初始化
     *
     * @param entity
     */
    @Override
    public void modify(OrganizeEntity entity) {
        super.modify(entity);
    }
}
