package com.xnet.shiro.manager.base;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.DataItemEntity;

/**
 * 数据字典分类表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月13日 下午3:14:19
 */
public interface DataItemManager {

	List<DataItemEntity> listDataItem(Page<DataItemEntity> page, Query search);
	
	int saveDataItem(DataItemEntity dataItem);
	
	DataItemEntity getDataItemById(String id);
	
	int updateDataItem(DataItemEntity dataItem);
	
	int batchRemove(String[] id);

	List<DataItemEntity> listAll();
	
}
