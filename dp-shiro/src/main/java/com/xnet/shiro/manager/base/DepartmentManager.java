package com.xnet.shiro.manager.base;

import com.xnet.shiro.entity.base.DepartmentEntity;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import java.util.List;

/**
 * 部门接口层
 *
 * @author Xingyc
 * @date 2018-1-11 11:57:29
 */
public interface DepartmentManager {

    /**
     * 获取部门列表
     * @param organizeId
     * @param keyword
     * @return
     */
    public List<DepartmentEntity> getListByOrganize(String organizeId, String keyword);

    List<DepartmentEntity> listDepartment(Page<DepartmentEntity> page, Query search);

    List<DepartmentEntity> listDepartment(String organizeId, String keyword);

    int saveDepartment(DepartmentEntity department);

    DepartmentEntity getDepartmentById(String id);

    int updateDepartment(DepartmentEntity department);

    int batchRemove(String[] id);

    /**
     * 获取所有部门列表
     * @param
     * @return
     */
    List<DepartmentEntity> listAll();
}
