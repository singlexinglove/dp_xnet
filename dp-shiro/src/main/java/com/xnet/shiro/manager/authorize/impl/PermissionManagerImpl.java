package com.xnet.shiro.manager.authorize.impl;

import com.xnet.common.constant.SystemConstant;
import com.xnet.common.utils.ShiroUtils;
import com.xnet.shiro.dao.authorize.PermissionMapper;
import com.xnet.shiro.dao.base.UserRelationMapper;
import com.xnet.shiro.entity.authorize.*;
import com.xnet.shiro.entity.base.UserRelationEntity;
import com.xnet.shiro.manager.authorize.PermissionManager;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.plaf.synth.Region;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("permissionManager")
public class PermissionManagerImpl implements PermissionManager {


    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private UserRelationMapper userRelationMapper ;

    /**
     * 菜单列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeEntity> listModule(String objectId) {

        return permissionMapper.listByType(objectId, SystemConstant.AuthorizeTypeEnum.Module.getValue());
    }

    /**
     * 菜单按钮列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeEntity> listModuleButton(String objectId) {

        return permissionMapper.listByType(objectId, SystemConstant.AuthorizeTypeEnum.Button.getValue());
    }

    /**
     * 菜单表格列列表
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeEntity> listModuleColumn(String objectId) {

        return permissionMapper.listByType(objectId, SystemConstant.AuthorizeTypeEnum.Column.getValue());
    }

    /**
     * 数据权限
     *
     * @param objectId
     * @return
     */
    @Override
    public List<AuthorizeDataEntity> listAuthorizeData(String objectId) {

        return permissionMapper.listAuthorizeData(objectId);
    }


    /**
     *获取登录用户可分配的菜单列表
     * @param userId
     * @return
     */
    @Override
    public List<ModuleEntity> listModuleByUser(String userId){
        return permissionMapper.listModuleByUser(userId);
    }

    /**
     *获取登录用户可分配的菜单按钮列表
     * @param userId
     * @return
     */
    @Override
    public List<ModuleButtonEntity> listModuleButtonByUser(String userId){
        return permissionMapper.listModuleButtonByUser(userId);
    }

    /**
     *获取登录用户可分配的数据列数据列表
     * @param userId
     * @return
     */
    @Override
    public List<ModuleColumnEntity> listModuleColumnByUser(String userId){
        return permissionMapper.listModuleColumnByUser(userId);
    }

    /**
     * 获取成员列表
     * @param objectId
     * @return
     */
    @Override
    public List<UserRelationEntity> listMember(String objectId)
    {
        return userRelationMapper.listMember(objectId);
    }

    /**
     * 获取对象列表
     * @param userId
     * @return
     */
    @Override
    public List<UserRelationEntity> listObject(String userId)
    {
        return userRelationMapper.listObject(userId);
    }

    /**
     * 保存权限
     *
     * @param authorizeType
     * @param objectId
     * @param moduleIds
     * @param moduleButtonIds
     * @param moduleColumnIds
     * @param authorizeDataList
     * @return
     */
    @Override
    @Transactional
    public int saveAuthorize(SystemConstant.AuthorizeCategoryEnum authorizeType, String objectId, String[] moduleIds, String[] moduleButtonIds, String[] moduleColumnIds, List<AuthorizeDataEntity> authorizeDataList) {

        permissionMapper.removeAuthorize(objectId);

        //region 菜单
        int sortCode = 1;
        for (String item : moduleIds) {
            AuthorizeEntity authorizeEntity = new AuthorizeEntity();
            authorizeEntity.setAuthorizeId(UUID.randomUUID().toString());
            authorizeEntity.setCreateDate(new Date());
            authorizeEntity.setCreateUserId(ShiroUtils.getUserId());
            authorizeEntity.setCreateUserId(ShiroUtils.getUserEntity().getAccount());
            authorizeEntity.setCategory(authorizeType.getValue());
            authorizeEntity.setObjectId(objectId);
            authorizeEntity.setItemType(SystemConstant.AuthorizeTypeEnum.Module.getValue());
            authorizeEntity.setItemId(item);
            authorizeEntity.setSortCode(sortCode++);
            permissionMapper.saveAuthorize(authorizeEntity);
        }
        //endregion

        //region 按钮
         sortCode = 1;
        for (String item : moduleButtonIds) {
            AuthorizeEntity authorizeEntity = new AuthorizeEntity();
            authorizeEntity.setAuthorizeId(UUID.randomUUID().toString());
            authorizeEntity.setCreateDate(new Date());
            authorizeEntity.setCreateUserId(ShiroUtils.getUserId());
            authorizeEntity.setCreateUserId(ShiroUtils.getUserEntity().getAccount());
            authorizeEntity.setCategory(authorizeType.getValue());
            authorizeEntity.setObjectId(objectId);
            authorizeEntity.setItemType(SystemConstant.AuthorizeTypeEnum.Button.getValue());
            authorizeEntity.setItemId(item);
            authorizeEntity.setSortCode(sortCode++);
            permissionMapper.saveAuthorize(authorizeEntity);
        }
        //endregion

        //region 列
        sortCode = 1;
        for (String item : moduleColumnIds) {
            AuthorizeEntity authorizeEntity = new AuthorizeEntity();
            authorizeEntity.setAuthorizeId(UUID.randomUUID().toString());
            authorizeEntity.setCreateDate(new Date());
            authorizeEntity.setCreateUserId(ShiroUtils.getUserId());
            authorizeEntity.setCreateUserId(ShiroUtils.getUserEntity().getAccount());
            authorizeEntity.setCategory(authorizeType.getValue());
            authorizeEntity.setObjectId(objectId);
            authorizeEntity.setItemType(SystemConstant.AuthorizeTypeEnum.Column.getValue());
            authorizeEntity.setItemId(item);
            authorizeEntity.setSortCode(sortCode++);
            permissionMapper.saveAuthorize(authorizeEntity);
        }
        //endregion

        //region 数据权限

        permissionMapper.removeAuthorizeData(objectId);
        sortCode = 1;
        for(AuthorizeDataEntity authorizeDataEntity:authorizeDataList)
        {
            authorizeDataEntity.setCreateDate(new Date());
            authorizeDataEntity.setAuthorizeDataId(UUID.randomUUID().toString());
            authorizeDataEntity.setCreateUserId(ShiroUtils.getUserId());
            authorizeDataEntity.setCreateUserId(ShiroUtils.getUserEntity().getAccount());
            authorizeDataEntity.setCategory(authorizeType.getValue());
            authorizeDataEntity.setObjectId(objectId);
            authorizeDataEntity.setSortCode(sortCode++);
            permissionMapper.saveAuthorizeData(authorizeDataEntity);
        }

        //endregion

        return 1;

    }

    /**
     * 保存成员用户分配
     * @param authorizeType
     * @param objectId
     * @param userIds
     * @return
     */
    @Override
    @Transactional
    public int saveMember(SystemConstant.AuthorizeCategoryEnum authorizeType, String objectId, String[] userIds)
    {
        userRelationMapper.batchRemoveByObjectId(objectId,0);
        int sortCode = 1;
        for(String userId:userIds)
        {
            UserRelationEntity userRelationEntity = new UserRelationEntity();
            userRelationEntity.setUserRelationId(UUID.randomUUID().toString());
            userRelationEntity.setCreateDate(new Date());
            userRelationEntity.setCreateUserId(ShiroUtils.getUserId());
            userRelationEntity.setCreateUserName(ShiroUtils.getUserEntity().getAccount());
            userRelationEntity.setCategory(authorizeType.getValue());
            userRelationEntity.setObjectId(objectId);
            userRelationEntity.setUserId(userId);
            userRelationEntity.setIsDefault(0);
            userRelationEntity.setSortCode(sortCode++);
            userRelationMapper.save(userRelationEntity);

        }
        return 1;

    }

}
