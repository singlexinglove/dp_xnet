package com.xnet.shiro.manager.authorize;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;

/**
 * 功能表格列表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
public interface ModuleColumnManager {

	List<ModuleColumnEntity> listModuleColumn(Page<ModuleColumnEntity> page, Query search);
	
	int saveModuleColumn(ModuleColumnEntity moduleColumn);
	
	ModuleColumnEntity getModuleColumnById(String id);
	
	int updateModuleColumn(ModuleColumnEntity moduleColumn);
	
	int batchRemove(String[] id);

	List<ModuleColumnEntity> listModuleId(String moduleId);

	List<ModuleColumnEntity> listAll();
}
