package com.xnet.shiro.manager.base.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.base.AreaMapper;
import com.xnet.shiro.entity.base.AreaEntity;
import com.xnet.shiro.manager.base.AreaManager;

/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
@Component("areaManager")
public class AreaManagerImpl implements AreaManager {

	@Autowired
	private AreaMapper areaMapper;
	

	@Override
	public List<AreaEntity> listArea(Page<AreaEntity> page, Query search) {
		return areaMapper.listForPage(page, search);
	}

	@Override
	public int saveArea(AreaEntity area) {
		return areaMapper.save(area);
	}

	@Override
	public AreaEntity getAreaById(String id) {
		AreaEntity area = areaMapper.getObjectById(id);
		return area;
	}

	@Override
	public int updateArea(AreaEntity area) {
		return areaMapper.update(area);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = areaMapper.batchRemove(id);
		return count;
	}

	//根据父类id获取区划列表
	public 	List<AreaEntity> getAreaListByParentId(String parentId)
	{
		return areaMapper.getAreaListByParentId(parentId);
	}
}
