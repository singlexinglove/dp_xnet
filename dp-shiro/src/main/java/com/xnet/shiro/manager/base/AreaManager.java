package com.xnet.shiro.manager.base;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.AreaEntity;

/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
public interface AreaManager {

	List<AreaEntity> listArea(Page<AreaEntity> page, Query search);
	
	int saveArea(AreaEntity area);
	
	AreaEntity getAreaById(String id);
	
	int updateArea(AreaEntity area);
	
	int batchRemove(String[] id);

	List<AreaEntity> getAreaListByParentId(String parentId);
}
