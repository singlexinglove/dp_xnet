package com.xnet.shiro.manager.base.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.base.DataItemMapper;
import com.xnet.shiro.entity.base.DataItemEntity;
import com.xnet.shiro.manager.base.DataItemManager;

/**
 * 数据字典分类表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月13日 下午3:14:19
 */
@Component("dataItemManager")
public class DataItemManagerImpl implements DataItemManager {

	@Autowired
	private DataItemMapper dataItemMapper;
	

	@Override
	public List<DataItemEntity> listDataItem(Page<DataItemEntity> page, Query search) {
		return dataItemMapper.listForPage(page, search);
	}

	@Override
	public int saveDataItem(DataItemEntity dataItem) {
		return dataItemMapper.save(dataItem);
	}

	@Override
	public DataItemEntity getDataItemById(String id) {
		DataItemEntity dataItem = dataItemMapper.getObjectById(id);
		return dataItem;
	}

	@Override
	public int updateDataItem(DataItemEntity dataItem) {
		return dataItemMapper.update(dataItem);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = dataItemMapper.batchRemove(id);
		return count;
	}

	@Override
	public List<DataItemEntity> listAll()
	{
		return dataItemMapper.list();
	}
	
}
