package com.xnet.shiro.manager.authorize;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;

/**
 * 功能按钮表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
public interface ModuleButtonManager {

	List<ModuleButtonEntity> listModuleButton(Page<ModuleButtonEntity> page, Query search);
	
	int saveModuleButton(ModuleButtonEntity moduleButton);
	
	ModuleButtonEntity getModuleButtonById(String id);
	
	int updateModuleButton(ModuleButtonEntity moduleButton);
	
	int batchRemove(String[] id);

	List<ModuleButtonEntity> listModuleId(String moduleId);

	List<ModuleButtonEntity> listAll();
}
