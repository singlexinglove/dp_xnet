package com.xnet.shiro.manager.base;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.CodeRuleEntity;

/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
public interface CodeRuleManager {

	List<CodeRuleEntity> listCodeRule(Page<CodeRuleEntity> page, Query search);
	
	int saveCodeRule(CodeRuleEntity codeRule);
	
	CodeRuleEntity getCodeRuleById(String id);
	
	int updateCodeRule(CodeRuleEntity codeRule);
	
	int batchRemove(String[] id);
	
}
