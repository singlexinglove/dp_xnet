package com.xnet.shiro.manager.authorize.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.authorize.ModuleButtonMapper;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.manager.authorize.ModuleButtonManager;

/**
 * 功能按钮表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
@Component("moduleButtonManager")
public class ModuleButtonManagerImpl implements ModuleButtonManager {

	@Autowired
	private ModuleButtonMapper moduleButtonMapper;
	

	@Override
	public List<ModuleButtonEntity> listModuleButton(Page<ModuleButtonEntity> page, Query search) {
		return moduleButtonMapper.listForPage(page, search);
	}

	@Override
	public int saveModuleButton(ModuleButtonEntity moduleButton) {
		return moduleButtonMapper.save(moduleButton);
	}

	@Override
	public ModuleButtonEntity getModuleButtonById(String id) {
		ModuleButtonEntity moduleButton = moduleButtonMapper.getObjectById(id);
		return moduleButton;
	}

	@Override
	public int updateModuleButton(ModuleButtonEntity moduleButton) {
		return moduleButtonMapper.update(moduleButton);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = moduleButtonMapper.batchRemove(id);
		return count;
	}

	@Override
	public List<ModuleButtonEntity> listModuleId(String moduleId)
	{
		return moduleButtonMapper.listModuleId(moduleId);
	}

	@Override
	public 	List<ModuleButtonEntity> listAll()
	{
		return moduleButtonMapper.listAll();

	}
	
}
