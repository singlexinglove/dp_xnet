package com.xnet.shiro.manager.base;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.RoleEntity;

import java.util.List;

/**
 * 系统角色
 *
 * @author Xingyc
 * @date 2018-1-10 08:34:28
 */
public interface RoleManager {

	List<RoleEntity> listRole(Page<RoleEntity> page, Query search);
	
	int saveRole(RoleEntity role);

	RoleEntity getRoleById(String id);
	
	int updateRole(RoleEntity role);
	
	int batchRemove(String[] id);
	
	List<RoleEntity> listRole(Query query);
	
	int updateRoleOptAuthorization(RoleEntity role);
	
	int updateRoleDataAuthorization(RoleEntity role);
	
}
