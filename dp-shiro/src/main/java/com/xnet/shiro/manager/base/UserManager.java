package com.xnet.shiro.manager.base;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.UserEntity;

import java.util.List;
import java.util.Set;

/**
 * 系统用户
 *
 * @author Xingyc
 * @date 2018-1-5 11:08:27
 */
public interface UserManager {

	UserEntity getByUserName(String username);
	
	List<UserEntity> listUser(Page<UserEntity> page, Query search);
	
	int saveUser(UserEntity user);

	UserEntity getById(String userId);
	
	int updateUser(UserEntity user);
	
	int batchRemove(String[] id);
	
	Set<String> listUserPerms(String userId);
	
	Set<String> listUserRoles(String userId);
	
	int updatePswdByUser(Query query);
	
	int updateUserEnable(String[] id);
	
	int updateUserDisable(String[] id);
	
	int updatePswd(UserEntity user);

	UserEntity getUserById(String userId);

	List<UserEntity> listAllUser();

	List<UserEntity> getListByDepartmentId(String departmentId);
}
