package com.xnet.shiro.manager.base;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.DataItemDetailEntity;

/**
 * 数据字典明细
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月08日 下午1:49:58
 */
public interface DataItemDetailManager {

	List<DataItemDetailEntity> listDataItemDetail(Page<DataItemDetailEntity> page, Query search);
	
	int saveDataItemDetail(DataItemDetailEntity dataItemDetail);
	
	DataItemDetailEntity getDataItemDetailById(String id);
	
	int updateDataItemDetail(DataItemDetailEntity dataItemDetail);
	
	int batchRemove(String[] id);

	List<DataItemDetailEntity> getDataItemList();

	List<DataItemDetailEntity> getListByItemId(String itemId);
}
