package com.xnet.shiro.manager.base.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.base.DataItemDetailMapper;
import com.xnet.shiro.entity.base.DataItemDetailEntity;
import com.xnet.shiro.manager.base.DataItemDetailManager;

/**
 * 数据字典明细
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月08日 下午1:49:58
 */
@Component("dataItemDetailManager")
public class DataItemDetailManagerImpl implements DataItemDetailManager {

	@Autowired
	private DataItemDetailMapper dataItemDetailMapper;
	

	@Override
	public List<DataItemDetailEntity> listDataItemDetail(Page<DataItemDetailEntity> page, Query search) {
		return dataItemDetailMapper.listForPage(page, search);
	}

	@Override
	public int saveDataItemDetail(DataItemDetailEntity dataItemDetail) {
		return dataItemDetailMapper.save(dataItemDetail);
	}

	@Override
	public DataItemDetailEntity getDataItemDetailById(String id) {
		DataItemDetailEntity dataItemDetail = dataItemDetailMapper.getObjectById(id);
		return dataItemDetail;
	}

	@Override
	public int updateDataItemDetail(DataItemDetailEntity dataItemDetail) {
		return dataItemDetailMapper.update(dataItemDetail);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = dataItemDetailMapper.batchRemove(id);
		return count;
	}
	//获取字典项目明细
	public List<DataItemDetailEntity> getDataItemList()
	{
		return dataItemDetailMapper.getDataItemList();
	}

	/**
	 * 根据id获取对应的明细字典
	 * @param itemId
	 * @return
	 */
	@Override
	public List<DataItemDetailEntity> getListByItemId(String itemId)
	{
		return dataItemDetailMapper.getListByItemId(itemId);
	}

}
