package com.xnet.shiro.manager.base;

import com.xnet.shiro.entity.base.OrganizeEntity;

import java.util.List;

/**
 * 组织机构
 *
 * @author Xingyc
 * @date 2018-1-11 11:57:29
 */
public interface OrganizeManager {

	List<OrganizeEntity> listOrg();
	
	int saveOrg(OrganizeEntity org);
	
	OrganizeEntity getOrg(String orgId);
	
	int updateOrg(OrganizeEntity org);
	
	int bactchRemoveOrg(String[] id);
	
	boolean hasChildren(String[] id);

	public List<OrganizeEntity> GetListByNature(String keyword);
	
}
