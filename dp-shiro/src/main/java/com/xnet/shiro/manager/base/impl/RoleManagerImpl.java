package com.xnet.shiro.manager.base.impl;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.base.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xnet.shiro.dao.base.RoleMapper;
import com.xnet.shiro.dao.base.AuthorizeMapper;
import com.xnet.shiro.dao.base.UserRelationMapper;
import com.xnet.shiro.manager.base.RoleManager;

import java.util.List;

/**
 * 系统角色
 *
 * @author Xingyc
 * @date 2018-1-9 17:38:48
 */
@Component("sysRoleManager")
public class RoleManagerImpl implements RoleManager {

	@Autowired
	private RoleMapper roleMapper;
	
	@Autowired
	private UserRelationMapper userRelationMapper;
	
	@Autowired
	private AuthorizeMapper authorizeMapper;
	
//	@Autowired
//	private SysRoleOrgMapper sysRoleOrgMapper;

	@Override
	public List<RoleEntity> listRole(Page<RoleEntity> page, Query search) {
		return roleMapper.listForPage(page, search);
	}

	@Override
	public int saveRole(RoleEntity role) {
		return roleMapper.save(role);
	}

	@Override
	public RoleEntity getRoleById(String id) {
		RoleEntity role = roleMapper.getObjectById(id);
//		List<String> menuId = authorizeMapper.listMenuId(id);
//		List<String> orgId = sysRoleOrgMapper.listOrgId(id);
//		role.setMenuIdList(menuId);
//		role.setOrgIdList(orgId);
		return role;
	}

	@Override
	public int updateRole(RoleEntity role) {
		return roleMapper.update(role);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = roleMapper.batchRemove(id);
		userRelationMapper.batchRemoveByRoleId(id);
		authorizeMapper.batchRemoveByRoleId(id);
//		sysRoleOrgMapper.batchRemoveByRoleId(id);
		return count;
	}

	@Override
	public List<RoleEntity> listRole(Query query) {
		return roleMapper.list(query);
	}

	@Override
	public int updateRoleOptAuthorization(RoleEntity role) {
		String roleId = role.getRoleId();
		int count = authorizeMapper.remove(roleId);
		Query query = new Query();
		query.put("roleId", roleId);
		List<String> menuId = role.getMenuIdList();
		if(menuId.size() > 0) {
			query.put("menuIdList", role.getMenuIdList());
			count = authorizeMapper.save(query);
		}
		return count;
	}

	@Override
	public int updateRoleDataAuthorization(RoleEntity role) {
//		String roleId = role.getRoleId();
//		int count = sysRoleOrgMapper.remove(roleId);
//		Query query = new Query();
//		query.put("roleId", roleId);
//		List<Long> orgId = role.getOrgIdList();
//		if(orgId.size() > 0) {
//			query.put("orgIdList", role.getOrgIdList());
//			count = sysRoleOrgMapper.save(query);
//		}
//		return count;
		return 0;
	}
	
}
