package com.xnet.shiro.manager.authorize;

import java.util.List;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import com.xnet.shiro.entity.authorize.ModuleEntity;

/**
 * 系统功能表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月07日 上午10:31:35
 */
public interface ModuleManager {

	List<ModuleEntity> listModule(Page<ModuleEntity> page, Query search);

	int saveModule(ModuleEntity module, List<ModuleButtonEntity> moduleButtonList, List<ModuleColumnEntity> moduleColumnList);

	ModuleEntity getModuleById(String id);

	int updateModule(ModuleEntity module);

	int batchRemove(String[] id);

	List<ModuleEntity> listMenu(Query search);

	List<ModuleEntity> listUserMenu(String userId);

	List<ModuleEntity> listParentId(String parentId, List<String> menuIdList);

	List<ModuleEntity> listNotButton();

	List<ModuleEntity> listAll();

	int saveMenu(ModuleEntity menu);

	ModuleEntity getMenuById(String id);

	int updateMenu(ModuleEntity menu);

	boolean hasChildren(String[] id);

}
