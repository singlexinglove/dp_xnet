package com.xnet.shiro.manager.authorize.impl;

import com.xnet.common.entity.Page;
import com.xnet.shiro.dao.authorize.ModuleButtonMapper;
import com.xnet.shiro.dao.authorize.ModuleColumnMapper;
import com.xnet.shiro.dao.base.UserMapper;
import com.xnet.shiro.entity.authorize.ModuleButtonEntity;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import com.xnet.shiro.entity.authorize.ModuleEntity;
import com.xnet.shiro.manager.authorize.ModuleManager;
import com.xnet.common.constant.SystemConstant.MenuType;
import com.xnet.common.entity.Query;
import com.xnet.common.utils.CommonUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xnet.shiro.dao.authorize.ModuleMapper;
import com.xnet.shiro.dao.base.AuthorizeMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 系统菜单
 * 
 * @author  Xingyc
 * @date 2018-1-9 17:39:54
 */
@Component("sysMenuManager")
@Transactional
public class ModuleManagerImpl implements ModuleManager {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private ModuleMapper moduleMapper;
	
	@Autowired
	private AuthorizeMapper authorizeMapper;


	@Autowired
	private ModuleButtonMapper moduleButtonMapper;

	@Autowired
	private ModuleColumnMapper moduleColumnMapper;

	@Override
	public List<ModuleEntity> listModule(Page<ModuleEntity> page, Query search) {
		return moduleMapper.listForPage(page, search);
	}

	/**
	 * 新增保存操作
	 * @param module
	 * @param moduleButtonList
	 * @param moduleColumnList
	 * @return
	 */
	@Override
	@Transactional
	public int saveModule(ModuleEntity module, List<ModuleButtonEntity> moduleButtonList, List<ModuleColumnEntity> moduleColumnList) {

		String moduleId= UUID.randomUUID().toString();
		if(StringUtils.isBlank(module.getModuleId()))
		{
			module.setModuleId(moduleId);

			//新增保存菜单项目
			module.setCreateDate(new Date());
			moduleMapper.save(module);
		}
		else
		{
			moduleId=module.getModuleId();
			module.setModifyDate(new Date());
			moduleMapper.update(module);
		}
		String finalModuleId=moduleId;
		moduleButtonList.stream().forEach(x->{
			x.setModuleId(finalModuleId);
		    x.setModuleButtonId(UUID.randomUUID().toString());});
		moduleColumnList.stream().forEach(x->{
			x.setModuleId(finalModuleId);
			x.setModuleColumnId(UUID.randomUUID().toString());
		});

		//先删除再插入按钮列表

		moduleButtonMapper.deleteModuleId(moduleId);
		if(moduleButtonList!=null&&!moduleButtonList.isEmpty())
		{
			moduleButtonMapper.batchInsert(moduleButtonList);
		}
		//先删除再插入数据列列表
		moduleColumnMapper.deleteModuleId(moduleId);
		if(moduleColumnList!=null&&!moduleColumnList.isEmpty())
		{
			moduleColumnMapper.batchInsert(moduleColumnList);
		}
		return 1;
	}

	@Override
	public ModuleEntity getModuleById(String id) {
		ModuleEntity module = moduleMapper.getObjectById(id);
		return module;
	}

	@Override
	public int updateModule(ModuleEntity module) {
		return moduleMapper.update(module);
	}


	@Override
	public List<ModuleEntity> listUserMenu(String userId) {
		List<String> menuIdList = userMapper.listAllMenuId(userId);
		return getAllMenuList(menuIdList);
	}
	
	/**
	 * 获取所有菜单列表
	 */
	private List<ModuleEntity> getAllMenuList(List<String> menuIdList){
		//查询根菜单列表
		List<ModuleEntity> menuList = listParentId("0", menuIdList);
		//递归获取子菜单
		getMenuTreeList(menuList, menuIdList);
		
		return menuList;
	}

	/**
	 * 递归
	 */
	private List<ModuleEntity> getMenuTreeList(List<ModuleEntity> menuList, List<String> menuIdList){
		List<ModuleEntity> subMenuList = new ArrayList<>();
		
		for(ModuleEntity entity : menuList){
			if(entity.getIsMenu() == MenuType.CATALOG.getValue()){//目录
				entity.setList(getMenuTreeList(listParentId(entity.getModuleId(), menuIdList), menuIdList));
			}
			subMenuList.add(entity);
		}
		return subMenuList;
	}

	@Override
	public List<ModuleEntity> listParentId(String parentId, List<String> menuIdList) {
		List<ModuleEntity> menuList = moduleMapper.listParentId(parentId);
		if(menuIdList == null){
			return menuList;
		}
		
		List<ModuleEntity> userMenuList = new ArrayList<>();
		for(ModuleEntity menu : menuList){
			if(menuIdList.contains(menu.getModuleId())){
				userMenuList.add(menu);
			}
		}
		return userMenuList;
	}

	@Override
	public List<ModuleEntity> listMenu(Query search) {
		return moduleMapper.list(search);
	}

	@Override
	public List<ModuleEntity> listAll() {
		return moduleMapper.list();
	}



	@Override
	public List<ModuleEntity> listNotButton() {
		return moduleMapper.listNotButton();
	}

	@Override
	public int saveMenu(ModuleEntity menu) {
		return moduleMapper.save(menu);
	}

	@Override
	public ModuleEntity getMenuById(String id) {
		return moduleMapper.getObjectById(id);
	}

	@Override
	public int updateMenu(ModuleEntity menu) {
		return moduleMapper.update(menu);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = moduleMapper.batchRemove(id);
		//authorizeMapper.batchRemoveByMenuId(id);
		return count;
	}

	@Override
	public boolean hasChildren(String[] id) {
		for(String parentId : id) {
			int count = moduleMapper.countMenuChildren(parentId);
			if(CommonUtils.isIntThanZero(count)) {
				return true;
			}
		}
		return false;
	}


}
