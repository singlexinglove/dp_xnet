package com.xnet.shiro.manager.authorize;

import com.xnet.common.constant.SystemConstant;
import com.xnet.shiro.entity.authorize.*;
import com.xnet.shiro.entity.base.UserRelationEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PermissionManager {

    /**
     * 菜单列表
     * @param objectId
     * @return
     */
    List<AuthorizeEntity> listModule(String objectId);

    /**
     * 菜单按钮列表
     * @param objectId
     * @return
     */
    List<AuthorizeEntity> listModuleButton(String objectId);

    /**
     * 菜单表格列列表
     * @param objectId
     * @return
     */
    List<AuthorizeEntity> listModuleColumn(String objectId);

    /**
     *数据权限
     * @param objectId
     * @return
     */
    List<AuthorizeDataEntity> listAuthorizeData(String objectId);

    /**
     *获取登录用户可分配的菜单列表
     * @param userId
     * @return
     */
    List<ModuleEntity> listModuleByUser(@Param("userId") String userId);

    /**
     *获取登录用户可分配的菜单按钮列表
     * @param userId
     * @return
     */
    List<ModuleButtonEntity> listModuleButtonByUser(@Param("userId") String userId);

    /**
     *获取登录用户可分配的数据列数据列表
     * @param userId
     * @return
     */
    List<ModuleColumnEntity> listModuleColumnByUser(@Param("userId") String userId);

    /**
     * 获取成员列表
     * @param objectId
     * @return
     */
    List<UserRelationEntity> listMember(String objectId);

    /**
     * 获取对象列表
     * @param userId
     * @return
     */
    List<UserRelationEntity> listObject(String userId);


    /**
     * 保存权限
     * @param authorizeType
     * @param objectId
     * @param moduleIds
     * @param moduleButtonIds
     * @param moduleColumnIds
     * @param authorizeDataList
     * @return
     */
    int saveAuthorize(SystemConstant.AuthorizeCategoryEnum authorizeType, String objectId, String[] moduleIds, String[] moduleButtonIds, String[] moduleColumnIds, List<AuthorizeDataEntity> authorizeDataList);


    /**
     * 保存成员用户分配
     * @param authorizeType
     * @param objectId
     * @param userIds
     * @return
     */
    int saveMember(SystemConstant.AuthorizeCategoryEnum authorizeType, String objectId, String[] userIds);

}
