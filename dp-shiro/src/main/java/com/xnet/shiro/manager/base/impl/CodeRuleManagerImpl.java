package com.xnet.shiro.manager.base.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.base.CodeRuleMapper;
import com.xnet.shiro.entity.base.CodeRuleEntity;
import com.xnet.shiro.manager.base.CodeRuleManager;

/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
@Component("codeRuleManager")
public class CodeRuleManagerImpl implements CodeRuleManager {

	@Autowired
	private CodeRuleMapper codeRuleMapper;
	

	@Override
	public List<CodeRuleEntity> listCodeRule(Page<CodeRuleEntity> page, Query search) {
		return codeRuleMapper.listForPage(page, search);
	}

	@Override
	public int saveCodeRule(CodeRuleEntity codeRule) {
		return codeRuleMapper.save(codeRule);
	}

	@Override
	public CodeRuleEntity getCodeRuleById(String id) {
		CodeRuleEntity codeRule = codeRuleMapper.getObjectById(id);
		return codeRule;
	}

	@Override
	public int updateCodeRule(CodeRuleEntity codeRule) {
		return codeRuleMapper.update(codeRule);
	}

	@Override
	public int batchRemove(String[] id) {
		int count = codeRuleMapper.batchRemove(id);
		return count;
	}
	
}
