package com.xnet.shiro.manager.base.impl;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.base.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xnet.shiro.entity.base.DepartmentEntity;
import com.xnet.shiro.manager.base.DepartmentManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门管理
 *
 * @author Xingyc
 * @date 2018-1-11 11:56:20
 */
@Component("departmentManager")
public class DepartmentManagerImpl implements DepartmentManager {

    @Autowired
    private DepartmentMapper departmentMapper;

    /**
     * 通过组织机构获取对应的部门列表(包含子级节点)
     *
     * @param organizeId
     * @param keyword
     * @return
     */
    @Override
    public List<DepartmentEntity> getListByOrganize(String organizeId, String keyword) {

        Query query = new Query();
        query.put("organizeId", organizeId);
        query.put("keyword", keyword);
        List<DepartmentEntity> deptList = departmentMapper.getList(query);
        List<DepartmentEntity> resultList = new ArrayList<>();

        if (deptList.isEmpty()) return resultList;
        //过滤无父类的部门列表
        List<DepartmentEntity> rootDeptList = deptList.stream().filter(x -> x.getParentId().equals("0")).collect(Collectors.toList());
        rootDeptList.forEach(x -> x.setParentId("0"));

        for (DepartmentEntity dept : rootDeptList) {
            resultList.add(dept);
            getChildren(dept, deptList, resultList);
        }
        return resultList;
    }

    /**
     * 通过组织机构获取对应的部门列表(全部列表)
     *
     * @param organizeId
     * @param keyword
     * @return
     */
    @Override
    public List<DepartmentEntity> listDepartment(String organizeId, String keyword) {

        Query query = new Query();
        query.put("organizeId", organizeId);
        query.put("keyword", keyword);
        return departmentMapper.getList(query);
    }


    /**
     * 获取所有子节点（递归）
     *
     * @param dept
     * @param deptList
     * @param resultList
     */
    private void getChildren(DepartmentEntity dept, List<DepartmentEntity> deptList, List<DepartmentEntity> resultList) {

        List<DepartmentEntity> temp = deptList.stream().filter(t -> t.getParentId().equals(dept.getDepartmentId())).collect(Collectors.toList());
        if (!temp.isEmpty()) {
            resultList.addAll(temp);
            for (DepartmentEntity item : temp) {
                getChildren(item, deptList, resultList);
            }
        }
    }

    @Override
    public List<DepartmentEntity> listDepartment(Page<DepartmentEntity> page, Query search) {

        return departmentMapper.listForPage(page, search);
    }

    @Override
    public int saveDepartment(DepartmentEntity department) {

        return departmentMapper.save(department);
    }

    @Override
    public DepartmentEntity getDepartmentById(String id) {

        DepartmentEntity department = departmentMapper.getObjectById(id);
        return department;
    }

    @Override
    public int updateDepartment(DepartmentEntity department) {

        return departmentMapper.update(department);
    }

    @Override
    public int batchRemove(String[] id) {

        int count = departmentMapper.batchRemove(id);
        return count;
    }

    /**
     * 获取所有部门列表
     *
     * @param
     * @return
     */
    @Override
    public List<DepartmentEntity> listAll() {

        return departmentMapper.listAll();
    }
}
