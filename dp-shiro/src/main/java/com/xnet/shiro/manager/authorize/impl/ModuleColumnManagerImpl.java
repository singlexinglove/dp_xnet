package com.xnet.shiro.manager.authorize.impl;

import java.util.List;

import com.xnet.shiro.dao.authorize.ModuleButtonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.shiro.dao.authorize.ModuleColumnMapper;
import com.xnet.shiro.entity.authorize.ModuleColumnEntity;
import com.xnet.shiro.manager.authorize.ModuleColumnManager;

/**
 * 功能表格列表
 *
 * @author xyc
 * @email
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
@Component("moduleColumnManager")
public class ModuleColumnManagerImpl implements ModuleColumnManager {

    @Autowired
    private ModuleColumnMapper moduleColumnMapper;

    @Override
    public List<ModuleColumnEntity> listModuleColumn(Page<ModuleColumnEntity> page, Query search) {

        return moduleColumnMapper.listForPage(page, search);
    }

    @Override
    public int saveModuleColumn(ModuleColumnEntity moduleColumn) {

        return moduleColumnMapper.save(moduleColumn);
    }

    @Override
    public ModuleColumnEntity getModuleColumnById(String id) {

        ModuleColumnEntity moduleColumn = moduleColumnMapper.getObjectById(id);
        return moduleColumn;
    }

    @Override
    public int updateModuleColumn(ModuleColumnEntity moduleColumn) {

        return moduleColumnMapper.update(moduleColumn);
    }

    @Override
    public int batchRemove(String[] id) {

        int count = moduleColumnMapper.batchRemove(id);
        return count;
    }

    @Override
    public List<ModuleColumnEntity> listModuleId(String moduleId) {

        return moduleColumnMapper.listModuleId(moduleId);
    }

    @Override
    public List<ModuleColumnEntity> listAll() {
        return moduleColumnMapper.listAll();
    }

}
