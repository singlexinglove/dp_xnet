package com.xnet.shiro.manager.base.impl;

import com.xnet.common.entity.UserEntity;
import com.xnet.common.utils.ShiroUtils;
import com.xnet.shiro.dao.base.OrganizeMapper;
import com.xnet.common.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xnet.shiro.entity.base.OrganizeEntity;
import com.xnet.shiro.manager.base.OrganizeManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 组织架构
 *
 * @author Xingyc
 * @date 2018-1-11 11:56:20
 */
@Component("sysOrgManager")
public class OrganizeManagerImpl implements OrganizeManager {

    @Autowired
    private OrganizeMapper organizeMapper;

//	@Autowired
//	private SysRoleOrgMapper sysRoleOrgMapper;

    @Override
    public List<OrganizeEntity> listOrg() {

        return organizeMapper.list();
    }

    @Override
    public int saveOrg(OrganizeEntity org) {

        return organizeMapper.save(org);
    }

    @Override
    public OrganizeEntity getOrg(String orgId) {

        return organizeMapper.getObjectById(orgId);
    }

    @Override
    public int updateOrg(OrganizeEntity org) {

        return organizeMapper.update(org);
    }

    @Override
    public int bactchRemoveOrg(String[] id) {

        int count = organizeMapper.batchRemove(id);
//		sysRoleOrgMapper.batchRemoveByOrgId(id);
        return count;
    }

    @Override
    public boolean hasChildren(String[] id) {

        for (String parentId : id) {
            int count = organizeMapper.countOrgChildren(parentId);
            if (CommonUtils.isIntThanZero(count)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取组织机构列表
     * @param keyword
     * @return
     */
    @Override
    public List<OrganizeEntity> GetListByNature(String keyword) {
//		if (OperatorProvider.Provider.Current().UserId == "System")
//		{
//			return this.GetList();
//		}
//		var companyId = OperatorProvider.Provider.Current().CompanyId;
        List<OrganizeEntity> resultList = new ArrayList<>();
        UserEntity currentUser = ShiroUtils.getUserEntity();
        List<OrganizeEntity> organizeListAll = organizeMapper.getList();
        List<OrganizeEntity> organizeList = organizeListAll.stream().filter(x -> x.getOrganizeId().equals(currentUser.getOrganizeId())).collect(Collectors.toList());
        if (organizeList.isEmpty()) return resultList;
        OrganizeEntity organize = organizeList.stream().findFirst().get();
        organize.setParentId("0");
        resultList.add(organize);
        getChildren(organize, organizeListAll, resultList);
//		resultList.addAll(resultList);
        return resultList;
    }

    /**
     * 获取所有子节点（递归）
     *
     * @param organize
     * @param organizeList
     * @param resultList
     */
    private void getChildren(OrganizeEntity organize, List<OrganizeEntity> organizeList, List<OrganizeEntity> resultList) {

        List<OrganizeEntity> temp = organizeList.stream().filter(t -> t.getParentId().equals(organize.getOrganizeId()) && t.getNature().equals(organize.getNature())).collect(Collectors.toList());
        if (!temp.isEmpty()) {
            resultList.addAll(temp);
            for (OrganizeEntity org : temp) {
                getChildren(org, organizeList, resultList);
            }
        }
    }

}
