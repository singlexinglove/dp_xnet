package com.xnet.shiro.entity.base;

import com.xnet.common.entity.BaseEntity;

import java.io.Serializable;
import java.util.Date;



/**
 * 编号规则表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:22:36
 */
public class CodeRuleEntity extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 编码规则主键
	 */
	private String ruleId;
	
	/**
	 * 系统功能Id
	 */
	private String moduleId;
	
	/**
	 * 系统功能
	 */
	private String moduleName;
	
	/**
	 * 编号
	 */
	private String enCode;
	
	/**
	 * 名称
	 */
	private String fullName;
	
	/**
	 * 方式（可编辑、自动）
	 */
	private Integer mode;
	
	/**
	 * 当前流水号
	 */
	private String currentNumber;
	
	/**
	 * 规则格式Json
	 */
	private String ruleFormatJson;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	
	/**
	 * 删除标记
	 */
	private Integer deleteMark;
	
	/**
	 * 有效标志
	 */
	private Integer enabledMark;
	
	/**
	 * 备注
	 */
	private String description;
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	
	/**
	 * 创建用户
	 */
	private String createUserName;
	
	/**
	 * 修改日期
	 */
	private Date modifyDate;
	
	/**
	 * 修改用户主键
	 */
	private String modifyUserId;
	
	/**
	 * 修改用户
	 */
	private String modifyUserName;
	

	public CodeRuleEntity() {
		super();
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
	
	public String getRuleId() {
		return ruleId;
	}
	
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	public String getModuleId() {
		return moduleId;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	
	public String getEnCode() {
		return enCode;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setMode(Integer mode) {
		this.mode = mode;
	}
	
	public Integer getMode() {
		return mode;
	}
	
	public void setCurrentNumber(String currentNumber) {
		this.currentNumber = currentNumber;
	}
	
	public String getCurrentNumber() {
		return currentNumber;
	}
	
	public void setRuleFormatJson(String ruleFormatJson) {
		this.ruleFormatJson = ruleFormatJson;
	}
	
	public String getRuleFormatJson() {
		return ruleFormatJson;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	
	public Integer getDeleteMark() {
		return deleteMark;
	}
	
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	
	public Integer getEnabledMark() {
		return enabledMark;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	public String getCreateUserName() {
		return createUserName;
	}
	
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public Date getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public String getModifyUserId() {
		return modifyUserId;
	}
	
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public String getModifyUserName() {
		return modifyUserName;
	}
	
}
