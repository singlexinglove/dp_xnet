package com.xnet.shiro.entity.base;

import java.io.Serializable;
import java.util.Date;
/**
 * 用户角色关系
 *
 * @author Xingyc
 * @date 2018-1-5 14:31:54
 */
public class UserRelationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //------------字段总数：9
    private  String userRelationId;
    private  String userId;
    private  Integer category;
    private  String objectId;
    private  Integer isDefault;
    private  Integer sortCode;
    private Date createDate;
    private  String createUserId;
    private  String createUserName;
//------------属性列表：9
    /**
     *用户关系主键
     *@return
     */
    public String getUserRelationId(){

        return userRelationId;
    }

    /**
     *用户关系主键
     *@param userRelationId
     */
    public void setUserRelationId(String userRelationId){

        this.userRelationId=userRelationId;
    }

    /**
     *用户主键
     *@return
     */
    public String getUserId(){

        return userId;
    }

    /**
     *用户主键
     *@param userId
     */
    public void setUserId(String userId){

        this.userId=userId;
    }

    /**
     *分类:1-部门2-角色3-岗位4-职位5-工作组
     *@return
     */
    public Integer getCategory(){

        return category;
    }

    /**
     *分类:1-部门2-角色3-岗位4-职位5-工作组
     *@param category
     */
    public void setCategory(Integer category){

        this.category=category;
    }

    /**
     *对象主键
     *@return
     */
    public String getObjectId(){

        return objectId;
    }

    /**
     *对象主键
     *@param objectId
     */
    public void setObjectId(String objectId){

        this.objectId=objectId;
    }

    /**
     *默认对象
     *@return
     */
    public Integer getIsDefault(){

        return isDefault;
    }

    /**
     *默认对象
     *@param isDefault
     */
    public void setIsDefault(Integer isDefault){

        this.isDefault=isDefault;
    }

    /**
     *排序码
     *@return
     */
    public Integer getSortCode(){

        return sortCode;
    }

    /**
     *排序码
     *@param sortCode
     */
    public void setSortCode(Integer sortCode){

        this.sortCode=sortCode;
    }

    /**
     *创建时间
     *@return
     */
    public Date getCreateDate(){

        return createDate;
    }

    /**
     *创建时间
     *@param createDate
     */
    public void setCreateDate(Date createDate){

        this.createDate=createDate;
    }

    /**
     *创建用户主键
     *@return
     */
    public String getCreateUserId(){

        return createUserId;
    }

    /**
     *创建用户主键
     *@param createUserId
     */
    public void setCreateUserId(String createUserId){

        this.createUserId=createUserId;
    }

    /**
     *创建用户
     *@return
     */
    public String getCreateUserName(){

        return createUserName;
    }

    /**
     *创建用户
     *@param createUserName
     */
    public void setCreateUserName(String createUserName){

        this.createUserName=createUserName;
    }




}
