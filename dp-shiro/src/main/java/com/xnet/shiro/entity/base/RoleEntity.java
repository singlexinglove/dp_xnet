package com.xnet.shiro.entity.base;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色实体
 *
 * @author Xingyc
 * @date 2018-1-5 11:20:00
 */

public class RoleEntity  implements Serializable {

    private static final long serialVersionUID = 1L;

    //------------字段总数：17
    private  String roleId;
    private  String organizeId;
    private  Integer category;
    private  String enCode;
    private  String fullName;
    private  Integer isPublic;
    private Date overdueTime;
    private  Integer sortCode;
    private  Integer deleteMark;
    private  Integer enabledMark;
    private  String description;
    private  Date createDate;
    private  String createUserId;
    private  String createUserName;
    private  Date modifyDate;
    private  String modifyUserId;
    private  String modifyUserName;
//------------属性列表：17
    /**
     *角色主键
     *@return
     */
    public String getRoleId(){

        return roleId;
    }

    /**
     *角色主键
     *@param roleId
     */
    public void setRoleId(String roleId){

        this.roleId=roleId;
    }

    /**
     *机构主键
     *@return
     */
    public String getOrganizeId(){

        return organizeId;
    }

    /**
     *机构主键
     *@param organizeId
     */
    public void setOrganizeId(String organizeId){

        this.organizeId=organizeId;
    }

    /**
     *分类1-角色2-岗位3-职位4-工作组
     *@return
     */
    public Integer getCategory(){

        return category;
    }

    /**
     *分类1-角色2-岗位3-职位4-工作组
     *@param category
     */
    public void setCategory(Integer category){

        this.category=category;
    }

    /**
     *角色编码
     *@return
     */
    public String getEnCode(){

        return enCode;
    }

    /**
     *角色编码
     *@param enCode
     */
    public void setEnCode(String enCode){

        this.enCode=enCode;
    }

    /**
     *角色名称
     *@return
     */
    public String getFullName(){

        return fullName;
    }

    /**
     *角色名称
     *@param fullName
     */
    public void setFullName(String fullName){

        this.fullName=fullName;
    }

    /**
     *公共角色
     *@return
     */
    public Integer getIsPublic(){

        return isPublic;
    }

    /**
     *公共角色
     *@param isPublic
     */
    public void setIsPublic(Integer isPublic){

        this.isPublic=isPublic;
    }

    /**
     *过期时间
     *@return
     */
    public Date getOverdueTime(){

        return overdueTime;
    }

    /**
     *过期时间
     *@param overdueTime
     */
    public void setOverdueTime(Date overdueTime){

        this.overdueTime=overdueTime;
    }

    /**
     *排序码
     *@return
     */
    public Integer getSortCode(){

        return sortCode;
    }

    /**
     *排序码
     *@param sortCode
     */
    public void setSortCode(Integer sortCode){

        this.sortCode=sortCode;
    }

    /**
     *删除标记
     *@return
     */
    public Integer getDeleteMark(){

        return deleteMark;
    }

    /**
     *删除标记
     *@param deleteMark
     */
    public void setDeleteMark(Integer deleteMark){

        this.deleteMark=deleteMark;
    }

    /**
     *有效标志
     *@return
     */
    public Integer getEnabledMark(){

        return enabledMark;
    }

    /**
     *有效标志
     *@param enabledMark
     */
    public void setEnabledMark(Integer enabledMark){

        this.enabledMark=enabledMark;
    }

    /**
     *备注
     *@return
     */
    public String getDescription(){

        return description;
    }

    /**
     *备注
     *@param description
     */
    public void setDescription(String description){

        this.description=description;
    }

    /**
     *创建日期
     *@return
     */
    public Date getCreateDate(){

        return createDate;
    }

    /**
     *创建日期
     *@param createDate
     */
    public void setCreateDate(Date createDate){

        this.createDate=createDate;
    }

    /**
     *创建用户主键
     *@return
     */
    public String getCreateUserId(){

        return createUserId;
    }

    /**
     *创建用户主键
     *@param createUserId
     */
    public void setCreateUserId(String createUserId){

        this.createUserId=createUserId;
    }

    /**
     *创建用户
     *@return
     */
    public String getCreateUserName(){

        return createUserName;
    }

    /**
     *创建用户
     *@param createUserName
     */
    public void setCreateUserName(String createUserName){

        this.createUserName=createUserName;
    }

    /**
     *修改日期
     *@return
     */
    public Date getModifyDate(){

        return modifyDate;
    }

    /**
     *修改日期
     *@param modifyDate
     */
    public void setModifyDate(Date modifyDate){

        this.modifyDate=modifyDate;
    }

    /**
     *修改用户主键
     *@return
     */
    public String getModifyUserId(){

        return modifyUserId;
    }

    /**
     *修改用户主键
     *@param modifyUserId
     */
    public void setModifyUserId(String modifyUserId){

        this.modifyUserId=modifyUserId;
    }

    /**
     *修改用户
     *@return
     */
    public String getModifyUserName(){

        return modifyUserName;
    }

    /**
     *修改用户
     *@param modifyUserName
     */
    public void setModifyUserName(String modifyUserName){

        this.modifyUserName=modifyUserName;
    }

    /**
     * 角色对应的菜单列表
     */
    private List<String> menuIdList;

    public List<String> getMenuIdList() {

        return menuIdList;
    }

    public void setMenuIdList(List<String> menuIdList) {

        this.menuIdList = menuIdList;
    }
    /**
     * 机构名称
     * @return
     */

    public String getOrganizeName() {

        return organizeName;
    }

    public void setOrganizeName(String organizeName) {

        this.organizeName = organizeName;
    }

    private  String organizeName;
}
