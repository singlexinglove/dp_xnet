package com.xnet.shiro.entity.base;

import com.xnet.common.entity.BaseEntity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * 组织机构
 *
 * @author Xingyc
 * @date 2018-1-11 11:44:08
 */
public class OrganizeEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	//------------字段总数：32
	private  String organizeId;
	private  Integer category;
	private  String parentId;
	private  String enCode;
	private  String shortName;
	private  String fullName;
	private  String nature;
	private  String outerPhone;
	private  String innerPhone;
	private  String fax;
	private  String postalcode;
	private  String email;
	private  String managerId;
	private  String manager;
	private  String provinceId;
	private  String cityId;
	private  String countyId;
	private  String address;
	private  String webAddress;
	private  Date foundedTime;
	private  String businessScope;
	private  Integer layer;
	private  Integer sortCode;
	private  Integer deleteMark;
	private  Integer enabledMark;
	private  String description;
	private  Date createDate;
	private  String createUserId;
	private  String createUserName;
	private  Date modifyDate;
	private  String modifyUserId;
	private  String modifyUserName;
//------------属性列表：32
	/**
	 *机构主键
	 *@return
	 */
	public String getOrganizeId(){

		return organizeId;
	}

	/**
	 *机构主键
	 *@param organizeId
	 */
	public void setOrganizeId(String organizeId){

		this.organizeId=organizeId;
	}

	/**
	 *机构分类
	 *@return
	 */
	public Integer getCategory(){

		return category;
	}

	/**
	 *机构分类
	 *@param category
	 */
	public void setCategory(Integer category){

		this.category=category;
	}

	/**
	 *父级主键
	 *@return
	 */
	public String getParentId(){

		return parentId;
	}

	/**
	 *父级主键
	 *@param parentId
	 */
	public void setParentId(String parentId){

		this.parentId=parentId;
	}

	/**
	 *机构代码
	 *@return
	 */
	public String getEnCode(){

		return enCode;
	}

	/**
	 *机构代码
	 *@param enCode
	 */
	public void setEnCode(String enCode){

		this.enCode=enCode;
	}

	/**
	 *机构简称
	 *@return
	 */
	public String getShortName(){

		return shortName;
	}

	/**
	 *机构简称
	 *@param shortName
	 */
	public void setShortName(String shortName){

		this.shortName=shortName;
	}

	/**
	 *机构名称
	 *@return
	 */
	public String getFullName(){

		return fullName;
	}

	/**
	 *机构名称
	 *@param fullName
	 */
	public void setFullName(String fullName){

		this.fullName=fullName;
	}

	/**
	 *机构性质
	 *@return
	 */
	public String getNature(){

		return nature;
	}

	/**
	 *机构性质
	 *@param nature
	 */
	public void setNature(String nature){

		this.nature=nature;
	}

	/**
	 *外线电话
	 *@return
	 */
	public String getOuterPhone(){

		return outerPhone;
	}

	/**
	 *外线电话
	 *@param outerPhone
	 */
	public void setOuterPhone(String outerPhone){

		this.outerPhone=outerPhone;
	}

	/**
	 *内线电话
	 *@return
	 */
	public String getInnerPhone(){

		return innerPhone;
	}

	/**
	 *内线电话
	 *@param innerPhone
	 */
	public void setInnerPhone(String innerPhone){

		this.innerPhone=innerPhone;
	}

	/**
	 *传真
	 *@return
	 */
	public String getFax(){

		return fax;
	}

	/**
	 *传真
	 *@param fax
	 */
	public void setFax(String fax){

		this.fax=fax;
	}

	/**
	 *邮编
	 *@return
	 */
	public String getPostalcode(){

		return postalcode;
	}

	/**
	 *邮编
	 *@param postalcode
	 */
	public void setPostalcode(String postalcode){

		this.postalcode=postalcode;
	}

	/**
	 *电子邮箱
	 *@return
	 */
	public String getEmail(){

		return email;
	}

	/**
	 *电子邮箱
	 *@param email
	 */
	public void setEmail(String email){

		this.email=email;
	}

	/**
	 *负责人主键
	 *@return
	 */
	public String getManagerId(){

		return managerId;
	}

	/**
	 *负责人主键
	 *@param managerId
	 */
	public void setManagerId(String managerId){

		this.managerId=managerId;
	}

	/**
	 *负责人
	 *@return
	 */
	public String getManager(){

		return manager;
	}

	/**
	 *负责人
	 *@param manager
	 */
	public void setManager(String manager){

		this.manager=manager;
	}

	/**
	 *省主键
	 *@return
	 */
	public String getProvinceId(){

		return provinceId;
	}

	/**
	 *省主键
	 *@param provinceId
	 */
	public void setProvinceId(String provinceId){

		this.provinceId=provinceId;
	}

	/**
	 *市主键
	 *@return
	 */
	public String getCityId(){

		return cityId;
	}

	/**
	 *市主键
	 *@param cityId
	 */
	public void setCityId(String cityId){

		this.cityId=cityId;
	}

	/**
	 *县/区主键
	 *@return
	 */
	public String getCountyId(){

		return countyId;
	}

	/**
	 *县/区主键
	 *@param countyId
	 */
	public void setCountyId(String countyId){

		this.countyId=countyId;
	}

	/**
	 *详细地址
	 *@return
	 */
	public String getAddress(){

		return address;
	}

	/**
	 *详细地址
	 *@param address
	 */
	public void setAddress(String address){

		this.address=address;
	}

	/**
	 *公司主页
	 *@return
	 */
	public String getWebAddress(){

		return webAddress;
	}

	/**
	 *公司主页
	 *@param webAddress
	 */
	public void setWebAddress(String webAddress){

		this.webAddress=webAddress;
	}

	/**
	 *成立时间
	 *@return
	 */
	public Date getFoundedTime(){

		return foundedTime;
	}

	/**
	 *成立时间
	 *@param foundedTime
	 */
	public void setFoundedTime(Date foundedTime){

		this.foundedTime=foundedTime;
	}

	/**
	 *经营范围
	 *@return
	 */
	public String getBusinessScope(){

		return businessScope;
	}

	/**
	 *经营范围
	 *@param businessScope
	 */
	public void setBusinessScope(String businessScope){

		this.businessScope=businessScope;
	}

	/**
	 *层
	 *@return
	 */
	public Integer getLayer(){

		return layer;
	}

	/**
	 *层
	 *@param layer
	 */
	public void setLayer(Integer layer){

		this.layer=layer;
	}

	/**
	 *排序码
	 *@return
	 */
	public Integer getSortCode(){

		return sortCode;
	}

	/**
	 *排序码
	 *@param sortCode
	 */
	public void setSortCode(Integer sortCode){

		this.sortCode=sortCode;
	}

	/**
	 *删除标记
	 *@return
	 */
	public Integer getDeleteMark(){

		return deleteMark;
	}

	/**
	 *删除标记
	 *@param deleteMark
	 */
	public void setDeleteMark(Integer deleteMark){

		this.deleteMark=deleteMark;
	}

	/**
	 *有效标志
	 *@return
	 */
	public Integer getEnabledMark(){

		return enabledMark;
	}

	/**
	 *有效标志
	 *@param enabledMark
	 */
	public void setEnabledMark(Integer enabledMark){

		this.enabledMark=enabledMark;
	}

	/**
	 *备注
	 *@return
	 */
	public String getDescription(){

		return description;
	}

	/**
	 *备注
	 *@param description
	 */
	public void setDescription(String description){

		this.description=description;
	}

	/**
	 *创建日期
	 *@return
	 */
	public Date getCreateDate(){

		return createDate;
	}

	/**
	 *创建日期
	 *@param createDate
	 */
	public void setCreateDate(Date createDate){

		this.createDate=createDate;
	}

	/**
	 *创建用户主键
	 *@return
	 */
	public String getCreateUserId(){

		return createUserId;
	}

	/**
	 *创建用户主键
	 *@param createUserId
	 */
	public void setCreateUserId(String createUserId){

		this.createUserId=createUserId;
	}

	/**
	 *创建用户
	 *@return
	 */
	public String getCreateUserName(){

		return createUserName;
	}

	/**
	 *创建用户
	 *@param createUserName
	 */
	public void setCreateUserName(String createUserName){

		this.createUserName=createUserName;
	}

	/**
	 *修改日期
	 *@return
	 */
	public Date getModifyDate(){

		return modifyDate;
	}

	/**
	 *修改日期
	 *@param modifyDate
	 */
	public void setModifyDate(Date modifyDate){

		this.modifyDate=modifyDate;
	}

	/**
	 *修改用户主键
	 *@return
	 */
	public String getModifyUserId(){

		return modifyUserId;
	}

	/**
	 *修改用户主键
	 *@param modifyUserId
	 */
	public void setModifyUserId(String modifyUserId){

		this.modifyUserId=modifyUserId;
	}

	/**
	 *修改用户
	 *@return
	 */
	public String getModifyUserName(){

		return modifyUserName;
	}

	/**
	 *修改用户
	 *@param modifyUserName
	 */
	public void setModifyUserName(String modifyUserName){

		this.modifyUserName=modifyUserName;
	}



}
