package com.xnet.shiro.entity.base;

import java.util.Date;

/**
 * 部门实体
 *
 * @author Xingyc
 * @date 2018-1-24 10:53:48
 */
public class DepartmentEntity {

    //------------字段总数：24
    private String departmentId;

    private String organizeId;

    private String parentId;

    private String enCode;

    private String fullName;

    private String shortName;

    private String nature;

    private String managerId;

    private String manager;

    private String outerPhone;

    private String innerPhone;

    private String email;

    private String fax;

    private Integer layer;

    private Integer sortCode;

    private Integer deleteMark;

    private Integer enabledMark;

    private String description;

    private Date createDate;

    private String createUserId;

    private String createUserName;

    private Date modifyDate;

    private String modifyUserId;

    private String modifyUserName;

//------------属性列表：24

    /**
     * 部门主键
     *
     * @return
     */
    public String getDepartmentId() {

        return departmentId;
    }

    /**
     * 部门主键
     *
     * @param departmentId
     */
    public void setDepartmentId(String departmentId) {

        this.departmentId = departmentId;
    }

    /**
     * 机构主键
     *
     * @return
     */
    public String getOrganizeId() {

        return organizeId;
    }

    /**
     * 机构主键
     *
     * @param organizeId
     */
    public void setOrganizeId(String organizeId) {

        this.organizeId = organizeId;
    }

    /**
     * 父级主键
     *
     * @return
     */
    public String getParentId() {

        return parentId;
    }

    /**
     * 父级主键
     *
     * @param parentId
     */
    public void setParentId(String parentId) {

        this.parentId = parentId;
    }

    /**
     * 部门代码
     *
     * @return
     */
    public String getEnCode() {

        return enCode;
    }

    /**
     * 部门代码
     *
     * @param enCode
     */
    public void setEnCode(String enCode) {

        this.enCode = enCode;
    }

    /**
     * 部门名称
     *
     * @return
     */
    public String getFullName() {

        return fullName;
    }

    /**
     * 部门名称
     *
     * @param fullName
     */
    public void setFullName(String fullName) {

        this.fullName = fullName;
    }

    /**
     * 部门简称
     *
     * @return
     */
    public String getShortName() {

        return shortName;
    }

    /**
     * 部门简称
     *
     * @param shortName
     */
    public void setShortName(String shortName) {

        this.shortName = shortName;
    }

    /**
     * 部门类型
     *
     * @return
     */
    public String getNature() {

        return nature;
    }

    /**
     * 部门类型
     *
     * @param nature
     */
    public void setNature(String nature) {

        this.nature = nature;
    }

    /**
     * 负责人主键
     *
     * @return
     */
    public String getManagerId() {

        return managerId;
    }

    /**
     * 负责人主键
     *
     * @param managerId
     */
    public void setManagerId(String managerId) {

        this.managerId = managerId;
    }

    /**
     * 负责人
     *
     * @return
     */
    public String getManager() {

        return manager;
    }

    /**
     * 负责人
     *
     * @param manager
     */
    public void setManager(String manager) {

        this.manager = manager;
    }

    /**
     * 外线电话
     *
     * @return
     */
    public String getOuterPhone() {

        return outerPhone;
    }

    /**
     * 外线电话
     *
     * @param outerPhone
     */
    public void setOuterPhone(String outerPhone) {

        this.outerPhone = outerPhone;
    }

    /**
     * 内线电话
     *
     * @return
     */
    public String getInnerPhone() {

        return innerPhone;
    }

    /**
     * 内线电话
     *
     * @param innerPhone
     */
    public void setInnerPhone(String innerPhone) {

        this.innerPhone = innerPhone;
    }

    /**
     * 电子邮件
     *
     * @return
     */
    public String getEmail() {

        return email;
    }

    /**
     * 电子邮件
     *
     * @param email
     */
    public void setEmail(String email) {

        this.email = email;
    }

    /**
     * 部门传真
     *
     * @return
     */
    public String getFax() {

        return fax;
    }

    /**
     * 部门传真
     *
     * @param fax
     */
    public void setFax(String fax) {

        this.fax = fax;
    }

    /**
     * 层
     *
     * @return
     */
    public Integer getLayer() {

        return layer;
    }

    /**
     * 层
     *
     * @param layer
     */
    public void setLayer(Integer layer) {

        this.layer = layer;
    }

    /**
     * 排序码
     *
     * @return
     */
    public Integer getSortCode() {

        return sortCode;
    }

    /**
     * 排序码
     *
     * @param sortCode
     */
    public void setSortCode(Integer sortCode) {

        this.sortCode = sortCode;
    }

    /**
     * 删除标记
     *
     * @return
     */
    public Integer getDeleteMark() {

        return deleteMark;
    }

    /**
     * 删除标记
     *
     * @param deleteMark
     */
    public void setDeleteMark(Integer deleteMark) {

        this.deleteMark = deleteMark;
    }

    /**
     * 有效标志
     *
     * @return
     */
    public Integer getEnabledMark() {

        return enabledMark;
    }

    /**
     * 有效标志
     *
     * @param enabledMark
     */
    public void setEnabledMark(Integer enabledMark) {

        this.enabledMark = enabledMark;
    }

    /**
     * 备注
     *
     * @return
     */
    public String getDescription() {

        return description;
    }

    /**
     * 备注
     *
     * @param description
     */
    public void setDescription(String description) {

        this.description = description;
    }

    /**
     * 创建日期
     *
     * @return
     */
    public Date getCreateDate() {

        return createDate;
    }

    /**
     * 创建日期
     *
     * @param createDate
     */
    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }

    /**
     * 创建用户主键
     *
     * @return
     */
    public String getCreateUserId() {

        return createUserId;
    }

    /**
     * 创建用户主键
     *
     * @param createUserId
     */
    public void setCreateUserId(String createUserId) {

        this.createUserId = createUserId;
    }

    /**
     * 创建用户
     *
     * @return
     */
    public String getCreateUserName() {

        return createUserName;
    }

    /**
     * 创建用户
     *
     * @param createUserName
     */
    public void setCreateUserName(String createUserName) {

        this.createUserName = createUserName;
    }

    /**
     * 修改日期
     *
     * @return
     */
    public Date getModifyDate() {

        return modifyDate;
    }

    /**
     * 修改日期
     *
     * @param modifyDate
     */
    public void setModifyDate(Date modifyDate) {

        this.modifyDate = modifyDate;
    }

    /**
     * 修改用户主键
     *
     * @return
     */
    public String getModifyUserId() {

        return modifyUserId;
    }

    /**
     * 修改用户主键
     *
     * @param modifyUserId
     */
    public void setModifyUserId(String modifyUserId) {

        this.modifyUserId = modifyUserId;
    }

    /**
     * 修改用户
     *
     * @return
     */
    public String getModifyUserName() {

        return modifyUserName;
    }

    /**
     * 修改用户
     *
     * @param modifyUserName
     */
    public void setModifyUserName(String modifyUserName) {

        this.modifyUserName = modifyUserName;
    }


}
