package com.xnet.shiro.entity.authorize;

import java.io.Serializable;
import java.util.Date;



/**
 * 授权数据表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月25日 下午4:41:49
 */
public class AuthorizeDataEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 授权数据主键
	 */
	private String authorizeDataId;
	
	/**
	 * 1-仅限本人2-仅限本人及下属3-所
	 */
	private Integer authorizeType;
	
	/**
	 * 对象分类:1-部门2-角色3-岗位4-
	 */
	private Integer category;
	
	/**
	 * 对象主键
	 */
	private String objectId;
	
	/**
	 * 项目Id
	 */
	private String itemId;
	
	/**
	 * 项目Id
	 */
	private String itemName;
	
	/**
	 * 项目名称
	 */
	private String resourceId;
	
	/**
	 * 只读
	 */
	private Integer isRead;
	
	/**
	 * 约束表达式
	 */
	private String authorizeConstraint;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	
	/**
	 * 创建时间
	 */
	private Date createDate;
	
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	
	/**
	 * 创建用户
	 */
	private String createUserName;
	
	/**
	 * 
	 */
	private Date modifyDate;
	
	/**
	 * 
	 */
	private String modifyUserId;
	
	/**
	 * 
	 */
	private String modifyUserName;
	

	public AuthorizeDataEntity() {
		super();
	}

	public void setAuthorizeDataId(String authorizeDataId) {
		this.authorizeDataId = authorizeDataId;
	}
	
	public String getAuthorizeDataId() {
		return authorizeDataId;
	}
	
	public void setAuthorizeType(Integer authorizeType) {
		this.authorizeType = authorizeType;
	}
	
	public Integer getAuthorizeType() {
		return authorizeType;
	}
	
	public void setCategory(Integer category) {
		this.category = category;
	}
	
	public Integer getCategory() {
		return category;
	}
	
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public String getObjectId() {
		return objectId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getItemId() {
		return itemId;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getResourceId() {
		return resourceId;
	}
	
	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}
	
	public Integer getIsRead() {
		return isRead;
	}
	
	public void setAuthorizeConstraint(String authorizeConstraint) {
		this.authorizeConstraint = authorizeConstraint;
	}
	
	public String getAuthorizeConstraint() {
		return authorizeConstraint;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	public String getCreateUserName() {
		return createUserName;
	}
	
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public Date getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public String getModifyUserId() {
		return modifyUserId;
	}
	
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public String getModifyUserName() {
		return modifyUserName;
	}
	
}
