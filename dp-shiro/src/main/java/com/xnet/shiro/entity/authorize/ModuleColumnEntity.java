package com.xnet.shiro.entity.authorize;

import java.io.Serializable;
import java.util.Date;



/**
 * 功能表格列表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月13日 下午3:37:36
 */
public class ModuleColumnEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 列主键
	 */
	private String moduleColumnId;
	
	/**
	 * 功能主键
	 */
	private String moduleId;
	
	/**
	 * 父级主键
	 */
	private String parentId;
	
	/**
	 * 编码
	 */
	private String enCode;
	
	/**
	 * 名称
	 */
	private String fullName;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	
	/**
	 * 备注
	 */
	private String description;
	

	public ModuleColumnEntity() {
		super();
	}

	public void setModuleColumnId(String moduleColumnId) {
		this.moduleColumnId = moduleColumnId;
	}
	
	public String getModuleColumnId() {
		return moduleColumnId;
	}
	
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	public String getModuleId() {
		return moduleId;
	}
	
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId() {
		return parentId;
	}
	
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	
	public String getEnCode() {
		return enCode;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
}
