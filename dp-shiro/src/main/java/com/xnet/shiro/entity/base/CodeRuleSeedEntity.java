package com.xnet.shiro.entity.base;

import java.io.Serializable;
import java.util.Date;



/**
 * 编号规则种子表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年10月20日 下午4:23:26
 */
public class CodeRuleSeedEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 编号规则种子主键
	 */
	private String ruleSeedId;
	
	/**
	 * 编码规则主键
	 */
	private String ruleId;
	
	/**
	 * 用户主键
	 */
	private String userId;
	
	/**
	 * 种子值
	 */
	private Integer seedValue;
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	
	/**
	 * 创建用户
	 */
	private String createUserName;
	
	/**
	 * 修改日期
	 */
	private Date modifyDate;
	
	/**
	 * 修改用户主键
	 */
	private String modifyUserId;
	
	/**
	 * 修改用户
	 */
	private String modifyUserName;
	

	public CodeRuleSeedEntity() {
		super();
	}

	public void setRuleSeedId(String ruleSeedId) {
		this.ruleSeedId = ruleSeedId;
	}
	
	public String getRuleSeedId() {
		return ruleSeedId;
	}
	
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
	
	public String getRuleId() {
		return ruleId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setSeedValue(Integer seedValue) {
		this.seedValue = seedValue;
	}
	
	public Integer getSeedValue() {
		return seedValue;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	public String getCreateUserName() {
		return createUserName;
	}
	
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public Date getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public String getModifyUserId() {
		return modifyUserId;
	}
	
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public String getModifyUserName() {
		return modifyUserName;
	}
	
}
