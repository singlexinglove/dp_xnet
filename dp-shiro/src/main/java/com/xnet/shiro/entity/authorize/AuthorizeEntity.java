package com.xnet.shiro.entity.authorize;

import com.xnet.common.entity.BaseEntity;

import java.io.Serializable;
import java.util.Date;



/**
 * 授权功能表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月18日 下午5:42:28
 */
public class AuthorizeEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 授权功能主键
	 */
	private String authorizeId;
	
	/**
	 * 对象分类:1-部门2-角色3-岗位4-
	 */
	private Integer category;
	
	/**
	 * 对象主键
	 */
	private String objectId;
	
	/**
	 * 项目类型:1-菜单2-按钮3-视图4表
	 */
	private Integer itemType;
	
	/**
	 * 项目主键
	 */
	private String itemId;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	
	/**
	 * 创建时间
	 */
	private Date createDate;
	
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	
	/**
	 * 创建用户
	 */
	private String createUserName;
	
	/**
	 * 
	 */
	private Date modifyDate;
	
	/**
	 * 
	 */
	private String modifyUserId;
	
	/**
	 * 
	 */
	private String modifyUserName;
	

	public AuthorizeEntity() {
		super();
	}

	public void setAuthorizeId(String authorizeId) {
		this.authorizeId = authorizeId;
	}
	
	public String getAuthorizeId() {
		return authorizeId;
	}
	
	public void setCategory(Integer category) {
		this.category = category;
	}
	
	public Integer getCategory() {
		return category;
	}
	
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public String getObjectId() {
		return objectId;
	}
	
	public void setItemType(Integer itemType) {
		this.itemType = itemType;
	}
	
	public Integer getItemType() {
		return itemType;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getItemId() {
		return itemId;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	public String getCreateUserName() {
		return createUserName;
	}
	
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public Date getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public String getModifyUserId() {
		return modifyUserId;
	}
	
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public String getModifyUserName() {
		return modifyUserName;
	}
	
}
