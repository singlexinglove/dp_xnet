package com.xnet.shiro.entity.base;

import com.xnet.common.entity.BaseEntity;

import java.io.Serializable;
import java.util.Date;



/**
 * 行政区域表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年4月12日 上午8:41:28
 */
public class AreaEntity extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 区域主键
	 */
	private String areaId;
	
	/**
	 * 父级主键
	 */
	private String parentId;
	
	/**
	 * 区域编码
	 */
	private String areaCode;
	
	/**
	 * 区域名称
	 */
	private String areaName;
	
	/**
	 * 快速查询
	 */
	private String quickQuery;
	
	/**
	 * 简拼
	 */
	private String simpleSpelling;
	
	/**
	 * 层次
	 */
	private Integer layer;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	
	/**
	 * 删除标记
	 */
	private Integer deleteMark;
	
	/**
	 * 有效标志
	 */
	private Integer enabledMark;
	
	/**
	 * 备注
	 */
	private String description;
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	
	/**
	 * 创建用户
	 */
	private String createUserName;
	
	/**
	 * 修改日期
	 */
	private Date modifyDate;
	
	/**
	 * 修改用户主键
	 */
	private String modifyUserId;
	
	/**
	 * 修改用户
	 */
	private String modifyUserName;
	

	public AreaEntity() {
		super();
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getAreaId() {
		return areaId;
	}
	
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId() {
		return parentId;
	}
	
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	public String getAreaName() {
		return areaName;
	}
	
	public void setQuickQuery(String quickQuery) {
		this.quickQuery = quickQuery;
	}
	
	public String getQuickQuery() {
		return quickQuery;
	}
	
	public void setSimpleSpelling(String simpleSpelling) {
		this.simpleSpelling = simpleSpelling;
	}
	
	public String getSimpleSpelling() {
		return simpleSpelling;
	}
	
	public void setLayer(Integer layer) {
		this.layer = layer;
	}
	
	public Integer getLayer() {
		return layer;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	
	public Integer getDeleteMark() {
		return deleteMark;
	}
	
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	
	public Integer getEnabledMark() {
		return enabledMark;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	public String getCreateUserName() {
		return createUserName;
	}
	
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public Date getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public String getModifyUserId() {
		return modifyUserId;
	}
	
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public String getModifyUserName() {
		return modifyUserName;
	}
	
}
