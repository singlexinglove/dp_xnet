package com.xnet.shiro.entity.base;

import com.xnet.common.entity.BaseEntity;

import java.util.Date;

/**
 * 数据字典分类实体
 *
 * @author Xingyc
 * @date 2018-3-14 16:03:24
 */
public class DataItemEntity extends BaseEntity {
    //------------字段总数：16
    private  String itemId;
    private  String parentId;
    private  String itemCode;
    private  String itemName;
    private  Integer isTree;
    private  Integer isNav;
    private  Integer sortCode;
    private  Integer deleteMark;
    private  Integer enabledMark;
    private  String description;
    private  Date createDate;
    private  String createUserId;
    private  String createUserName;
    private  Date modifyDate;
    private  String modifyUserId;
    private  String modifyUserName;
//------------属性列表：16
    /**
     *分类主键
     *@return
     */
    public String getItemId(){

        return itemId;
    }

    /**
     *分类主键
     *@param itemId
     */
    public void setItemId(String itemId){

        this.itemId=itemId;
    }

    /**
     *父级主键
     *@return
     */
    public String getParentId(){

        return parentId;
    }

    /**
     *父级主键
     *@param parentId
     */
    public void setParentId(String parentId){

        this.parentId=parentId;
    }

    /**
     *分类编码
     *@return
     */
    public String getItemCode(){

        return itemCode;
    }

    /**
     *分类编码
     *@param itemCode
     */
    public void setItemCode(String itemCode){

        this.itemCode=itemCode;
    }

    /**
     *分类名称
     *@return
     */
    public String getItemName(){

        return itemName;
    }

    /**
     *分类名称
     *@param itemName
     */
    public void setItemName(String itemName){

        this.itemName=itemName;
    }

    /**
     *树型结构
     *@return
     */
    public Integer getIsTree(){

        return isTree;
    }

    /**
     *树型结构
     *@param isTree
     */
    public void setIsTree(Integer isTree){

        this.isTree=isTree;
    }

    /**
     *导航标记
     *@return
     */
    public Integer getIsNav(){

        return isNav;
    }

    /**
     *导航标记
     *@param isNav
     */
    public void setIsNav(Integer isNav){

        this.isNav=isNav;
    }

    /**
     *排序码
     *@return
     */
    public Integer getSortCode(){

        return sortCode;
    }

    /**
     *排序码
     *@param sortCode
     */
    public void setSortCode(Integer sortCode){

        this.sortCode=sortCode;
    }

    /**
     *删除标记
     *@return
     */
    public Integer getDeleteMark(){

        return deleteMark;
    }

    /**
     *删除标记
     *@param deleteMark
     */
    public void setDeleteMark(Integer deleteMark){

        this.deleteMark=deleteMark;
    }

    /**
     *有效标志
     *@return
     */
    public Integer getEnabledMark(){

        return enabledMark;
    }

    /**
     *有效标志
     *@param enabledMark
     */
    public void setEnabledMark(Integer enabledMark){

        this.enabledMark=enabledMark;
    }

    /**
     *备注
     *@return
     */
    public String getDescription(){

        return description;
    }

    /**
     *备注
     *@param description
     */
    public void setDescription(String description){

        this.description=description;
    }

    /**
     *创建日期
     *@return
     */
    public Date getCreateDate(){

        return createDate;
    }

    /**
     *创建日期
     *@param createDate
     */
    public void setCreateDate(Date createDate){

        this.createDate=createDate;
    }

    /**
     *创建用户主键
     *@return
     */
    public String getCreateUserId(){

        return createUserId;
    }

    /**
     *创建用户主键
     *@param createUserId
     */
    public void setCreateUserId(String createUserId){

        this.createUserId=createUserId;
    }

    /**
     *创建用户
     *@return
     */
    public String getCreateUserName(){

        return createUserName;
    }

    /**
     *创建用户
     *@param createUserName
     */
    public void setCreateUserName(String createUserName){

        this.createUserName=createUserName;
    }

    /**
     *修改日期
     *@return
     */
    public Date getModifyDate(){

        return modifyDate;
    }

    /**
     *修改日期
     *@param modifyDate
     */
    public void setModifyDate(Date modifyDate){

        this.modifyDate=modifyDate;
    }

    /**
     *修改用户主键
     *@return
     */
    public String getModifyUserId(){

        return modifyUserId;
    }

    /**
     *修改用户主键
     *@param modifyUserId
     */
    public void setModifyUserId(String modifyUserId){

        this.modifyUserId=modifyUserId;
    }

    /**
     *修改用户
     *@return
     */
    public String getModifyUserName(){

        return modifyUserName;
    }

    /**
     *修改用户
     *@param modifyUserName
     */
    public void setModifyUserName(String modifyUserName){

        this.modifyUserName=modifyUserName;
    }


}
