package com.xnet.shiro.entity.authorize;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 系统功能表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月07日 上午10:31:35
 */
public class ModuleEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 功能主键
	 */
	private String moduleId;
	
	/**
	 * 父级主键
	 */
	private String parentId;
	
	/**
	 * 编码
	 */
	private String enCode;
	
	/**
	 * 名称
	 */
	private String fullName;
	
	/**
	 * 图标
	 */
	private String icon;
	
	/**
	 * 导航地址
	 */
	private String urlAddress;
	
	/**
	 * 导航目标
	 */
	private String target;
	
	/**
	 * 菜单选项
	 */
	private Integer isMenu;
	
	/**
	 * 允许展开
	 */
	private Integer allowExpand;
	
	/**
	 * 是否公开
	 */
	private Integer isPublic;
	
	/**
	 * 允许编辑
	 */
	private Integer allowEdit;
	
	/**
	 * 允许删除
	 */
	private Integer allowDelete;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	
	/**
	 * 删除标记
	 */
	private Integer deleteMark;
	
	/**
	 * 有效标志
	 */
	private Integer enabledMark;
	
	/**
	 * 备注
	 */
	private String description;
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	
	/**
	 * 创建用户
	 */
	private String createUserName;
	
	/**
	 * 修改日期
	 */
	private Date modifyDate;
	
	/**
	 * 修改用户主键
	 */
	private String modifyUserId;
	
	/**
	 * 修改用户
	 */
	private String modifyUserName;
	

	public ModuleEntity() {
		super();
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	public String getModuleId() {
		return moduleId;
	}
	
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId() {
		return parentId;
	}
	
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	
	public String getEnCode() {
		return enCode;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void setUrlAddress(String urlAddress) {
		this.urlAddress = urlAddress;
	}
	
	public String getUrlAddress() {
		return urlAddress;
	}
	
	public void setTarget(String target) {
		this.target = target;
	}
	
	public String getTarget() {
		return target;
	}
	
	public void setIsMenu(Integer isMenu) {
		this.isMenu = isMenu;
	}
	
	public Integer getIsMenu() {
		return isMenu;
	}
	
	public void setAllowExpand(Integer allowExpand) {
		this.allowExpand = allowExpand;
	}
	
	public Integer getAllowExpand() {
		return allowExpand;
	}
	
	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}
	
	public Integer getIsPublic() {
		return isPublic;
	}
	
	public void setAllowEdit(Integer allowEdit) {
		this.allowEdit = allowEdit;
	}
	
	public Integer getAllowEdit() {
		return allowEdit;
	}
	
	public void setAllowDelete(Integer allowDelete) {
		this.allowDelete = allowDelete;
	}
	
	public Integer getAllowDelete() {
		return allowDelete;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	
	public Integer getDeleteMark() {
		return deleteMark;
	}
	
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	
	public Integer getEnabledMark() {
		return enabledMark;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	public String getCreateUserName() {
		return createUserName;
	}
	
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public Date getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public String getModifyUserId() {
		return modifyUserId;
	}
	
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public String getModifyUserName() {
		return modifyUserName;
	}

	private List<?> list;


	public List<?> getList() {

		return list;
	}

	public void setList(List<?> list) {

		this.list = list;
	}
	
}
