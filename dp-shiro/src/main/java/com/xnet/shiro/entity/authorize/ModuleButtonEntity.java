package com.xnet.shiro.entity.authorize;

import java.io.Serializable;
import java.util.Date;



/**
 * 功能按钮表
 *
 * @author xyc
 * @email 
 * @url http://www.abcd.com
 * @date 2018年9月11日 下午2:21:14
 */
public class ModuleButtonEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 按钮主键
	 */
	private String moduleButtonId;
	
	/**
	 * 功能主键
	 */
	private String moduleId;
	
	/**
	 * 父级主键
	 */
	private String parentId;
	
	/**
	 * 图标
	 */
	private String icon;
	
	/**
	 * 编码
	 */
	private String enCode;
	
	/**
	 * 名称
	 */
	private String fullName;
	
	/**
	 * Action地址
	 */
	private String actionAddress;
	
	/**
	 * 排序码
	 */
	private Integer sortCode;
	

	public ModuleButtonEntity() {
		super();
	}

	public void setModuleButtonId(String moduleButtonId) {
		this.moduleButtonId = moduleButtonId;
	}
	
	public String getModuleButtonId() {
		return moduleButtonId;
	}
	
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	public String getModuleId() {
		return moduleId;
	}
	
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId() {
		return parentId;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	
	public String getEnCode() {
		return enCode;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setActionAddress(String actionAddress) {
		this.actionAddress = actionAddress;
	}
	
	public String getActionAddress() {
		return actionAddress;
	}
	
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	
	public Integer getSortCode() {
		return sortCode;
	}
	
}
