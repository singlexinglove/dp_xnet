package com.xnet.shiro.entity.base;
import com.xnet.common.entity.BaseEntity;

import java.io.Serializable;
import java.security.PrivateKey;
import java.util.Date;
/**
 * 数据字典分类实体
 *
 * @author Xingyc
 * @date 2018-3-14 16:03:24
 */

public class DataItemDetailEntity extends BaseEntity implements Serializable {

    private  String enCode;
    private  String itemDetailId;
    private  String itemId;
    private  String parentId;
    private  String itemCode;
    private  String itemName;
    private  String itemValue;
    private  String quickQuery;
    private  String simpleSpelling;
    private  Integer isDefault;
    private  Integer sortCode;
    private  Integer deleteMark;
    private  Integer enabledMark;
    private  String description;
    private  Date createDate;
    private  String createUserId;
    private  String createUserName;
    private  Date modifyDate;
    private  String modifyUserId;
    private  String modifyUserName;
//------------属性列表：19
    /**
     *明细主键
     *@return
     */
    public String getItemDetailId(){

        return itemDetailId;
    }

    /**
     *明细主键
     *@param itemDetailId
     */
    public void setItemDetailId(String itemDetailId){

        this.itemDetailId=itemDetailId;
    }

    public String getEnCode() {

        return enCode;
    }

    public void setEnCode(String enCode) {

        this.enCode = enCode;
    }

    /**
     *分类主键
     *@return
     */
    public String getItemId(){

        return itemId;
    }

    /**
     *分类主键
     *@param itemId
     */
    public void setItemId(String itemId){

        this.itemId=itemId;
    }

    /**
     *父级主键
     *@return
     */
    public String getParentId(){

        return parentId;
    }

    /**
     *父级主键
     *@param parentId
     */
    public void setParentId(String parentId){

        this.parentId=parentId;
    }

    /**
     *编码
     *@return
     */
    public String getItemCode(){

        return itemCode;
    }

    /**
     *编码
     *@param itemCode
     */
    public void setItemCode(String itemCode){

        this.itemCode=itemCode;
    }

    /**
     *名称
     *@return
     */
    public String getItemName(){

        return itemName;
    }

    /**
     *名称
     *@param itemName
     */
    public void setItemName(String itemName){

        this.itemName=itemName;
    }

    /**
     *值
     *@return
     */
    public String getItemValue(){

        return itemValue;
    }

    /**
     *值
     *@param itemValue
     */
    public void setItemValue(String itemValue){

        this.itemValue=itemValue;
    }

    /**
     *快速查询
     *@return
     */
    public String getQuickQuery(){

        return quickQuery;
    }

    /**
     *快速查询
     *@param quickQuery
     */
    public void setQuickQuery(String quickQuery){

        this.quickQuery=quickQuery;
    }

    /**
     *简拼
     *@return
     */
    public String getSimpleSpelling(){

        return simpleSpelling;
    }

    /**
     *简拼
     *@param simpleSpelling
     */
    public void setSimpleSpelling(String simpleSpelling){

        this.simpleSpelling=simpleSpelling;
    }

    /**
     *是否默认
     *@return
     */
    public Integer getIsDefault(){

        return isDefault;
    }

    /**
     *是否默认
     *@param isDefault
     */
    public void setIsDefault(Integer isDefault){

        this.isDefault=isDefault;
    }

    /**
     *排序码
     *@return
     */
    public Integer getSortCode(){

        return sortCode;
    }

    /**
     *排序码
     *@param sortCode
     */
    public void setSortCode(Integer sortCode){

        this.sortCode=sortCode;
    }

    /**
     *删除标记
     *@return
     */
    public Integer getDeleteMark(){

        return deleteMark;
    }

    /**
     *删除标记
     *@param deleteMark
     */
    public void setDeleteMark(Integer deleteMark){

        this.deleteMark=deleteMark;
    }

    /**
     *有效标志
     *@return
     */
    public Integer getEnabledMark(){

        return enabledMark;
    }

    /**
     *有效标志
     *@param enabledMark
     */
    public void setEnabledMark(Integer enabledMark){

        this.enabledMark=enabledMark;
    }

    /**
     *备注
     *@return
     */
    public String getDescription(){

        return description;
    }

    /**
     *备注
     *@param description
     */
    public void setDescription(String description){

        this.description=description;
    }

    /**
     *创建日期
     *@return
     */
    public Date getCreateDate(){

        return createDate;
    }

    /**
     *创建日期
     *@param createDate
     */
    public void setCreateDate(Date createDate){

        this.createDate=createDate;
    }

    /**
     *创建用户主键
     *@return
     */
    public String getCreateUserId(){

        return createUserId;
    }

    /**
     *创建用户主键
     *@param createUserId
     */
    public void setCreateUserId(String createUserId){

        this.createUserId=createUserId;
    }

    /**
     *创建用户
     *@return
     */
    public String getCreateUserName(){

        return createUserName;
    }

    /**
     *创建用户
     *@param createUserName
     */
    public void setCreateUserName(String createUserName){

        this.createUserName=createUserName;
    }

    /**
     *修改日期
     *@return
     */
    public Date getModifyDate(){

        return modifyDate;
    }

    /**
     *修改日期
     *@param modifyDate
     */
    public void setModifyDate(Date modifyDate){

        this.modifyDate=modifyDate;
    }

    /**
     *修改用户主键
     *@return
     */
    public String getModifyUserId(){

        return modifyUserId;
    }

    /**
     *修改用户主键
     *@param modifyUserId
     */
    public void setModifyUserId(String modifyUserId){

        this.modifyUserId=modifyUserId;
    }

    /**
     *修改用户
     *@return
     */
    public String getModifyUserName(){

        return modifyUserName;
    }

    /**
     *修改用户
     *@param modifyUserName
     */
    public void setModifyUserName(String modifyUserName){

        this.modifyUserName=modifyUserName;
    }


}
