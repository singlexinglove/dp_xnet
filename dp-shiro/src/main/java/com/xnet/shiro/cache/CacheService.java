package com.xnet.shiro.cache;

import java.util.List;

public interface CacheService<T> {

    List<T> list();

}
