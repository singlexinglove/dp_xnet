package com.xnet.shiro.cache;

import com.xnet.common.redis.RedisCacheManager;
import com.xnet.common.utils.JSONUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

//@Service("BaseCache")
public class BaseCache<T> {

    @Autowired
    private RedisCacheManager cacheManager;

    private CacheService cacheService;

    public CacheService getCacheService() {
        return cacheService;
    }

    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }


    /**
     * 获取缓存列表
     *
     * @param cacheKey
     * @return
     */
    public List<T> getListCache(String cacheKey) {
        if (cacheManager.hasKey(cacheKey)) {
            String entityListJson = cacheManager.get(cacheKey).toString();
            if (StringUtils.isBlank(entityListJson)) {
                return setCache(cacheKey);
            } else {
                List<T> result = (List<T>) JSONUtils.jsonToArray(entityListJson, getClazz());
                return result;
            }
        } else {
            return setCache(cacheKey);
        }
    }

    /**
     * 写入缓存
     *
     * @return
     */
    public List<T> setCache(String cacheKey) {
        List<T> entityList = cacheService.list();
        String entityListJson = JSONUtils.beanToJson(entityList, "yyyy-MM-dd");
        cacheManager.set(cacheKey, entityListJson);
        return entityList;
    }

    /**
     *更新缓存数据
     * @param cacheKey
     */
    public void updateCache(String cacheKey) {
        if (cacheManager.hasKey(cacheKey)) {
            cacheManager.del(cacheKey);
        }
        setCache(cacheKey);
    }


    /**
     * 获取泛型类类型
     *
     * @return
     */
    private Class<T> getClazz() {
        Type genType = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        return (Class) params[0];
    }

}
