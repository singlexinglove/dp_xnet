package com.xnet.shiro.cache.base;

import com.xnet.common.redis.RedisCacheManager;
import com.xnet.common.utils.JSONUtils;
import com.xnet.shiro.cache.BaseCache;
import com.xnet.shiro.entity.base.OrganizeEntity;
import com.xnet.shiro.service.base.OrganizeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("OrganizeCache")
public class OrganizeCache extends BaseCache<OrganizeEntity> {

    public String cacheKey = "OrganizeCache";

    @Autowired
    private RedisCacheManager cacheManager;

    @Autowired
    private OrganizeService organizeService;

    /**
     * 缓存列表
     *
     * @return
     */
    public List<OrganizeEntity> listCache() {

        super.setCacheService(organizeService);

        return super.getListCache(cacheKey);


//        if (cacheManager.hasKey(cacheKey)) {
////            List<Object> cacheList = cacheManager.lGet(cacheKey, 0, -1);
////            if (cacheList == null || cacheList.isEmpty()) {
////                return setCache();
////            } else {
////                return (List<OrganizeEntity>)cacheList.get(0);
////            }
//            String entityListJson=cacheManager.get(cacheKey).toString();
//            if(StringUtils.isBlank(entityListJson))
//            {
//                return setCache();
//            }
//            else
//            {
//                List<OrganizeEntity> result= (List<OrganizeEntity>) JSONUtils.jsonToArray(entityListJson, OrganizeEntity.class);
//                return result;
//            }
//
//        } else {
//            return setCache();
//        }

    }

}
