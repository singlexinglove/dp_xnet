package com.xnet.base.service.impl;

import com.xnet.common.entity.BaseEntity;
import com.xnet.base.service.BaseService;
import com.xnet.common.utils.ShiroUtils;

import java.util.Date;

public class BaseServiceImpl<T extends  BaseEntity> implements BaseService<T> {

    @Override
    public void create(T obj) {
        obj.setCreateUserId(ShiroUtils.getUserId());
        obj.setCreateUserName(ShiroUtils.getUserEntity().getAccount());
        obj.setCreateDate(new Date());
    }

    @Override
    public void modify(T obj) {
        obj.setModifyUserId(ShiroUtils.getUserId());
        obj.setModifyUserName(ShiroUtils.getUserEntity().getAccount());
        obj.setModifyDate(new Date());

    }
}
