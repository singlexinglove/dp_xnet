package com.xnet.base.service;

import com.xnet.common.entity.BaseEntity;

public interface BaseService<T extends BaseEntity> {

    /**
     * 创建默认方法
     * @param obj
     */
    void create(T obj);

    /**
     * 修改默认方法
     * @param obj
     */
    void modify(T obj);

}
