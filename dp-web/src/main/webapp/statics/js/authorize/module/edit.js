/**
 * 编辑-系统功能表js
 */

var keyValue = request('keyValue');
var parentId = request('parentId');

//初始化页面
function initialPage() {
    //加载导向
    $('#wizard').wizard().on('change', function (e, data) {
        var $finish = $("#btn_finish");
        var $next = $("#btn_next");
        if (data.direction == "next") {
            if (data.step == 2) {
                $finish.removeAttr('disabled');
                $next.attr('disabled', 'disabled');
            } else {
                $finish.attr('disabled', 'disabled');
            }
        } else {
            $finish.attr('disabled', 'disabled');
            $next.removeAttr('disabled');
        }
    });
    initControl();
}

//选取图标
function selectIcon() {
    dialogOpen({
        id: "SelectIcon",
        title: '图标选择',
        url: '/system/icon.html?controlId=icon',
        width: "1000px",
        height: "600px",
        btn: false,
        scroll: true
    });
}

//初始化控件
function initControl() {
    //目标
    $("#target").ComboBox({
        description: "==请选择==",
        height: "200px"
    });
    //上级
    $("#parentId").ComboBoxTree({
        url: "../../sys/menu/listTree",
        description: "==请选择==",
        height: "195px",
        allowSearch: true
    });
}

//按钮操作（上一步、下一步、完成、关闭）
function buttonOperation() {
    var $last = $("#btn_last");
    var $next = $("#btn_next");
    var $finish = $("#btn_finish");
    //如果是菜单，开启 上一步、下一步
    $("#isMenu").click(function () {
        if (!$(this).attr("checked")) {
            $(this).attr("checked", true)
            $next.removeAttr('disabled');
            $finish.attr('disabled', 'disabled');
        } else {
            $(this).attr("checked", false)
            $next.attr('disabled', 'disabled');
            $finish.removeAttr('disabled');
        }
    });
    //完成提交保存
    $finish.click(function () {
        vm.acceptClick();
    })
}

//region 菜单按钮

/*系统按钮begin==================================*/

var buttonJson = "";

function getGridButton() {
    var moduleId = $("#moduleId").val();
    $.ajax({
        url: "../../authorizeManage/moduleButton/listModuleId?moduleId=" + keyValue,
        type: "get",
        dataType: "json",
        success: function (data) {
            buttonJson = data;
        },
    });
    var $grid = $("#gridTable-button");
    $grid.jqGrid({
        unwritten: false,
        url: "../../authorizeManage/moduleButton/listGridTree?moduleId=" + keyValue,
        datatype: "json",
        height: $(window).height() - 165,
        width: $(window).width() - 11,
        colModel: [
            {label: "主键", name: "moduleButtonId", hidden: true},
            {label: "名称", name: "fullName", width: 140, align: "left", sortable: false},
            {label: "编号", name: "enCode", width: 140, align: "left", sortable: false},
            {label: "地址", name: "actionAddress", width: 500, align: "left", sortable: false},
        ],
        treeGrid: true,
        treeGridModel: "nested",
        ExpandColumn: "enCode",
        rowNum: "all",
        rownumbers: true
    });

    //处理Json
    function converToListTreeJson(buttonJson) {
        $.ajax({
            url: "../../authorizeManage/moduleButton/getListTreeGridJson",
            data: {json: JSON.stringify(buttonJson)},
            type: "post",
            dataType: "json",
            success: function (data) {
                $grid.clearGridData();
                $grid[0].addJSONData(data);
            },
        });
    }

    //新增
    $("#sx-add-button").click(function () {
        var parentId = $("#gridTable-button").jqGridRowValue("moduleButtonId");
        dialogOpen({
            id: "buttonForm",
            title: '添加按钮',
            url: '/authorize/moduleButton/add.html?parentId=' + parentId + "&moduleId=" + escape(moduleId),
            width: "450px",
            height: "300px",
            yes: function (iframeId) {
                top.frames[iframeId].vm.acceptClick(function (data) {
                    buttonJson.push(data);
                    converToListTreeJson(buttonJson);
                });
            }
        });
    })
    //编辑
    $("#sx-edit-button").click(function () {
        var moduleButtonId = $("#gridTable-button").jqGridRowValue("moduleButtonId");
        if (checkedRowEx(moduleButtonId)) {
            dialogOpen({
                id: "buttonForm",
                title: '编辑按钮',
                url: '/authorize/moduleButton/edit.html?moduleButtonId=' + moduleButtonId + "&moduleId=" + escape(moduleId),
                width: "450px",
                height: "300px",
                yes: function (iframeId) {
                    top.frames[iframeId].vm.acceptClick(function (data) {

                        $.each(buttonJson, function (i) {
                            if (buttonJson[i].moduleButtonId == moduleButtonId) {
                                buttonJson[i] = data;
                            }
                        });
                        //console.log('buttonJson:' + JSON.stringify(buttonJson));
                        converToListTreeJson(buttonJson);
                    });
                }
            });
        }
    })
    //删除
    $("#sx-delete-button").click(function () {
        var moduleButtonId = $("#gridTable-button").jqGridRowValue("moduleButtonId");
        if (checkedRowEx(moduleButtonId)) {
            $.each(buttonJson, function (i) {
                if (buttonJson[i].moduleButtonId == moduleButtonId) {
                    buttonJson.splice(i, 1);
                    converToListTreeJson(buttonJson);
                    return false;
                }
            });
        }
    });
    //复制
    $("#sx-copy-button").click(function () {
        var moduleButtonId = $("#gridTable-button").jqGridRowValue("moduleButtonId");
        if (checkedRowEx(moduleButtonId)) {
            dialogOpen({
                id: "OptionModule",
                title: '将按钮复制到指定菜单',
                url: '/authorize/moduleButton/option.html?keyValue=' + moduleButtonId,
                width: "500px",
                height: "500px",
                yes: function (iframeId) {
                    top.frames[iframeId].vm.acceptClick();
                }
            });
        }
    });
}

/*系统按钮end====================================*/

//endregion

//region 表格列数据

/*系统视图begin==================================*/

var columnJson = "";

function getGridView() {
    var moduleId = $("#moduleId").val();
    $.ajax({
        url: "../../authorizeManage/moduleColumn/listModuleId?moduleId=" + keyValue,
        type: "get",
        dataType: "json",
        success: function (data) {
            columnJson = data;
        },
    });
    var $grid = $("#gridTable-view");
    $grid.jqGrid({
        unwritten: false,
        url: "../../authorizeManage/moduleColumn/listModuleId?moduleId=" + keyValue,
        datatype: "json",
        height: $(window).height() - 165,
        width: $(window).width() - 11,
        colModel: [
            {label: "主键", name: "moduleColumnId", index: "moduleColumnId", hidden: true},
            {label: "名称", name: "fullName", index: "fullName", width: 140, align: "left", sortable: false,},
            {label: "编号", name: "enCode", index: "enCode", width: 140, align: "left", sortable: false},
            {label: "描述", name: "description", index: "description", width: 500, align: "left", sortable: false}
        ],
        rowNum: 1000,
        rownumbers: true
    });

    //处理Json
    function ViewListToListTreeJson(columnJson) {
        $.ajax({
            url: "../../authorizeManage/moduleColumn/getListJson",
            data: {json: JSON.stringify(columnJson)},
            type: "post",
            dataType: "json",
            success: function (data) {
                $grid.clearGridData();
                $grid[0].addJSONData(data);
            },
        });
    }

    //新增
    $("#sx-add-view").click(function () {
        dialogOpen({
            id: "viewForm",
            title: '添加视图',
            url: '/authorize/moduleColumn/add.html?moduleId=' + escape(moduleId),
            width: "450px",
            height: "260px",
            yes: function (iframeId) {
                top.frames[iframeId].vm.acceptClick(function (data) {
                    columnJson.push(data);
                    ViewListToListTreeJson(columnJson);
                });
            }
        });
    })
    //编辑
    $("#sx-edit-view").click(function () {
        var moduleColumnId = $("#gridTable-view").jqGridRowValue("moduleColumnId");
        if (checkedRowEx(moduleColumnId)) {
            dialogOpen({
                id: "viewForm",
                title: '编辑视图',
                url: '/authorize/moduleColumn/edit.html?moduleColumnId=' + moduleColumnId + "&moduleId=" + escape(moduleId),
                width: "450px",
                height: "260px",
                yes: function (iframeId) {
                    top.frames[iframeId].vm.acceptClick(function (data) {
                        $.each(columnJson, function (i) {
                            if (columnJson[i].moduleColumnId == moduleColumnId) {
                                columnJson[i] = data;
                            }
                        });
                        ViewListToListTreeJson(columnJson);
                    });
                }
            });
        }
    })
    //删除
    $("#sx-delete-view").click(function () {
        var moduleColumnId = $("#gridTable-view").jqGridRowValue("moduleColumnId");
        if (checkedRowEx(moduleColumnId)) {
            $.each(columnJson, function (i) {
                if (columnJson[i].moduleColumnId == moduleColumnId) {
                    columnJson.splice(i, 1);
                    ViewListToListTreeJson(columnJson);
                    return false;
                }
            });
        }
    });
    //批量添加
    $("#sx-batch-addview").click(function () {
        dialogOpen({
            id: "viewForm",
            title: '批量添加视图',
            url: '/AuthorizeManage/ModuleColumn/BatchAdd?moduleId=' + escape(moduleId),
            width: "450px",
            height: "300px",
            callBack: function (iframeId) {
                top.frames[iframeId].AcceptClick(function (data) {
                    columnJson = data;
                    ViewListToListTreeJson(data);
                });
            }
        });
    })
}

/*系统视图end====================================*/

//endregion

var vm = new Vue({
    el: '#dpLTE',
    data: {
        module: {
            moduleId: 0
        }
    },
    methods: {
        setForm: function () {
            $.SetForm({
                url: '../../sys/menu/info?_' + $.now(),
                param: vm.module.moduleId,
                success: function (data) {
                    //console.log(JSON.stringify(data));
                    $("#form").SetWebControls(data);
                    if (data.isMenu == 1) {
                        $("#btn_next").removeAttr('disabled');
                        $("#btn_finish").attr('disabled', 'disabled');
                    }
                }
            });
        },
        acceptClick: function () {
            if (!$('#form').Validform()) {
                return false;
            }
            var postData = $("#dpLTE").GetWebControls();
            if (postData["parentId"] == "") {
                postData["parentId"] = 0;
            }

            $.SaveForm({
                url: '../../sys/menu/update?_' + $.now(),
                param: {
                    module: postData,
                    moduleButtonListJson: JSON.stringify(buttonJson),
                    moduleColumnListJson: JSON.stringify(columnJson)
                },
                success: function (data) {
                    $.currentIframe().vm.load();
                }
            });
        }
    },
    mounted: function () {
        initialPage();
        buttonOperation();
        getGridButton();
        getGridView();
    }
})
