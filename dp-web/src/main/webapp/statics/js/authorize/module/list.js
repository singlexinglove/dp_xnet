/**
 * 系统菜单js
 */

$(function () {
	initialPage();
    getTree();
	getGrid();
});

function initialPage() {

    //layout布局
    $('#dpLTE').layout({
        applyDemoStyles: true,
        onresize: function () {
            $(window).resize();
        }
    });

    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 165);
            $("#itemTree").setTreeHeight($(window).height() - 47);
        }, 200);
        e.stopPropagation();
    });
}

//加载树
var _parentId = "";
function getTree() {
    var item = {
        height: $(window).height() - 47,
        url: "../../sys/menu/listTree",
        onnodeclick: function (item) {
            //console.log(item);
            _parentId = item.id;
            //$('#btn_Search').trigger("click");
            getGridDataByParentId(item.id);
        }
    };
    //初始化
    $("#itemTree").treeview(item);
}

//根据选中节点查询对应的子节点
function getGridDataByParentId(parentId)
{
    var gridTable = $('#gridTable');
    gridTable.jqGrid('setGridParam', {
        url: "../../sys/menu/list",
        postData: {
            parentId: parentId,
            // condition: $("#queryCondition").find('.dropdown-text').attr('data-value'),
            // keyword: $("#txt_Keyword").val()
        }
    }).trigger('reloadGrid');
}


//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var gridTable = $('#gridTable');

    gridTable.jqGrid({
		url: '../../sys/menu/list?_' + $.now(),
        datatype: "json",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        height: $(window).height() - 165,
        autowidth: true,
        colModel: [
            { label: "主键", name: "moduleId", index: "moduleId", hidden: true },
            { label: "编号", name: "enCode", index: "enCode", width: 150, align: "left" },
            { label: "名称", name: "fullName", index: "fullName", width: 100, align: "left" },
            { label: "地址", name: "urlAddress", index: "urlAddress", width: 350, align: "left" },
            { label: "目标", name: "target", index: "target", width: 60, align: "center" },
            {
                label: "菜单", name: "isMenu", index: "isMenu", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i value=" + cellvalue + " class=\"fa fa-toggle-on\"></i>" : "<i value=" + cellvalue + " class=\"fa fa-toggle-off\"></i>";
                }
            },
            {
                label: "展开", name: "allowExpand", index: "allowExpand", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {
                label: "公共", name: "isPublic", index: "isPublic", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {
                label: "有效", name: "enabledMark", index: "enabledMark", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {label:  '排序', name: 'sortCode', index: 'sortCode', width : 50, align: 'center'},
            { label: "描述", name: "description", index: "description", width: 100, align: "left" }
		],
        viewrecords: true,
        rowNum: 30,
        rowList: [30, 50, 100],
        pager: "#gridPager",
        sortname: 'createDate',
        sortorder: 'desc',
        altRows: true,
        altclass: 'altrow-background',
        rownumbers: true,
        shrinkToFit: false,
        gridview: true,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
	})
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
            $("#gridTable").trigger("reloadGrid");
		},
		save: function() {
			dialogOpen({
				title: '新增',
				url: 'authorize/module/add.html?parentId=' + _parentId,
				width: '700px',
				height: '430px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
                btn: null
			});
		},
		edit: function() {
            var keyValue = $("#gridTable").jqGridRowValue("moduleId");
            if (checkedRowEx(keyValue)) {
				dialogOpen({
					title: '编辑',
					url: 'authorize/module/edit.html?keyValue=' + keyValue,
					width: '700px',
					height: '430px',
                    btn: null,
					success: function(iframeId){
						top.frames[iframeId].vm.module.moduleId =keyValue;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
            var keyValue = $("#gridTable").jqGridRowValue("moduleId");
            if (keyValue) {
                var ids=[];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../sys/menu/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的菜单！', 0);
            }

		}
	}
})