/**
 * 编辑-功能表格列表js
 */
var moduleColumnId = request('moduleColumnId');
var moduleId = request('moduleId');
var dataJson = top.layerForm.columnJson;

//初始化控件
function initControl() {
    if (!!moduleColumnId) {
        $.each(dataJson, function (i) {
            var row = dataJson[i];
            if (row.moduleColumnId == moduleColumnId) {
                $("#dpLTE").SetWebControls(row);
            }
        });
    } else {
        $("#moduleId").val(moduleId);
    }
}

var vm = new Vue({
    el: '#dpLTE',
    data: {
        moduleColumn: {
            moduleColumnId: 0
        }
    },
    methods: {
        setForm: function () {
            $.SetForm({
                url: '../../authorizeManage/moduleColumn/info?_' + $.now(),
                param: vm.moduleColumn.moduleColumnId,
                success: function (data) {
                    $("#form").SetWebControls(data);
                }
            });
        },
        acceptClick: function (callback) {
            if (!$('#form').Validform()) {
                return false;
            }
            var data = $("#dpLTE").GetWebControls();
            data["parentId"] = 0;
            // data["moduleColumnId"] = newGuid();
            callback(data);
            dialogClose();
        }
    },
    mounted: function () {
        initControl();
    }
})