/**
 * 功能表格列表js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 136.5);
        }, 200);
        e.stopPropagation();
    });
}
//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var gridTable = $('#gridTable');

    gridTable.jqGrid({
		url: '../../authorizeManage/moduleColumn/list?_' + $.now(),
        datatype: "json",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            { label:  '列主键', name: 'moduleColumnId', hidden: true },
			{label:  '功能主键', name: 'moduleId', index: 'moduleId', width : '100px', align: 'left'},
			{label:  '父级主键', name: 'parentId', index: 'parentId', width : '100px', align: 'left'},
			{label:  '编码', name: 'enCode', index: 'enCode', width : '100px', align: 'left'},
			{label:  '名称', name: 'fullName', index: 'fullName', width : '100px', align: 'left'},
			{label:  '排序码', name: 'sortCode', index: 'sortCode', width : '100px', align: 'left'},
			{label:  '备注', name: 'description', index: 'description', width : '100px', align: 'left'}

		],
        viewrecords: true,
        rowNum: 30,
        rowList: [30, 50, 100],
        pager: "#gridPager",
        sortname: 'createDate',
        sortorder: 'desc',
        altRows: true,
        altclass: 'altrow-background',
        rownumbers: true,
        shrinkToFit: false,
        gridview: true,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
	})
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
            $("#gridTable").trigger("reloadGrid");
		},
		save: function() {
			dialogOpen({
				title: '新增功能表格列表',
				url: 'authorize/moduleColumn/add.html?_' + $.now(),
				width: '420px',
				height: '350px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
            var keyValue = $("#gridTable").jqGridRowValue("moduleColumnId");
            if (checkedRowEx(keyValue)) {
				dialogOpen({
					title: '编辑功能表格列表',
					url: 'authorize/moduleColumn/edit.html?_' + $.now(),
					width: '420px',
					height: '350px',
					success: function(iframeId){
						top.frames[iframeId].vm.moduleColumn.moduleColumnId =keyValue;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
            var keyValue = $("#gridTable").jqGridRowValue("moduleColumnId");
            if (keyValue) {
                var ids=[];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../authorizeManage/moduleColumn/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的角色！', 0);
            }

		}
	}
})