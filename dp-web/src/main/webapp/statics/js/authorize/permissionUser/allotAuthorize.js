var userId = request('userId');
$(function () {
    initialPage();
    getModuleTree();
    getModuleButtonTree();
    getModuleColumnTree();
    getOrganizeTree();
})
//初始化页面
function initialPage() {
    //加载导向
    $('#wizard').wizard().on('change', function (e, data) {
        var $finish = $("#btn_finish");
        var $next = $("#btn_next");
        if (data.direction == "next") {
            if (data.step == 1) {
                var moduleId = $("#moduleTree").getCheckedAllNodes();
                for (var i = 0; i < moduleId.length; i++) {
                    var $thisid = $("#moduleButtonTree").find('ul').find('[data-value=' + moduleId[i] + ']').parent().parent();
                    $thisid.show();
                    $thisid.parents('ul').find('.' + moduleId[i]).parent().show();
                }
            }
            if (data.step == 2) {
                var moduleId = $("#moduleTree").getCheckedAllNodes();
                for (var i = 0; i < moduleId.length; i++) {
                    var $thisid = $("#moduleColumnTree").find('ul').find('[data-value=' + moduleId[i] + ']').parent().parent();
                    $thisid.show();
                    $thisid.parents('ul').find('.' + moduleId[i]).parent().show();
                }
            }
            if (data.step == 3) {
                $finish.removeAttr('disabled');
                $next.attr('disabled', 'disabled');
            }
        } else {
            $finish.attr('disabled', 'disabled');
            $next.removeAttr('disabled');
        }
    });
    //数据权限 、点击类型触发事件
    $("input[name='authorizeType']").click(function () {
        var value = $(this).val();
        if (value == -5) {
            $("#organizeTreebackground").hide();
        } else {
            $("#organizeTreebackground").show();
        }
    })
    buttonOperation();
}
//获取系统功能
function getModuleTree() {
    var item = {
        height: 540,
        showCheck: true,
        url: "../../authorizeManage/permissionUser/moduleTree?userId=" + userId
    };
    $("#moduleTree").treeview(item);
}
//获取系统按钮
function getModuleButtonTree() {
    var item = {
        height: 540,
        showCheck: true,
        url: "../../authorizeManage/permissionUser/moduleButtonTree?userId=" + userId
    };
    $("#moduleButtonTree").treeview(item);
    $("#moduleButtonTree").find('.bbit-tree-node-el').hide();
}
//获取系统视图
function getModuleColumnTree() {
    var item = {
        height: 540,
        showCheck: true,
        url: "../../authorizeManage/permissionUser/moduleColumnTree?userId=" + userId
    };
    $("#moduleColumnTree").treeview(item);
    $("#moduleColumnTree").find('.bbit-tree-node-el').hide();
}
//获取组织架构
function getOrganizeTree() {
    $.ajax({
        url: "../../authorizeManage/permissionUser/organizeTree?userId=" + userId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data) {
            //console.log(data);
            var $treeJson = data.treeJson;
            var $authorizeType = data.authorizeType;
            var $authorizeData = data.authorizeData;
            var item = {
                height: 330,
                showCheck: true,
                data: JSON.parse($treeJson),
            };

            $("#organizeTree").treeview(item);
            $("input[name='authorizeType'][value=" + $authorizeType + "]").trigger("click");
            $("#organizeTree").find('li.bbit-tree-node').each(function () {
                var $li = $(this);
                $li.css({ position: 'relative' });
                var _data_value = $li.find('a').find('span').attr('data-value');
                var _html = '<div style="position: absolute;right: 0px;top:4px;z-index: 1;"><div class="checkbox">';
                _html += '<label><input name="' + _data_value + '" type="checkbox" value="1" />只读</label>';
                _html += '</div></div>';
                $li.append(_html);
            });
            //console.log($authorizeData);
            $.each($authorizeData, function (i) {
                var row = $authorizeData[i]
                var resourceId = row.ResourceId;
                var IsRead = row.IsRead;
                if (IsRead == 1) {
                    $("input[name='" + resourceId + "']").attr("checked", true);
                }
            });
        }
    });
}
//按钮操作（上一步、下一步、完成、关闭）
function buttonOperation() {
    var $last = $("#btn_last");
    var $next = $("#btn_next");
    var $finish = $("#btn_finish");
    //完成提交保存
    $finish.click(function () {
        var postData = $("#dpForm").GetWebControls();
        postData["userId"] = userId;
        postData["moduleIds"] = String($("#moduleTree").getCheckedAllNodes());
        postData["moduleButtonIds"] = String($("#moduleButtonTree").getCheckedAllNodes());
        postData["moduleColumnIds"] = String($("#moduleColumnTree").getCheckedAllNodes());
        postData["authorizeDataJson"] = JSON.stringify(getDataAuthorize());
        $.SaveForm({
            url: "../../authorizeManage/permissionUser/saveAuthorize",
            param: postData,
            loading: "正在保存授权数据...",
            success: function () {
                $.currentIframe().$("#gridTable").trigger("reloadGrid");
            }
        })
    })
}
//获取数据范围权限选中值、返回Json
function getDataAuthorize() {
    var dataAuthorize = [];
    var authorizeType = $("input[name='authorizeType']:checked").val();
    if (authorizeType == -5) {
        var selectedData = $("#organizeTree").getCheckedAllNodes();
        for (var i = 0; i < selectedData.length; i++) {
            var ResourceId = selectedData[i];
            var IsRead = $("input[name='" + ResourceId + "']:checked").val() == 1 ? 1 : 0;
            var rowdata = {
                ResourceId: ResourceId,
                IsRead: IsRead,
                AuthorizeType: -5
            }
            dataAuthorize.push(rowdata);
        }
    } else {
        var rowdata = {
            IsRead: 0,
            AuthorizeType: authorizeType
        }
        dataAuthorize.push(rowdata);
    }
    return dataAuthorize;
}