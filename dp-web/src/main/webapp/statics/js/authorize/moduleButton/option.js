var keyValue = request('keyValue');

//初始化控件
function initControl() {
    getModuleTree();
};

//加载功能模块树
var moduleId = "";
function getModuleTree() {
    var item = {
        onnodeclick: function (item) {
            moduleId = item.id;
        },
        url: "../../sys/menu/listTree"
    };
    $("#moduleTree").treeview(item);
}


var vm = new Vue({
    el:'#dpLTE',
    data: {
        moduleButton: {
            moduleButtonId: 0
        }
    },
    methods : {
        acceptClick:function() {
            if (!moduleId) {
                dialogMsg('请选择功能菜单！', 0);
                return false;
            }
            $.SaveForm({
                url: '../../authorizeManage/moduleButton/copyForm?_' + $.now(),
                param: {keyValue:keyValue,moduleId:moduleId},
                success: function(data) {
                    $.currentIframe().vm.load();
                }
            });
            // callback(data);
            //dialogClose();

        }
    },
    mounted:function(){
        initControl();
    }
})