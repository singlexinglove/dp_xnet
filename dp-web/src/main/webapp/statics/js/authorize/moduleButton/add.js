/**
 * 新增-功能按钮表js
 */

var parentId = request('parentId');
var moduleButtonId = request('moduleButtonId');
var moduleId = request('moduleId');
var dataJson = top.layerForm.buttonJson;

//初始化控件
function initControl() {
    //上级
    $("#parentId").ComboBoxTree({
        url: "../../authorizeManage/moduleButton/getListTreeJson",
        param: { json: JSON.stringify(dataJson) },
        method: "post",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        description: "==请选择==",
        height: "150px"
    });
    if (!!moduleButtonId) {
        $.each(dataJson, function (i) {
            var row = dataJson[i];
            if (row.moduleButtonId == moduleButtonId) {
                $("#dpLTE").SetWebControls(row);
            }
        });
    } else {
        $("#moduleId").val(moduleId);
        if (!!parentId) {
            $("#parentId").ComboBoxTreeSetValue(parentId);
        } else {
            $("#parentId").ComboBoxTreeSetValue(0);
        }
    }
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		moduleButton: {
			moduleButtonId: 0
		}
	},
	methods : {
	    acceptClick:function(callback) {
            if (!$('#form').Validform()) {
                return false;
            }
            var data = $("#dpLTE").GetWebControls(moduleButtonId);
            if (data["parentId"] == "") {
                data["parentId"] = 0;
            }
            data["moduleButtonId"]=newGuid();
            callback(data);
            dialogClose();

		}
	},
	mounted:function(){
		 initControl();
	}
})
