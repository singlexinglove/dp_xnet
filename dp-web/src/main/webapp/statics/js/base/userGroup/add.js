/**
 * 新增-用户组管理js
 */

//初始化控件
function initControl() {

    //机构
    $("#organizeId").ComboBoxTree({
        url: "../../basemanage/organize/treelist",
        description: "==请选择==",
        height: "100px",
        allowSearch: true,
    });
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		role: {}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
            postData["category"]=4;
		    $.SaveForm({
		    	url: '../../basemanage/role/save?_' + $.now(),
		    	param: postData,//vm.role,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},

    mounted:function(){
        initControl();
    }
})
