/**
 * 角色管理js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 136.5);
        }, 200);
        e.stopPropagation();
    });
}


//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var $gridTable = $('#gridTable');
    $gridTable.jqGrid({
        url: '../../basemanage/role/listForRole?_' + $.now(),
        datatype: "json",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            { label: '主键', name: 'roleId', hidden: true },
            { label: '角色编号', name: 'enCode', index: 'enCode', width: 100, align: 'left' },
            { label: '角色名称', name: 'fullName', index: 'fullName', width: 200, align: 'left' },
            {
                label: '隶属单位', name: 'organizeName', index: 'organizeName', width: 250, align: 'left'
                // formatter: function (cellvalue, options, rowObject) {
                //     return top.clientorganizeData[cellvalue].FullName;
                // }
            },
            {
                label: '创建时间', name: 'createDate', index: 'createDate', width: 130, align: 'center',
                formatter: "date", formatoptions: { srcformat: 'Y-m-d H:i', newformat: 'Y-m-d H:i' }
            },
            {
                label: "公共", name: "isPublic", index: "isPublic", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {
                label: "有效", name: "enabledMark", index: "enabledMark", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            { label: "角色描述", name: "description", index: "description", width: 200, align: "left" }
        ],
        viewrecords: true,
        rowNum: 30,
        rowList: [30, 50, 100],
        pager: "#gridPager",
        sortname: 'createDate',
        sortorder: 'desc',
        altRows: true,
        altclass: 'altrow-background',
        rownumbers: true,
        shrinkToFit: false,
        gridview: true,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
    });
    // $gridTable.authorizeColModel();
    // //查询条件
    // $("#queryCondition .dropdown-menu li").click(function () {
    //     var text = $(this).find('a').html();
    //     var value = $(this).find('a').attr('data-value');
    //     $("#queryCondition .dropdown-text").html(text).attr('data-value', value)
    // });
    // //查询事件
    // $("#btn_Search").click(function () {
    //     var queryJson = {
    //         condition: $("#queryCondition").find('.dropdown-text').attr('data-value'),
    //         keyword: $("#txt_Keyword").val()
    //     }
    //     $gridTable.jqGrid('setGridParam', {
    //         postData: { queryJson: JSON.stringify(queryJson) },
    //         page: 1
    //     }).trigger('reloadGrid');
    // });
    // //查询回车
    // $('#txt_Keyword').bind('keypress', function (event) {
    //     if (event.keyCode == "13") {
    //         $('#btn_Search').trigger("click");
    //     }
    // });
}


var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
            $("#gridTable").trigger("reloadGrid");
		},
		save: function() {
			dialogOpen({
				title: '新增角色',
				url: 'base/role/add.html?_' + $.now(),
				width: '420px',
				height: '350px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
			// var ck = $('#dataGrid').bootstrapTable('getSelections');
            var keyValue = $("#gridTable").jqGridRowValue("roleId");
            if (checkedRowEx(keyValue)) {
				dialogOpen({
					title: '编辑角色',
					url: 'base/role/edit.html?_' + $.now(),
					width: '420px',
					height: '350px',
					success: function(iframeId){
						top.frames[iframeId].vm.role.roleId = keyValue;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			} ;
		},
		remove: function() {
            var keyValue = $("#gridTable").jqGridRowValue("roleId");
            if (keyValue) {
            	var ids=[];
            	ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/role/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的角色！', 0);
            }

		},
        authorize: function () {
            var keyValue = $("#gridTable").jqGridRowValue("roleId");
            var realName = $("#gridTable").jqGridRowValue("fullName");
            if (checkedRowEx(keyValue)) {
                dialogOpen({
                    id: "AllotAuthorize",
                    title: '角色授权 - ' + realName,
                    url: '/authorize/permissionRole/allotAuthorize.html?roleId=' + keyValue,
                    width: "700px",
                    height: "690px",
                    btn: null
                });
            }
        },
        member:function () {
            var keyValue = $("#gridTable").jqGridRowValue("roleId");
            var roleName = $("#gridTable").jqGridRowValue("fullName");
            if (checkedRowEx(keyValue)) {
                dialogOpen({
                    id: "AllotMember",
                    title: '角色成员 - ' + roleName,
                    url: '/authorize/permissionRole/allotMember.html?roleId=' + keyValue,
                    width: "650px",
                    height: "450px",
                    yes: function(iframeId){
                        top.frames[iframeId].acceptClick();
                    }
                });
            }
        },
		authorizeData: function(){
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if(checkedRow(ck)){
				dialogOpen({
					title: '数据权限',
					url: 'base/role/data.html?_' + $.now(),
					scroll : true,
					width: "300px",
					height: "450px",
					success: function(iframeId){
						top.frames[iframeId].vm.role.roleId = ck[0].roleId;
						top.frames[iframeId].vm.setForm();
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					}
				})
			}
		},
	}
})