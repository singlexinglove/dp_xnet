

//初始化控件
function initControl() {
	// console.log('initControl');
    var parentId = request('parentId');
    // console.log(parentId);
    //上级公司
    $("#parentId").ComboBoxTree({
        url: "../../basemanage/organize/treelist",
        description: "==请选择==",
        height: "260px",
        allowSearch: true
    }).ComboBoxTreeSetValue(parentId);
    //公司性质
    $("#nature").ComboBox({
        url: "../../basemanage/dataItemDetail/itemList",
        param: { enCode: "CompanyNature" },
        id: "itemValue",
        text: "itemName",
        description: "==请选择==",
        height: "200px"
    });
    //负责人
    $("#managerId").ComboBoxTree({
        url: "../../sys/user/getOrgDeptUserTree",
        description: "==请选择==",
        height: "220px",
        allowSearch: true
    });
    //省份
    $("#provinceId").ComboBox({
        url: "../../basemanage/area/itemList",
        param: { parentId: "0" },
        id: "areaCode",
        text: "areaName",
        description: "选择省",
        height: "170px"
    }).bind("change", function () {
        var value = $(this).attr('data-value');
        $("#cityId").ComboBox({
            url: "../../basemanage/area/itemList",
            param: { parentId: value },
            id: "areaCode",
            text: "areaName",
            description: "选择市",
            height: "170px"
        });
    });
    //城市
    $("#cityId").ComboBox({
        description: "选择市",
        height: "170px"
    }).bind("change", function () {
        var value = $(this).attr('data-value');
        if (value) {
            $("#countyId").ComboBox({
                url: "../../basemanage/area/itemList",
                param: { parentId: value },
                id: "areaCode",
                text: "areaName",
                description: "选择县/区",
                height: "170px"
            });
        }
    });
    //县/区
    $("#countyId").ComboBox({
        description: "选择县/区",
        height: "170px"
    });
}

/**
 * 新增-机构管理js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		organize: {
		}
	},
	methods : {
		orgTree: function() {
			dialogOpen({
				id: 'layerOrgTree',
				title: '选择机构',
		        url: 'base/org/tree.html?_' + $.now(),
		        scroll : true,
		        width: "300px",
		        height: "450px",
		        yes : function(iframeId) {
		        	top.frames[iframeId].vm.acceptClick();
				}
		    })
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
		    $.SaveForm({
		    	url: '../../basemanage/organize/save?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},

    mounted:function(){
        initControl();
    }
})
