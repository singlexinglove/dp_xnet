/**
 * 编辑-机构管理js
 */

//初始化控件
function initControl() {
    //上级公司
    $("#parentId").ComboBoxTree({
        url: "../../basemanage/organize/treelist",
        description: "==请选择==",
        height: "260px",
        allowSearch: true
    });
    //公司性质
    $("#nature").ComboBox({
        url: "../../basemanage/dataItemDetail/itemList",
        param: { enCode: "CompanyNature" },
        id: "itemValue",
        text: "itemName",
        description: "==请选择==",
        height: "200px"
    });
    //负责人
    $("#managerId").ComboBoxTree({
        url: "../../sys/user/getOrgDeptUserTree",
        description: "==请选择==",
        height: "220px",
        allowSearch: true
    });
    //省份
    $("#provinceId").ComboBox({
        url: "../../basemanage/area/itemList",
        param: { parentId: "0" },
        id: "areaCode",
        text: "areaName",
        description: "选择省",
        height: "170px"
    }).bind("change", function () {
        var value = $(this).attr('data-value');
        $("#cityId").ComboBox({
            url: "../../basemanage/area/itemList",
            param: { parentId: value },
            id: "areaCode",
            text: "areaName",
            description: "选择市",
            height: "170px"
        });
    });
    //城市
    $("#cityId").ComboBox({
        description: "选择市",
        height: "170px"
    }).bind("change", function () {
        var value = $(this).attr('data-value');
        if (value) {
            $("#countyId").ComboBox({
                url: "../../basemanage/area/itemList",
                param: { parentId: value },
                id: "areaCode",
                text: "areaName",
                description: "选择县/区",
                height: "170px"
            });
        }
    });
    //县/区
    $("#countyId").ComboBox({
        description: "选择县/区",
        height: "170px"
    });
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		organize: {}
	},
	methods : {
		setForm: function() {
            initControl();
			$.SetForm({
				url: '../../basemanage/organize/info?_' + $.now(),
		    	param: vm.organize.keyvalue,
		    	success: function(data) {
		    		// vm.org = data;
                    $("#form").SetWebControls(data);
                    $("#foundedTime").val(formatDate(data.foundedTime, "yyyy-MM-dd"));
                    $("#provinceId").trigger("change");
                    if (data.cityId != undefined) {
                        $("#cityId").ComboBoxSetValue(data.cityId).trigger("change");
                        $("#countyId").ComboBoxSetValue(data.countyId);
                    }
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
		    $.ConfirmForm({
		    	url: '../../basemanage/organize/update?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
                    // $.currentIframe().$("#gridTable").resetSelection();
                    // $.currentIframe().$("#gridTable").trigger("reloadGrid");
		    	}
		    });
		},
        mounted:function(){
            // initControl();
        }
	}
})
