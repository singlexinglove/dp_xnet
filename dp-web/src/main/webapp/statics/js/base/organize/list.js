/**
 * 组织机构js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 108.5);
        }, 200);
        e.stopPropagation();
    });
}

//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var $gridTable = $('#gridTable');
    $gridTable.jqGrid({
        url: "../../basemanage/organize/list?_" + $.now(),
        datatype: "json",
        height: $(window).height() - 108.5,
        autowidth: true,
        colModel: [
            { label: '主键', name: 'organizeId', hidden: true },
            { label: "公司名称", name: "fullName", width: 300, align: "left", sortable: false },
            { label: "外文名称", name: "enCode", width: 150, align: "left", sortable: false },
            { label: "中文名称", name: "shortName", width: 150, align: "left", sortable: false },
            { label: "公司性质", name: "nature", width: 100, align: "left", sortable: false },
            {
                label: "成立时间", name: "foundedTime", width: 100, align: "left", sortable: false,
                formatter: function (cellvalue, options, rowObject) {
                    return formatDate(cellvalue, 'yyyy-MM-dd');
                }
            },
            { label: "负责人", name: "manager", width: 100, align: "left", sortable: false },
            { label: "经营范围", name: "fax", width: 200, align: "left", sortable: false },
            { label: "备注", name: "description", width: 200, align: "left", sortable: false }
        ],
        treeGrid: true,
        treeGridModel: "nested",
        ExpandColumn: "enCode",
        rowNum: "all",
        rownumbers: true,
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).resetSelection();
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
    });
    //查询条件设置
    $("#queryCondition .dropdown-menu li").click(function () {
        var text = $(this).find('a').html();
        var value = $(this).find('a').attr('data-value');
        $("#queryCondition .dropdown-text").html(text).attr('data-value', value)
    });
    //查询事件
    $("#btn_Search").click(function () {
        $gridTable.jqGrid('setGridParam', {
            postData: {
                condition: $("#queryCondition").find('.dropdown-text').attr('data-value'),
                keyword: $("#txt_Keyword").val()
            }
        }).trigger('reloadGrid');
    });
    //查询回车事件
    $('#txt_Keyword').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $('#btn_Search').trigger("click");
        }
    });
}

var vm = new Vue({
	el:'#dpLTE',
	methods : {
        load: function() {
            $("#gridTable").resetSelection();
            $("#gridTable").trigger("reloadGrid");
        },
		save: function() {
            var parentId = $("#gridTable").jqGridRowValue("organizeId");
			dialogOpen({
				title: '新增机构',
				url: 'base/organize/add.html?parentId='+parentId+'&_' + $.now(),
				width: '750px',
				height: '500px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
			// var ck = TreeGrid.table.getSelectedRow();
            var keyValue = $("#gridTable").jqGridRowValue("organizeId");
            if (checkedRowEx(keyValue)) {
				dialogOpen({
					title: '编辑机构',
					url: 'base/organize/edit.html?_' + $.now(),
					width: '750px',
					height: '500px',
					success: function(iframeId){
						top.frames[iframeId].vm.organize.keyvalue =keyValue;
						top.frames[iframeId].vm.setForm();
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					},
				});
			}
		},
		remove: function() {
            var keyValue = $("#gridTable").jqGridRowValue("organizeId");
            if (keyValue) {
                var ids=[];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/organize/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的机构！', 0);
            }
		}
	}
})


