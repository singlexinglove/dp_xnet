/**
 * 编辑-用户管理js
 */

//初始化控件
function initControl() {
    // alert("test1");
    //性别
    $("#gender").ComboBox({
        description: "==请选择==",
    });
    //岗位
    $("#postId").ComboBox({
        description: "==请选择==",
        height: "200px",
        allowSearch: true
    });

    //加载角色
    $("#roleId").ComboBox({
        url: "../../basemanage/role/selectItemList?category=1",// + value,
        id: "roleId",
        text: "fullName",
        description: "==请选择==",
        height: "200px",
        allowSearch: true
    });
    //职位
    $("#jobId").ComboBox({
        description: "==请选择==",
        height: "200px",
        allowSearch: true
    });
    //主管
    $("#managerId").ComboBox({
        description: "==请选择==",
        height: "200px",
        allowSearch: true
    });

    //公司
    $("#organizeId").ComboBoxTree({
        url: "../../basemanage/organize/treelist",
        description: "==请选择==",
        height: "200px",
        allowSearch: true,
    }).bind("change", function () {
        var value = $(this).attr('data-value');
        //加载部门
        $("#departmentId").ComboBoxTree({
            url: "../../basemanage/department/getTreelist?organizeId=" + value,
            description: "==请选择==",
            allowSearch: true
        });
        //加载岗位
        $("#postId").ComboBox({
            url: "../../basemanage/role/selectItemList?category=2&organizeId=" + value,
            id: "roleId",
            text: "fullName",
            description: "==请选择==",
            allowSearch: true
        });
    });
    //部门
    $("#departmentId").ComboBoxTree({
        description: "==请选择==",
        height: "200px",
        allowSearch: true
    }).bind("change", function () {
        var value = $(this).attr('data-value');
        //加载用户
        $("#managerId").ComboBox({
            url: "../../sys/user/departmentUser?departmentId=" + value,
            id: "userId",
            text: "realName",
            description: "==请选择==",
            allowSearch: true
        });
        //加载职位
        $("#jobId").ComboBox({
            url: "../../basemanage/role/selectItemList?category=3&organizeId=" + value,
            id: "roleId",
            text: "fullName",
            description: "==请选择==",
            allowSearch: true
        });
    });

};

var vm = new Vue({
    el: '#dpLTE',
    data: {
        roleList: {},
        user: {
            orgId: 0,
            orgName: null,
            status: 1,
            roleIdList: []
        }
    },
    methods: {
        setForm: function () {
            //console.log("setForm");
            $.SetForm({
                url: '../../sys/user/infoUser?_' + $.now(),
                param: vm.user.userId,
                success: function (data) {
                    //vm.user = data;
                    $("#form").SetWebControls(data);
                    $("#organizeId").trigger("change");
                    $("#departmentId").ComboBoxTreeSetValue(data.departmentId).trigger("change");
                    $("#jobId").ComboBoxSetValue(data.jobId);
                    $("#roleId").ComboBoxSetValue(data.roleId);
                    $("#postId").ComboBoxSetValue(data.postId);
                    $("#managerId").ComboBoxSetValue(data.managerId);
                    $("#birthday").val(formatDate(data.birthday, "yyyy-MM-dd"));
                    $("#password").val("******").attr('disabled', 'disabled');
                }
            });
        },
        acceptClick: function () {
            if (!$('#form').Validform()) {
                return false;
            }
            var postData = $("#dpLTE").GetWebControls();
            postData["jobName"] = $("#jobId").attr('data-text');
            postData["postName"] = $("#postId").attr('data-text');
            postData["manager"] = $("#managerId").attr('data-text');
            $.ConfirmForm({
                url: '../../sys/user/update?_' + $.now(),
                param: postData,
                success: function (data) {
                    //console.log($.currentIframe().vm);
                    $.currentIframe().vm.load();

                }
            });
        }
    },
    created: function () {
        //this.getRoleList();
    },
    mounted: function () {
        initControl();
    }
})
