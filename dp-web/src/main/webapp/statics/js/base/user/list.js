/**
 * 用户管理js
 */

$(function () {
    initialPage();
    GetGridJQ();
});

function test() {
    var data = JSON.stringify({sortOrder: "asc", pageSize: 10, pageNumber: 1, roleName: null});
    $.ajax({
        url: '../../basemanage/role/list?_' + $.now(),
        type: "post",
        data: data,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
        }
    });
}


function initialPage() {
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 136.5);
        }, 200);
        e.stopPropagation();
    });
}

//加载表格
function GetGridJQ() {
    var selectedRowIndex = 0;
    var $gridTable = $('#gridTable');
    $gridTable.jqGrid({
        url: '../../sys/user/list?_' + $.now(),
        datatype: "json",
        ajaxGridOptions: {contentType: 'application/json; charset=utf-8'},
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            // { label: '主键', name: 'userId', hidden: true },
            {label: '编号', name: 'userId', index: 'userId', width: 100, align: 'left', sortable: false, hidden: true},
            {label: '登录名', name: 'account', index: 'account', width: 100, align: 'left'},
            {label: '姓名', name: 'realName', index: 'realName', width: 100, align: 'left'},
            {
                label: '性别', name: 'gender', index: 'gender', width: 45, align: 'center',
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "男" : "女";
                }
            },
            {label: '手机', name: 'mobile', index: 'mobile', width: 100, align: 'center'},
            {
                label: '所属机构', name: 'organizeName', index: 'organizeName', width: 200, align: 'left'
            },
            {
                label: '部门', name: 'departmentName', index: 'departmentName', width: 100, align: 'left'
            },
            {label: '岗位', name: 'postName', index: 'postName', width: 100, align: 'center'},
            {label: '职位', name: 'jobName', index: 'jobName', width: 100, align: 'center'},
            // {
            //     label: '角色', name: 'RoleId', index: 'RoleId', width: 100, align: 'left',
            //     formatter: function (cellvalue, options, rowObject) {
            //         return top.clientroleData[cellvalue] == null ? "" : top.clientroleData[cellvalue].FullName;
            //     }
            // },
            {label: '主管', name: 'manager', index: 'manager', width: 100, align: 'left'},
            {
                label: "状态", name: "enabledMark", index: "enabledMark", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == '1') {
                        return '<span onclick=\"vm.disable(\'' + rowObject.userId + '\')\" class=\"label label-success\" style=\"cursor: pointer;\">正常</span>';
                    } else if (cellvalue == '0') {
                        return '<span onclick=\"vm.enable(\'' + rowObject.userId + '\')\" class=\"label label-default\" style=\"cursor: pointer;\">禁用</span>';
                    }
                }
            },
            {label: "创建时间", name: "createDate", index: "createDate", width: 200, align: "left"},
            {label: "备注", name: "description", index: "description", width: 200, align: "left"}
        ],
        viewrecords: true,
        rowNum: 30,
        rowList: [30, 50, 100],
        pager: "#gridPager",
        sortname: 'userId',
        sortorder: 'desc',
        altRows: true,
        altclass: 'altrow-background',
        rownumbers: true,
        shrinkToFit: false,
        gridview: true,
        serializeGridData: function (postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
    });
    //$gridTable.authorizeColModel()
    //查询条件
    // $("#queryCondition .dropdown-menu li").click(function () {
    //     var text = $(this).find('a').html();
    //     var value = $(this).find('a').attr('data-value');
    //     $("#queryCondition .dropdown-text").html(text).attr('data-value', value)
    // });
    // //查询事件
    // $("#btn_Search").click(function () {
    //     var queryJson = {
    //         condition: $("#queryCondition").find('.dropdown-text').attr('data-value'),
    //         keyword: $("#txt_Keyword").val()
    //     }
    //     $gridTable.jqGrid('setGridParam', {
    //         postData: { queryJson: JSON.stringify(queryJson) }, page: 1
    //     }).trigger('reloadGrid');
    // });
    // //查询回车
    // $('#txt_Keyword').bind('keypress', function (event) {
    //     if (event.keyCode == "13") {
    //         $('#btn_Search').trigger("click");
    //     }
    // });
}

//导出
function btn_export() {
    location.href = "../../BaseManage/User/ExportUserList";
}

////禁用
// function btn_disabled(keyValue) {
//     if (keyValue == undefined) {
//         keyValue = $("#gridTable").jqGridRowValue("userId");
//     }
//     //var keyValue = $("#gridTable").jqGridRowValue("userId");
//
//     if (checkedRow(keyValue)) {
//         $.ConfirmAjax({
//             msg: "注：您确定要【禁用】账户？",
//             url: "../../BaseManage/User/DisabledAccount",
//             param: { keyValue: keyValue },
//             success: function (data) {
//                 $("#gridTable").trigger("reloadGrid");
//             }
//         });
//     }
// }
// //启用
// function btn_enabled(keyValue) {
//     if (keyValue == undefined) {
//         keyValue = $("#gridTable").jqGridRowValue("UserId");
//     }
//     if (checkedRow(keyValue)) {
//         $.ConfirmAjax({
//             msg: "注：您确定要【启用】账户？",
//             url: "../../BaseManage/User/EnabledAccount",
//             param: { keyValue: keyValue },
//             success: function (data) {
//                 $("#gridTable").trigger("reloadGrid");
//             }
//         });
//     }
// }
//用户授权
function btn_authorize() {
    var keyValue = $("#gridTable").jqGridRowValue("UserId");
    var RealName = $("#gridTable").jqGridRowValue("RealName");
    if (checkedRow(keyValue)) {
        dialogOpen({
            id: "AllotAuthorize",
            title: '用户授权 - ' + RealName,
            url: '/AuthorizeManage/PermissionUser/AllotAuthorize?userId=' + keyValue,
            width: "700px",
            height: "690px",
            btn: null
        });
    }
}

//IP过滤
function btn_ipfilter() {
    var keyValue = $("#gridTable").jqGridRowValue("UserId");
    var FullName = $("#gridTable").jqGridRowValue("RealName");
    if (checkedRow(keyValue)) {
        dialogOpen({
            id: "FilterIP",
            title: 'TCP/IP 地址访问限制 - ' + FullName,
            url: '/AuthorizeManage/FilterIP/Index?objectId=' + keyValue + '&objectType=Uesr',
            width: "600px",
            height: "400px",
            btn: null
        });
    }
}

//时段过滤
function btn_timefilter() {
    var keyValue = $("#gridTable").jqGridRowValue("UserId");
    var FullName = $("#gridTable").jqGridRowValue("RealName");
    if (checkedRow(keyValue)) {
        dialogOpen({
            id: "FilterTime",
            title: '时段访问过滤 - ' + FullName,
            url: '/AuthorizeManage/FilterTime/Index?objectId=' + keyValue + '&objectType=Uesr',
            width: "640px",
            height: "480px",
            callBack: function (iframeId) {
                top.frames[iframeId].AcceptClick();
            }
        });
    }
}


var vm = new Vue({
    el: '#dpLTE',
    data: {
        keyword: null
    },
    methods: {
        load: function () {
            $("#gridTable").trigger("reloadGrid");
        },
        save: function () {
            dialogOpen({
                title: '新增用户',
                url: 'base/user/add.html?_' + $.now(),
                width: '600px',
                height: '510px',
                //scroll : true,
                yes: function (iframeId) {
                    top.frames[iframeId].vm.acceptClick();
                },
            });
        },
        edit: function () {
            //var ck = $('#dataGrid').bootstrapTable('getSelections');
            var ck = $('#gridTable').jqGridRowValue('userId');
            //console.log(ck);
            if (checkedRowEx(ck)) {
                dialogOpen({
                    title: '编辑用户',
                    url: 'base/user/edit.html?_' + $.now(),
                    width: '600px',
                    height: '510px',
                    scroll: true,
                    success: function (iframeId) {
                        //console.log(top.frames[iframeId].vm.user);
                        top.frames[iframeId].vm.user.userId = ck;// ck[0].userId;
                        top.frames[iframeId].vm.setForm();
                    },
                    yes: function (iframeId) {
                        top.frames[iframeId].vm.acceptClick();
                    },
                });
            }
        },
        remove: function () {
            var keyValue = $("#gridTable").jqGridRowValue("userId");
            if (keyValue) {
                var ids = [];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../sys/user/remove?_" + $.now(),
                    param: ids,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的用户！', 0);
            }
        },
        disable: function (keyValue) {
            //console.log(keyValue);
            if (keyValue == undefined) {
                keyValue = $("#gridTable").jqGridRowValue("userId");
            }
            //console.log(keyValue);
            var ids = [];
            ids.push(keyValue);
            if (checkedRow(ids)) {
                $.ConfirmForm({
                    msg: '您确定要【禁用】所选账户吗？',
                    url: '../../sys/user/disable?_' + $.now(),
                    param: ids,
                    success: function (data) {
                        $("#gridTable").trigger("reloadGrid");
                    }
                });
            }
        },
        enable: function (keyValue) {
            if (keyValue == undefined) {
                keyValue = $("#gridTable").jqGridRowValue("userId");
            }
            var ids = [];
            ids.push(keyValue);
            if (checkedRow(ids)) {
                $.ConfirmForm({
                    msg: '您是否要【启用】所选账户吗？',
                    url: '../../sys/user/enable?_' + $.now(),
                    param: ids,
                    success: function (data) {
                        $("#gridTable").trigger("reloadGrid");
                    }
                });
            }
        },
        reset: function () {
            var keyValue = $("#gridTable").jqGridRowValue("userId");
            var ids = [];
            ids.push(keyValue);
            var account = $("#gridTable").jqGridRowValue("account");
            var realName = $("#gridTable").jqGridRowValue("realName");
            if (checkedRow(ids)) {
                dialogOpen({
                    title: '重置密码',
                    url: 'base/user/reset.html?account=' +escape(account)+'&realName='+  escape(realName),
                    width: '400px',
                    height: '220px',
                    success: function (iframeId) {
                        top.frames[iframeId].vm.user.userId = keyValue;
                    },
                    yes: function (iframeId) {
                        top.frames[iframeId].vm.acceptClick();
                    },
                });
            }
        },
        authorize: function () {
            var keyValue = $("#gridTable").jqGridRowValue("userId");
            var realName = $("#gridTable").jqGridRowValue("realName");
            if (checkedRowEx(keyValue)) {
                dialogOpen({
                    id: "AllotAuthorize",
                    title: '用户授权 - ' + realName,
                    url: '/authorize/permissionUser/allotAuthorize.html?userId=' + keyValue,
                    width: "700px",
                    height: "690px",
                    btn: null
                });
            }
        }

    }
})