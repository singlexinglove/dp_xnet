/**
 * 新增-数据字典分类表js
 */
var parentId = request('parentId');
//初始化控件
function initControl() {
    //上级
    $("#parentId").ComboBoxTree({
        url: "../../basemanage/dataItem/treelist",
        description: "==请选择==",
        height: "230px"
    }).ComboBoxTreeSetValue(parentId);;
}
var vm = new Vue({
	el:'#dpLTE',
	data: {
		dataItem: {
			itemId: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
            if (postData["parentId"] == "") {
                postData["parentId"] = 0;
            }
		    $.SaveForm({
		    	url: '../../basemanage/dataItem/save?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
                    top.DataItemCategory.vm.load();
		    	}
		    });
		}
	},
	mounted:function(){
		 initControl();
	}
})
