/**
 * 数据字典分类表js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 136.5);
        }, 200);
        e.stopPropagation();
    });
}
//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var gridTable = $('#gridTable');

    gridTable.jqGrid({
		url: '../../basemanage/dataItem/treeGridList?_' + $.now(),
        datatype: "json",
        postData:{keyword:null},
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            { label: '主键', name: 'itemId', hidden: true },
            { label: '名称', name: 'itemName', index: 'itemName', width: 200, align: 'left' },
            { label: '编号', name: 'itemCode', index: 'itemCode', width: 200, align: 'left' },
            { label: '排序', name: 'sortCode', index: 'sortCode', width: 80, align: 'left' },
            {
                label: "树型", name: "isTree", index: "isTree", width: 50, align: "center",
                formatter: function (cellvalue) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {
                label: "有效", name: "enabledMark", index: "enabledMark", width: 50, align: "center",
                formatter: function (cellvalue) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            { label: "备注", name: "description", index: "description", width: 200, align: "left" }
        ],
        treeGrid: true,
        treeGridModel: "nested",
        ExpandColumn: "itemCode",
        rowNum: "10000",
        rownumbers: true,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).resetSelection();
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
	});
    //查询事件
    $("#btn_Search").click(function () {
        gridTable.jqGrid('setGridParam', {
            postData: { keyword: $("#txt_Keyword").val() },
        }).trigger('reloadGrid');
    });
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
            $("#gridTable").resetSelection();
            $("#gridTable").trigger("reloadGrid");
		},
		save: function() {
            var parentId = $("#gridTable").jqGridRowValue("itemId");
			dialogOpen({
				title: '新增',
				url: 'base/dataItem/add.html?parentId=' + parentId+'&_' + $.now(),
				width: '420px',
				height: '400px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
            var keyValue = $("#gridTable").jqGridRowValue("itemId");
            if (checkedRowEx(keyValue)) {
				dialogOpen({
					title: '编辑',
					url: 'base/dataItem/edit.html?keyValue='+keyValue+'&_' + $.now(),
					width: '420px',
					height: '400px',
					success: function(iframeId){
						top.frames[iframeId].vm.dataItem.itemId =keyValue;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
            var keyValue = $("#gridTable").jqGridRowValue("itemId");
            if (keyValue) {
                var ids=[];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/dataItem/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的记录！', 0);
            }

		}
	}
})