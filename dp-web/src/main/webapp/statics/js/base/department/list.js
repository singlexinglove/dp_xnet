/**
 * 部门信息表js
 */

$(function () {
    initialPage();
    getGrid();
});

function initialPage() {
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 136.5);
        }, 200);
        e.stopPropagation();
    });
}

//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var gridTable = $('#gridTable');

    gridTable.jqGrid({
        url: '../../basemanage/department/getTreeListJson?_' + $.now(),
        datatype: "json",
        ajaxGridOptions: {contentType: 'application/json; charset=utf-8'},
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            {label: '主键', name: 'departmentId', hidden: true},
            {label: '公司Id', name: 'organizeId', hidden: true},
            {label: 'Sort', name: 'sort', hidden: true},
            {label: "部门名称", name: "fullName", width: 200, align: "left", sortable: false},
            {label: "部门编号", name: "enCode", width: 100, align: "center", sortable: false},
            {label: "部门简称", name: "shortName", width: 100, align: "left", sortable: false},
            {label: "部门性质", name: "nature", width: 100, align: "left", sortable: false},
            {label: "负责人", name: "manager", width: 100, align: "left", sortable: false},
            {label: "电话号", name: "outerPhone", width: 100, align: "left", sortable: false},
            {label: "分机号", name: "innerPhone", width: 100, align: "center", sortable: false},
            {label: "备注", name: "description", width: 100, align: "left", sortable: false}

        ],
        treeGrid: true,
        treeGridModel: "nested",
        ExpandColumn: "enCode",
        rowNum: "all",
        rownumbers: true,
        // rowNum: 30,
        // rowList: [30, 50, 100],
        // pager: "#gridPager",
        // sortname: 'createDate',
        // sortorder: 'desc',
        // altRows: true,
        // altclass: 'altrow-background',
        // rownumbers: true,
        // shrinkToFit: false,
        //gridview: true,
        // serializeGridData: function(postdata) {
        //     return JSON.stringify(postdata);
        // },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).resetSelection();
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
    })
}

var vm = new Vue({
    el: '#dpLTE',
    data: {
        keyword: null
    },
    methods: {
        load: function () {
            $("#gridTable").trigger("reloadGrid");
        },
        save: function () {
            var keyValue = $("#gridTable").jqGridRowValue("departmentId");
            if (checkedRowEx(keyValue)) {
                var organizeId = $("#gridTable").jqGridRowValue("organizeId");
                var parentId = keyValue;
                var sort = $("#gridTable").jqGridRowValue("sort");
                if (sort == 'Organize') {
                    parentId = 0;
                }
                dialogOpen({
                    title: '新增部门信息表',
                    url: 'base/department/add.html?departmentId=' + keyValue + '&organizeId=' + organizeId + '&parentId=' + parentId+'&_' + $.now(),
                    width: '700px',
                    height: '400px',
                    yes: function (iframeId) {
                        top.frames[iframeId].vm.acceptClick();
                    },
                });
            }
        },
        edit: function () {
            var keyValue = $("#gridTable").jqGridRowValue("departmentId");
            if (checkedRowEx(keyValue)) {
                var sort = $("#gridTable").jqGridRowValue("sort");
                if (sort == 'Organize') {
                    return false;
                }
                dialogOpen({
                    title: '编辑部门信息表',
                    url: 'base/department/edit.html?_' + $.now(),
                    width: '700px',
                    height: '400px',
                    success: function (iframeId) {
                        top.frames[iframeId].vm.department.departmentId = keyValue;
                        top.frames[iframeId].vm.setForm();
                    },
                    yes: function (iframeId) {
                        top.frames[iframeId].vm.acceptClick();
                    }
                });
            }
        },
        remove: function () {
            var keyValue = $("#gridTable").jqGridRowValue("departmentId");
            if (keyValue) {
                var sort = $("#gridTable").jqGridRowValue("sort");
                if (sort == 'Organize') return;
                var ids = [];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/department/remove?_" + $.now(),
                    param: ids,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的记录！', 0);
            }

        }
    }
})