/**
 * 编辑-部门信息表js
 */
var keyValue = request('keyValue');
var organizeId ;
var departmentId ;
var parentId ;
//初始化控件
function initControl() {
    //上级部门
    $("#organizeTree").ComboBoxTree({
        url: "../../basemanage/department/getOrgDeptTree",
        description: "==请选择==",
        height: "160px",
        allowSearch: true,
        click: function (item) {
            if (item.Sort == 'Organize') {
                organizeId = item.id;
                parentId = 0;
            } else {
                parentId = item.id;
            }
        }
    });
    //公司性质
    $("#nature").ComboBox({
        url: "../../basemanage/dataItemDetail/itemList",
        param: { enCode: "CompanyNature" },
        id: "itemValue",
        text: "itemName",
        description: "==请选择==",
        height: "200px"
    });
    //负责人
    $("#managerId").ComboBoxTree({
        url: "../../sys/user/getOrgDeptUserTree",
        description: "==请选择==",
        height: "220px",
        allowSearch: true
    });
}
var vm = new Vue({
	el:'#dpLTE',
	data: {
		department: {
			departmentId: 0
		}
	},
	methods : {
		setForm: function() {
			$.SetForm({
				url: '../../basemanage/department/info?_' + $.now(),
		    	param: vm.department.departmentId,
		    	success: function(data) {
                    $("#form").SetWebControls(data);
                    if (data.parentId == 0) {
                        $("#organizeTree").ComboBoxTreeSetValue(data.organizeId);
                    } else {
                        $("#organizeTree").ComboBoxTreeSetValue(data.parentId);
                    }
                    parentId = data.parentId;
                    organizeId = data.organizeId;
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
            postData["organizeId"] = organizeId;
            postData["parentId"] = parentId;
            postData["manager"] = $("#managerId").attr('data-text');
		    $.ConfirmForm({
		    	url: '../../basemanage/department/update?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
	mounted:function(){
		initControl();
	}
})