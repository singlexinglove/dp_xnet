/**
 * 新增-部门信息表js
 */
var keyValue = request('keyValue');
var organizeId = request('organizeId');
var departmentId = request('departmentId');
var parentId = request('parentId');
//初始化控件
function initControl() {
    //上级部门
    $("#organizeTree").ComboBoxTree({
        url: "../../basemanage/department/getOrgDeptTree",
        description: "==请选择==",
        height: "160px",
        allowSearch: true,
        click: function (item) {
            if (item.Sort == 'Organize') {
                organizeId = item.id;
                parentId = 0;
            } else {
                parentId = item.id;
            }
        }
    }).ComboBoxTreeSetValue(departmentId);
    //公司性质
    $("#nature").ComboBox({
        url: "../../basemanage/dataItemDetail/itemList",
        param: { enCode: "CompanyNature" },
        id: "itemValue",
        text: "itemName",
        description: "==请选择==",
        height: "200px"
    });
    //负责人
    $("#managerId").ComboBoxTree({
        url: "../../sys/user/getOrgDeptUserTree",
        description: "==请选择==",
        height: "220px",
        allowSearch: true
    });
}
var vm = new Vue({
	el:'#dpLTE',
	data: {
		department: {
			departmentId: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
            postData["organizeId"] = organizeId;
            postData["parentId"] = parentId;
            postData["manager"] = $("#managerId").attr('data-text');
		    $.SaveForm({
		    	url: '../../basemanage/department/save?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
	mounted:function(){
		 initControl();
	}
})
