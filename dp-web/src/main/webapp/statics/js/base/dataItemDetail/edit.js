/**
 * 编辑-js
 */

/**
 * 新增-js
 */
var keyValue = request('keyValue');
var itemId = request('itemId');
var parentId = request('parentId');
// $(function () {
//     initControl();
// })
//初始化控件
// function initControl() {
//     //获取表单
//     if (!!keyValue) {
//         $.SetForm({
//             url: "../../SystemManage/DataItemDetail/GetFormJson",
//             param: { keyValue: keyValue },
//             success: function (data) {
//                 $("#dpLTE").SetWebControls(data);
//             }
//         });
//     } else {
//         $("#parentId").val(parentId);
//         $("#itemId").val(itemId);
//     }
// }
//
// var vm = new Vue({
//     el:'#dpLTE',
//     data: {
//         dataItemDetail: {
//             itemDetailId: 0
//         }
//     },
//     methods : {
//         acceptClick: function() {
//             if (!$('#dpLTE').Validform()) {
//                 return false;
//             }
//             var postData = $("#dpLTE").GetWebControls();
//             $.SaveForm({
//                 url: '../../basemanage/dataItemDetail/save?_' + $.now(),
//                 param: postData,
//                 success: function(data) {
//                     $.currentIframe().vm.load();
//                 }
//             });
//         }
//     },
//     mounted:function(){
//         initControl();
//     }
// })

var vm = new Vue({
	el:'#dpLTE',
	data: {
		dataItemDetail: {
			itemDetailId: 0
		}
	},
	methods : {
		setForm: function() {
			$.SetForm({
				url: '../../basemanage/dataItemDetail/info?_' + $.now(),
		    	param: keyValue,
		    	success: function(data) {
		    		vm.dataItemDetail = data;
                    $("#dpLTE").SetWebControls(data);
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#dpLTE').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
		    $.ConfirmForm({
		    	url: '../../basemanage/dataItemDetail/update?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
	mounted:function(){
		//initControl();
	}
})