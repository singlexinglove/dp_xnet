/**
 * js
 */

$(function() {
    initialPage();
    getTree();
    getGrid();
});

//初始化页面
function initialPage() {
    //layout布局
    $('#layout').layout({
        applyDemoStyles: true,
        onresize: function () {
            $(window).resize();
        }
    });
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 141);
            $("#itemTree").setTreeHeight($(window).height() - 52);
        }, 200);
        e.stopPropagation();
    });
}

//加载树
var _itemId = "";
var _itemName = "";
var _isTree = "";
function getTree() {
    var item = {
        height: $(window).height() - 52,
        url: "../../basemanage/dataItem/treelist",
        onnodeclick: function (item) {
            _itemId = item.id;
            _itemName = item.text;
            _isTree = item.isTree;
            $("#titleinfo").html(_itemName + "(" + item.value + ")");
            $("#txt_Keyword").val("");
            $('#btn_Search').trigger("click");
        }
    };
    //初始化
    $("#itemTree").treeview(item);
}

//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var gridTable = $('#gridTable');
    var queryJson = {
        itemId: null
    }
    gridTable.jqGrid({
        url: "../../basemanage/dataItemDetail/treeList",
        datatype: "json",
        postData: queryJson,//JSON.stringify(),
        ajaxGridOptions: {contentType: 'application/json; charset=utf-8'},
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            {label: '主键', name: 'itemDetailId', hidden: true},
            {label: '项目名', name: 'itemName', index: 'itemName', width: 200, align: 'left', sortable: false},
            {label: '项目值', name: 'itemValue', index: 'itemValue', width: 200, align: 'left', sortable: false},
            {label: '简拼', name: 'simpleSpelling', index: 'simpleSpelling', width: 150, align: 'left', sortable: false},
            {label: '排序', name: 'sortCode', index: 'sortCode', width: 80, align: 'center', sortable: false},
            {
                label: "默认", name: "isDefault", index: "isDefault", width: 50, align: "center", sortable: false,
                formatter: function (cellvalue) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {
                label: "有效", name: "enabledMark", index: "enabledMark", width: 50, align: "center", sortable: false,
                formatter: function (cellvalue) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            {label: "备注", name: "description", index: "description", width: 200, align: "left", sortable: false}

        ],
        treeGrid: true,
        treeGridModel: "nested",
        ExpandColumn: "itemValue",
        rowNum: "10000",
        rownumbers: true,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
    });
    //查询条件
    $("#queryCondition .dropdown-menu li").click(function () {
        var text = $(this).find('a').html();
        var value = $(this).find('a').attr('data-value');
        $("#queryCondition .dropdown-text").html(text).attr('data-value', value)
    });
    //查询事件
    $("#btn_Search").click(function () {
        var queryJson = {
            itemId: _itemId,
            condition: $("#queryCondition").find('.dropdown-text').attr('data-value'),
            keyword: $("#txt_Keyword").val()
        }
        gridTable.jqGrid('setGridParam', {
            url: "../../basemanage/dataItemDetail/treeList",
            postData: queryJson,//JSON.stringify(),
        }).trigger('reloadGrid');
    });
    //查询回车
    $('#txt_Keyword').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $('#btn_Search').trigger("click");
        }
    });
}

var vm = new Vue({
    el: '#dpLTE',
    data: {
        keyword: null
    },
    methods: {
        load: function () {
            $("#gridTable").resetSelection();
            $("#gridTable").trigger("reloadGrid");
        },
        save: function () {
            if (!_itemId) {
                return false;
            }
            var parentId = $("#gridTable").jqGridRowValue("itemDetailId");
            if (_isTree != 1) {
                parentId = 0;
            }
            dialogOpen({
                id: "Form",
                title: '新增',
                url: 'base/dataItemDetail/add.html?parentId=' + parentId + "&itemId=" + _itemId,
                width: "500px",
                height: "370px",
                yes: function (iframeId) {
                    top.frames[iframeId].vm.acceptClick();
                }
            });
        },
        edit: function () {
            var keyValue = $("#gridTable").jqGridRowValue("itemDetailId");
            if (checkedRowEx(keyValue)) {
                dialogOpen({
                    title: '编辑',
                    url: 'base/dataItemDetail/edit.html?keyValue='+keyValue+'&_' + $.now(),
                    width: '420px',
                    height: '350px',
                    success: function (iframeId) {
                        top.frames[iframeId].vm.dataItemDetail.itemDetailId = keyValue;
                        top.frames[iframeId].vm.setForm();
                    },
                    yes: function (iframeId) {
                        top.frames[iframeId].vm.acceptClick();
                    }
                });
            }
        },
        remove: function () {
            var keyValue = $("#gridTable").jqGridRowValue("itemDetailId");
            if (keyValue) {
                var ids = [];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/dataItemDetail/remove?_" + $.now(),
                    param: ids,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的记录!', 0);
            }

        },
        category:function(){
            dialogOpen({
                id: "DataItemCategory",
                title: '字典分类',
                url: '/base/dataItem/list.html',
                width: "800px",
                height: "600px",
                btn: null
            });
        },
        mounted: function () {
            // initialPage();
            // getGrid();
        }
    }
})