/**
 * 新增-js
 */
var keyValue = request('keyValue');
var itemId = request('itemId');
var parentId = request('parentId');
// $(function () {
//     initControl();
// })
//初始化控件
function initControl() {
    //获取表单
        $("#parentId").val(parentId);
        $("#itemId").val(itemId);

}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		dataItemDetail: {
			itemDetailId: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#dpLTE').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
		    $.SaveForm({
		    	url: '../../basemanage/dataItemDetail/save?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
	mounted:function(){
		  initControl();
	}
})
