/**
 * 编辑-行政区域js
 */

/**
 * 新增-行政区域js
 */

var keyValue = request('keyValue');
// var parentId = request('parentId');

//初始化控件
function initControl() {
}
var vm = new Vue({
    el:'#dpLTE',
    data: {
        area: {
            areaId:null
        }
    },
    methods : {
        setForm: function() {
            $.SetForm({
                url: '../../basemanage/area/info?_' + $.now(),
                param: vm.area.areaId ,
                success: function(data) {
                   // console.log(data);
                    $("#dpLTE").SetWebControls(data);
                }
            });
        },
        acceptClick: function() {
            if (!$('#dpLTE').Validform()) {
                return false;
            }
            var postData = $("#dpLTE").GetWebControls();
            postData["areaId"]=vm.area.areaId;
            $.SaveForm({
                url: '../../basemanage/area/update?_' + $.now(),
                param: postData,
                success: function(data) {
                    $.currentIframe().vm.load();
                }
            });
        },
        mounted:function(){
            initControl();
        }
    }
})