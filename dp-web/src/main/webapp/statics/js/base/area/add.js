/**
 * 新增-行政区域js
 */

var keyValue = request('keyValue');
var parentId = request('parentId');

//初始化控件
function initControl() {
        $("#parentId").val(parentId);
}
var vm = new Vue({
	el:'#dpLTE',
	data: {
		area: {
			parentCode: 0,
			parentName: null,
			status: 1,
			orderNum: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#dpLTE').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
		    $.SaveForm({
		    	url: '../../basemanage/area/save?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		},
        mounted:function(){
            initControl();
        }
	}
})
