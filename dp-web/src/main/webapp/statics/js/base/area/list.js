/**
 * 行政区域js
 */

$(function() {
	initialPage();
    getTree();
    getGrid();
});

// function initialPage() {
// 	$("#treePanel").css('height', $(window).height()-54);
// 	$(window).resize(function() {
// 		$("#treePanel").css('height', $(window).height()-54);
// 		$('#dataGrid').bootstrapTable('resetView', {
// 			height : $(window).height() - 108
// 		});
// 	});
// }
//初始化页面
function initialPage() {
    //layout布局
    $('#layout').layout({
        applyDemoStyles: true,
        onresize: function () {
            $(window).resize()
        }
    });
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 141);
            $("#itemTree").setTreeHeight($(window).height() - 52);
        }, 200);
        e.stopPropagation();
    });
}

//加载树
var AreaCode = 0;
function getTree() {
    var item = {
        height: $(window).height() - 52,
        url: "../../basemanage/area/treelist",
        onnodeclick: function (item) {
            AreaCode = item.id;
            //展开下级
            $(".bbit-tree-selected").children('.bbit-tree-ec-icon').trigger("click");
            $('#btn_Search').trigger("click");
        },
    };
    //初始化
    $("#itemTree").treeview(item);
}
//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var $gridTable = $("#gridTable");
    $gridTable.jqGrid({
        url: "../../basemanage/area/itemList",
        datatype: "json",
        height: $(window).height() - 141,
        autowidth: true,
        colModel: [
            { label: '主键', name: 'areaId', hidden: true },
            { label: '编号', name: 'areaCode', index: 'areaCode', width: 200, align: 'left' },
            { label: '名称', name: 'areaName', index: 'areaName', width: 300, align: 'left' },
            { label: '简拼', name: 'simpleSpelling', index: 'simpleSpelling', width: 100, align: 'left' },
            { label: '级', name: 'layer', index: 'layer', width: 50, align: 'center' },
            {
                label: "有效", name: "enabledMark", index: "enabledMark", width: 50, align: "center",
                formatter: function (cellvalue, options, rowObject) {
                    return cellvalue == 1 ? "<i class=\"fa fa-toggle-on\"></i>" : "<i class=\"fa fa-toggle-off\"></i>";
                }
            },
            { label: "备注", name: "description", index: "description", width: 200, align: "left" }
        ],
        rowNum: "10000",
        rownumbers: true,
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
    });
    //查询事件
    $("#btn_Search").click(function () {
        $gridTable.jqGrid('setGridParam', {
            postData: { parentId: AreaCode, keyword: $("#txt_Keyword").val() },
        }).trigger('reloadGrid');
    });
}


var vm = new Vue({
	el : '#dpLTE',
	data : {
		keyword : null,
		parentCode : '0',
		parentName : null
	},
	methods : {
        load: function() {
            $("#gridTable").resetSelection();
            $("#gridTable").trigger("reloadGrid");
        },
		save : function() {
			dialogOpen({
				title : '新增区域',
				url : 'base/area/add.html?_' + $.now(),
				width : '500px',
				height : '370px',
				success : function(iframeId) {
					// top.frames[iframeId].vm.area.parentCode = vm.parentCode;
					// top.frames[iframeId].vm.area.parentName = vm.parentName;
				},
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit : function() {
            var ck = $('#gridTable').jqGridRowValue('areaId');
            if (checkedRowEx(ck)) {
				dialogOpen({
					title : '编辑区域',
					url : 'base/area/edit.html?id='+ck+'&_' + $.now(),
					width : '500px',
					height : '370px',
					success : function(iframeId) {
						top.frames[iframeId].vm.area.areaId = ck;
						top.frames[iframeId].vm.setForm();
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove : function() {
            var keyValue = $("#gridTable").jqGridRowValue("areaId");
            if (keyValue) {
                var ids=[];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/area/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的记录！', 0);
            }
		}
	},
	created : function() {
		//this.getArea(this.parentCode);
	}
})