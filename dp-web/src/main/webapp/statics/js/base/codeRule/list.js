/**
 * 编号规则表js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
    //resize重设(表格、树形)宽高
    $(window).resize(function (e) {
        window.setTimeout(function () {
            $('#gridTable').setGridWidth(($('.gridPanel').width()));
            $("#gridTable").setGridHeight($(window).height() - 136.5);
        }, 200);
        e.stopPropagation();
    });
}
//加载表格
function getGrid() {
    var selectedRowIndex = 0;
    var gridTable = $('#gridTable');

    gridTable.jqGrid({
		url: '../../basemanage/codeRule/list?_' + $.now(),
        datatype: "json",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        height: $(window).height() - 136.5,
        autowidth: true,
        colModel: [
            { label: '主键', name: 'ruleId', hidden: true },
            { label: '对象编号', name: 'enCode', index: 'enCode', width: 150, align: 'left' },
            { label: '对象名称', name: 'fullName', index: 'fullName', width: 200, align: 'left' },
            {
                label: '规则方式', name: 'mode', index: 'mode', width: 100, align: 'center',
                formatter: function (cellvalue) {
                    if (cellvalue == 1) {
                        return "自动";
                    } else {
                        return "可更改";
                    }
                }
            },
            { label: '系统功能', name: 'moduleName', index: 'moduleName', width: 100, align: 'center' },
            {
                label: '当前流水号', name: 'currentNumber', index: 'currentNumber', width: 150, align: 'left',
                formatter: function (cellvalue) {
                    if (cellvalue) {
                        return cellvalue;
                    } else {
                        return "<span class=\"label label-danger\">未使用</span>";
                    }
                }
            },
            { label: '创建用户', name: 'createUserName', index: 'createUserName', width: 100, align: 'left' },
            {
                label: '创建时间', name: 'createDate', index: 'createDate', width: 130, align: 'left',
                formatter: function (cellvalue, options, rowObject) {
                    return formatDate(cellvalue, 'yyyy-MM-dd hh:mm');
                }
            },
            { label: "说明", name: "description", index: "description", width: 200, align: "left" }
        ],
        viewrecords: true,
        rowNum: 30,
        rowList: [30, 50, 100],
        pager: "#gridPager",
        sortname: 'createDate',
        sortorder: 'desc',
        altRows: true,
        altclass: 'altrow-background',
        rownumbers: true,
        shrinkToFit: false,
        gridview: true,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
        onSelectRow: function () {
            selectedRowIndex = $("#" + this.id).getGridParam('selrow');
        },
        gridComplete: function () {
            $("#" + this.id).resetSelection();
            $("#" + this.id).setSelection(selectedRowIndex, false);
        }
	});
    //查询条件
    $("#queryCondition .dropdown-menu li").click(function () {
        var text = $(this).find('a').html();
        var value = $(this).find('a').attr('data-value');
        $("#queryCondition .dropdown-text").html(text).attr('data-value', value)
    });
    //查询事件
    $("#btn_Search").click(function () {
        var queryJson = {
            condition: $("#queryCondition").find('.dropdown-text').attr('data-value'),
            keyword: $("#txt_Keyword").val()
        }
        $gridTable.jqGrid('setGridParam', {
            postData: { queryJson: JSON.stringify(queryJson) },
            page: 1
        }).trigger('reloadGrid');
    });
    //查询回车
    $('#txt_Keyword').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $('#btn_Search').trigger("click");
        }
    });
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
            $("#gridTable").trigger("reloadGrid");
		},
		save: function() {
			dialogOpen({
                id: "Form",
				title: '新增',
				url: 'base/codeRule/add.html?_' + $.now(),
				width: '800px',
				height: '450px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
            var keyValue = $("#gridTable").jqGridRowValue("ruleId");
            if (checkedRowEx(keyValue)) {
				dialogOpen({
                    id: "Form",
					title: '编辑',
					url: 'base/codeRule/edit.html?keyValue='+keyValue+'&_' + $.now(),
					width: '800px',
					height: '450px',
					success: function(iframeId){
						top.frames[iframeId].vm.codeRule.ruleId =keyValue;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
            var keyValue = $("#gridTable").jqGridRowValue("ruleId");
            if (keyValue) {
                var ids=[];
                ids.push(keyValue);
                $.RemoveForm({
                    url: "../../basemanage/codeRule/remove?_" + $.now(),
                    param: ids ,
                    success: function (data) {
                        vm.load();
                    }
                })
            } else {
                dialogMsg('请选择需要删除的记录！', 0);
            }

		},
		detail:function(){}
	}
})