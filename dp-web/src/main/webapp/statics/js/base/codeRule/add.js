/**
 * 新增-编号规则表js
 */

var keyValue = request('keyValue');
$(function () {
    initControl();
   getGrid();
});
//初始化控件
function initControl() {
    //规则方式
    $("#mode").ComboBox({
        description: "==请选择=="
    });
    //系统功能菜单
    $("#moduleId").ComboBoxTree({
        url: "../../sys/menu/listTree",
        description: "==请选择==",
        height: "240px",
        allowSearch: true
    });
}
//加载表格
function getGrid() {
    var $gridTable = $("#gridTable");
    $gridTable.jqGrid({
        datatype: "local",
        height: 180,
        autowidth: true,
        unwritten: false,
        colModel: [
            { label: "ItemType", name: "ItemType", hidden: true },
            { label: "前缀", name: "ItemTypeName", width: 120, align: "left", sortable: false },
            { label: "格式", name: "FormatStr", width: 120, align: "left", sortable: false },
            { label: "步长", name: "StepValue", width: 100, align: "left", sortable: false },
            { label: "初始值", name: "InitValue", width: 120, align: "left", sortable: false },
            { label: "说明", name: "Description", width: 200, align: "left", sortable: false }
        ],
        caption: "规则设计",
        rowNum: "1000",
        rownumbers: true,
        shrinkToFit: false,
        gridview: true,
        hidegrid: false,
        serializeGridData: function(postdata) {
            return JSON.stringify(postdata);
        },
    });
    //获取表单
    if (!!keyValue) {
        $.SetForm({
            url: "../../basemanage/codeRule/info",
            param: { keyValue: keyValue },
            success: function (data) {
                $("#form").SetWebControls(data);
                $gridTable[0].addJSONData(JSON.parse(data.ruleFormatJson));
            }
        });
    }
}
//添加
function btn_add_field() {
    dialogOpen({
        id: "ItemForm",
        title: '添加',
        url: '/base/codeRule/itemForm.html',
        width: "450px",
        height: "320px",
        yes: function (iframeId) {
            top.frames[iframeId].acceptClick();
        }
    });
};
//修改
function btn_edit_field() {
    var keyValue = $("#gridTable").getGridParam('selrow');
    if (checkedRowEx(keyValue)) {
        dialogOpen({
            id: "ItemForm",
            title: '修改',
            url: '/base/codeRule/itemForm.html?keyValue=' + keyValue,
            width: "450px",
            height: "320px",
            yes: function (iframeId) {
                top.frames[iframeId].acceptClick();
            }
        });
    }
}
//移除
function btn_delete_field() {
    var keyValue = $("#gridTable").getGridParam('selrow');
    if (keyValue) {
        dialogConfirm('注：您确定要移除吗？该操作将无法恢复？', function (r) {
            if (r) {
                $("#gridTable").delRowData(keyValue);
                dialogMsg("移除成功。", 1);
            }
        });

    } else {
        dialogMsg('请选择需要移除的数据！', 0);
    }
}
//保存表单
// function acceptClick() {
//     if (!$('#form').Validform()) {
//         return false;
//     }
//     var postData = $("#form").GetWebControls(keyValue);
//     postData["module"] = $("#moduleId").attr('data-text');
//     postData["ruleFormatJson"] = JSON.stringify($("#gridTable").jqGrid("getRowData"));
//     $.SaveForm({
//         url: "../../basemanage/codeRule/SaveForm?keyValue=" + keyValue,
//         param: postData,
//         loading: "正在保存数据...",
//         success: function () {
//             $.currentIframe().$("#gridTable").trigger("reloadGrid");
//         }
//     })
// }
var vm = new Vue({
	el:'#dpLTE',
	data: {
		codeRule: {
			ruleId: 0
		}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#form").GetWebControls(keyValue);
            postData["moduleName"] = $("#moduleId").attr('data-text');
            postData["ruleFormatJson"] = JSON.stringify($("#gridTable").jqGrid("getRowData"));
		    $.SaveForm({
		    	url: '../../basemanage/codeRule/save?_' + $.now(),
		    	param: postData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
	mounted:function(){
		 initControl();
	}
})
