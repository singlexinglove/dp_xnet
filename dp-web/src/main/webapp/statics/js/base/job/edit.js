/**
 * 编辑-职位管理js
 */

//初始化控件
function initControl() {
    //机构
    $("#organizeId").ComboBoxTree({
        url: "../../basemanage/department/getOrgDeptTree",
        description: "==请选择==",
        height: "100px",
        allowSearch: true,
    });
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		role: {	}
	},
	methods : {
		orgTree: function() {
			dialogOpen({
				id: 'layerOrgTree',
				title: '选择机构',
		        url: 'base/role/org.html?_' + $.now(),
		        scroll : true,
		        width: "300px",
		        height: "450px",
		        yes : function(iframeId) {
		        	top.frames[iframeId].vm.acceptClick();
				}
		    })
		},
		setForm: function() {
			// console.log(vm.role.roleId);
			$.SetForm({
				url: '../../basemanage/role/info?_' + $.now(),
		    	param: vm.role.roleId,
		    	success: function(data) {
					// console.log(data);
                    // vm.role = data;
		    		// $("#OrganizeId").ComboBoxTreeSetValue(data.organizeId);
                    $("#form").SetWebControls(data);
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            // vm.role.organizeId=$("#OrganizeId").attr('data-value');
            var postData = $("#dpLTE").GetWebControls();
		    $.ConfirmForm({
		    	url: '../../basemanage/role/update?_' + $.now(),
		    	param: postData,//vm.role,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
    mounted:function(){
        initControl();
    }
})