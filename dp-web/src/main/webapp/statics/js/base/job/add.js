/**
 * 新增-职位管理js
 */

//初始化控件
function initControl() {

    //机构
    $("#organizeId").ComboBoxTree({
        url: "../../basemanage/department/getOrgDeptTree",
        description: "==请选择==",
        height: "100px",
        allowSearch: true,
    });
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		role: {}
	},
	methods : {
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
            var postData = $("#dpLTE").GetWebControls();
		    // console.log(postData);
            // vm.role.organizeId=$("#OrganizeId").attr('data-value');
            // console.log(vm.role.organizeId);
            postData["category"]=3;
		    $.SaveForm({
		    	url: '../../basemanage/role/save?_' + $.now(),
		    	param: postData,//vm.role,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},

    mounted:function(){
        initControl();
    }
})
