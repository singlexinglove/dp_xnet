package com.xnet.common.constant;

/**
 * 系统级静态变量
 *
 * @author Xingyc
 * @date 2018-1-5 10:07:58
 */
public class SystemConstant {
	
	/**
	 * 超级管理员ID
	 */
	public static final long SUPER_ADMIN = 1;

	public  static final String IS_ADMIN="System";
	
	/**
	 * 数据标识
	 */
	public static final String DATA_ROWS = "rows";
	
	/**
	 * 未授权错误代码
	 */
	public static final int UNAUTHORIZATION_CODE = 401;
	
	/**
	 * 菜单类型
	 *
	 * @author Xingyc
	 * @date 2018-1-5 10:07:58
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        private MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * 定时任务状态
     *
	 * @author Xingyc
	 * @date 2018-1-5 10:07:58
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(1),
        /**
         * 暂停
         */
    	PAUSE(0);

        private int value;

        private ScheduleStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }
    
    /**
     * 通用字典
     *
	 * @author Xingyc
	 * @date 2018-1-5 10:07:58
     */
    public enum MacroType {
    	
    	/**
    	 * 类型
    	 */
    	TYPE(0),
    	
    	/**
    	 * 参数
    	 */
    	PARAM(1);
    	
    	private int value;
    	
    	private MacroType(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    	
    }
    
    /**
     * 通用变量，表示可用、禁用、显示、隐藏
     *
     * @author ZhouChenglin
     * @email yczclcn@163.com
     * @url www.chenlintech.com
     * @date 2017年8月15日 下午7:31:49
     */
    public enum StatusType {
    	
    	/**
    	 * 禁用，隐藏
    	 */
    	DISABLE(0),
    	
    	/**
    	 * 可用，显示
    	 */
    	ENABLE(1),
    	
    	/**
    	 * 显示
    	 */
    	SHOW(1),
    	
    	/**
    	 * 隐藏
    	 */
    	HIDDEN(0);
    	
    	private int value;
    	
    	private StatusType(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    	
    }

	/**
	 * 角色表Category
	 */
	public enum RoleCategoryTypeEnum {

    	ROLE(1),

    	POST(2),

		JOB(3),

		USER_GROUP(4);

		private int value;

		private RoleCategoryTypeEnum(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}

	/**
	 * 权限类别
	 */
	public enum AuthorizeTypeEnum {

		Module(1),

		Button(2),

		Column(3);

		private int value;

		private AuthorizeTypeEnum(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}

	/**
	 * 权限类别
	 */
	public enum AuthorizeCategoryEnum {

		//部门
		Department(1),
        //角色
		Role(2),
        //岗位
		Post(3),
        //职位
		Job(4),
		//用户
		User(5),
        //用户组
		UserGroup(6);

		private int value;

		private AuthorizeCategoryEnum(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}


}
