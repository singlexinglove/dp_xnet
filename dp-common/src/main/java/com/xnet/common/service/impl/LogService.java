package com.xnet.common.service.impl;

import com.xnet.common.manager.ILogManager;
import com.xnet.common.service.ILogService;
import com.xnet.common.utils.CommonUtils;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.entity.LogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 系统日志
 *
 * @author Xingyc
 * @date 2018-1-5 10:07:58
 */
@Service("sysLogService")
public class LogService implements ILogService {

	@Autowired
	private ILogManager ILogManager;
	
	@Override
	public Page<LogEntity> listLog(Map<String, Object> params) {
		Query query = new Query(params);
		Page<LogEntity> page = new Page<>(query);
		ILogManager.listLog(page, query);
		return page;
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = ILogManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	@Override
	public R batchRemoveAll() {
		int count = ILogManager.batchRemoveAll();
		return CommonUtils.msg(count);
	}

}
