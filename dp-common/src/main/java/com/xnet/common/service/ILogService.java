package com.xnet.common.service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.common.entity.LogEntity;

import java.util.Map;

/**
 * 系统日志
 *
 * @author Xingyc
 * @date 2018-1-5 10:13:47
 */
public interface ILogService {

	Page<LogEntity> listLog(Map<String, Object> params);
	
	R batchRemove(Long[] id);
	
	R batchRemoveAll();
	
}
