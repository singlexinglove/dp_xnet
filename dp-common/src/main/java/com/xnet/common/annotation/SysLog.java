package com.xnet.common.annotation;

import java.lang.annotation.*;

/**
 * 系统日志注解
 *
 * @author Xingyc
 * @date 2018-1-5 10:07:58
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

	String value() default "";
}
