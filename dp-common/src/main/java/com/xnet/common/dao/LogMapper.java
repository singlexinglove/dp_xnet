package com.xnet.common.dao;

import com.xnet.common.entity.LogEntity;
import org.mybatis.spring.annotation.MapperScan;

/**
 * 系统日志 
 *
 * @author Xingyc
 * @date 2018-1-5 10:31:24
 */
@MapperScan
public interface LogMapper extends BaseMapper<LogEntity> {

	int batchRemoveAll();
	
}
