package com.xnet.common.aspect;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.manager.ILogManager;
import com.xnet.common.utils.*;
import com.xnet.common.entity.LogEntity;
import com.xnet.common.entity.UserEntity;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * 系统日志，切面处理类
 * 
 * @author Xingyc
 * @date 2017年8月14日 下午8:00:45
 */
@Aspect
@Component
public class LogAspect {
	
	@Autowired
	private ILogManager ILogManager;
	
	@Pointcut("@annotation(com.xnet.common.annotation.SysLog)")
	public void logPointCut() { 
		
	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;
		//保存日志
		saveSysLog(point, time);
		return result;
	}

	private void saveSysLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		LogEntity sysLog = new LogEntity();
		SysLog syslog = method.getAnnotation(SysLog.class);
		if(syslog != null){
			//注解上的描述
//			sysLog.setOperation(syslog.value());
			sysLog.setExecuteResultJson(syslog.value());
		}
		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
//		sysLog.setMethod(className + "." + methodName + "()");
		sysLog.setSourceObjectId(className + "." + methodName + "()");
		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = JSONUtils.beanToJson(args[0]);
//			sysLog.setParams(params);
			sysLog.setSourceContentJson(params);
		}catch (Exception e){

		}
		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		String ipAddress=IPUtils.getIpAddr(request);
		sysLog.setIPAddress(ipAddress);
		sysLog.setBrowser(IPUtils.getOsAndBrowserInfo(request));
		sysLog.setHost(IPUtils.getHostName(ipAddress));
		//用户名
		UserEntity currUser = ShiroUtils.getUserEntity();
		if(CommonUtils.isNullOrEmpty(currUser)) {
			if(CommonUtils.isNullOrEmpty(sysLog.getSourceContentJson())) {
				sysLog.setOperateUserId("");//;UserId(-1L);
//				sysLog.setUsername(sysLog.getParams());
			} else {
				sysLog.setOperateUserId("");
				sysLog.setDescription("获取用户信息为空");
			}
		} else {
			sysLog.setOperateUserId(ShiroUtils.getUserId());
			sysLog.setOperateAccount(ShiroUtils.getUserEntity().getAccount());
		}
		sysLog.setOperateTime(new Date());
		//保存系统日志
		ILogManager.saveLog(sysLog);
	}
	
}
