package com.xnet.common.manager;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.LogEntity;

import java.util.List;

/**
 * 系统日志
 *
 * @author Xingyc
 * @date 2018-1-5 10:07:58
 */
public interface ILogManager {

	void saveLog(LogEntity log);
	
	List<LogEntity> listLog(Page<LogEntity> page, Query query);
	
	int batchRemove(Long[] id);
	
	int batchRemoveAll();
	
}
