package com.xnet.common.manager.impl;

import com.xnet.common.dao.LogMapper;
import com.xnet.common.manager.ILogManager;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.LogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 系统日志
 *
 * @author Xingyc
 * @date 2018-1-5 10:19:34
 */
@Component("logManager")
public class LogManager implements ILogManager {

	@Autowired
	private LogMapper logMapper;
	
	@Override
	public void saveLog(LogEntity entity) {
		entity.setLogId(UUID.randomUUID().toString());
		entity.setOperateTime(new Date());
		logMapper.save(entity);
	}

	@Override
	public List<LogEntity> listLog(Page<LogEntity> page, Query query) {
		return logMapper.listForPage(page, query);
	}

	@Override
	public int batchRemove(Long[] id) {
		return logMapper.batchRemove(id);
	}

	@Override
	public int batchRemoveAll() {
		return logMapper.batchRemoveAll();
	}

}
