package com.xnet.common.controller;

import com.xnet.common.annotation.SysLog;
import com.xnet.common.entity.LogEntity;
import com.xnet.common.entity.Page;
import com.xnet.common.entity.R;
import com.xnet.common.service.ILogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 系统日志
 *
 * @author Xingyc
 * @date 2018-1-5 10:07:58
 */
@RestController
@RequestMapping("/sys/log")
public class LogController extends AbstractController {

	@Autowired
	private ILogService logService;
	
	/**
	 * 日志列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<LogEntity> listLog(@RequestBody Map<String, Object> params) {
		return logService.listLog(params);
	}
	
	/**
	 * 删除日志
	 * @param id
	 * @return
	 */
	@SysLog("删除日志")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return logService.batchRemove(id);
	}
	
	/**
	 * 清空日志
	 * @return
	 */
	@SysLog("清空日志")
	@RequestMapping("/clear")
	public R batchRemoveAll() {
		return logService.batchRemoveAll();
	}
	
}
