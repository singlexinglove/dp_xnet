package com.xnet.common.controller;

import com.xnet.common.entity.UserEntity;
import com.xnet.common.utils.ShiroUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 *
 * @author Xingyc
 * @date 2018-1-5 10:07:58
 */
public abstract class AbstractController {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected UserEntity getUser() {
		return ShiroUtils.getUserEntity();
	}

	//获取用户Id
	protected String getUserId() {
		return getUser().getUserId();
	}

	//获取用户账号名称
	protected String getUserAccount() {
		return getUser().getAccount();
	}
	
}
