package com.xnet.common.entity;

import java.util.List;

/**
 * 通用树形结构实体
 * @author Xingyc
 * @date 2018-1-22 10:53:05
 */
public class TreeEntity {

    public  TreeEntity() {};

    public String getAttribute() {

        return attribute;
    }

    public void setAttribute(String attribute) {

        this.attribute = attribute;
    }

    public String getAttributeA() {

        return attributeA;
    }

    public void setAttributeA(String attributeA) {

        this.attributeA = attributeA;
    }

    public String getAttributeValue() {

        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {

        this.attributeValue = attributeValue;
    }

    public String getAttributeValueA() {

        return attributeValueA;
    }

    public void setAttributeValueA(String attributeValueA) {

        this.attributeValueA = attributeValueA;
    }

    public int getCheckState() {

        return checkState;
    }

    public void setCheckState(int checkState) {

        this.checkState = checkState;
    }

    public boolean isComplete() {

        return complete;
    }

    public void setComplete(boolean complete) {

        this.complete = complete;
    }

    public boolean isDisabled() {

        return disabled;
    }

    public void setDisabled(boolean disabled) {

        this.disabled = disabled;
    }

    public boolean isHasChildren() {

        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {

        this.hasChildren = hasChildren;
    }
    public boolean getHasChildren() {

       return this.hasChildren ;
    }
    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getImg() {

        return img;
    }

    public void setImg(String img) {

        this.img = img;
    }

    public boolean getIsExpand() {

        return isExpand;
    }

    public void setIsExpand(boolean isExpand) {

       this.isExpand = isExpand;
    }

    public int getLevel() {

        return level;
    }

    public void setLevel(int level) {

        this.level = level;
    }

    public String getParentId() {

        return parentId;
    }

    public void setParentId(String parentId) {

        this.parentId = parentId;
    }

    public boolean isShowCheck() {

        return showCheck;
    }

    public void setShowCheck(boolean showCheck) {

        this.showCheck = showCheck;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getValue() {

        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public List<TreeEntity> getChildNodes() {

        return childNodes;
    }

    public void setChildNodes(List<TreeEntity> childNodes) {

        this.childNodes = childNodes;
    }
    public String getParentNodes() {

        return parentNodes;
    }

    public void setParentNodes(String parentNodes) {

        this.parentNodes = parentNodes;
    }

    private String attribute;
    private String attributeA ;
    private String attributeValue ;
    private String attributeValueA ;
    private int  checkState ;
    private boolean complete ;
    private boolean disabled ;
    private boolean hasChildren ;
    private String id ;
    private String img ;
    private boolean isExpand ;
    private int level ;
    private String parentId ;
    private boolean showCheck ;
    private String text ;
    private String title ;
    private String value ;
    private String parentNodes;
    private List<TreeEntity> childNodes;
}
