package com.xnet.common.entity;

public class AuthorizeMember {

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {

        this.userId = userId;
    }

    public String getEnCode() {

        return enCode;
    }

    public void setEnCode(String enCode) {

        this.enCode = enCode;
    }

    public String getAccount() {

        return account;
    }

    public void setAccount(String account) {

        this.account = account;
    }

    public String getRealName() {

        return realName;
    }

    public void setRealName(String realName) {

        this.realName = realName;
    }

    public Integer getGender() {

        return gender;
    }

    public void setGender(Integer gender) {

        this.gender = gender;
    }

    public Integer getIsCheck() {

        return isCheck;
    }

    public void setIsCheck(Integer isCheck) {

        this.isCheck = isCheck;
    }

    public Integer getIsDefault() {

        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {

        this.isDefault = isDefault;
    }

    public String getOrganizeName() {

        return organizeName;
    }

    public void setOrganizeName(String organizeName) {

        this.organizeName = organizeName;
    }


    private String userId;

    private String enCode;

    private String account;

    private String realName;

    private Integer gender;

    private Integer isCheck;

    private Integer isDefault;

    private String organizeName;

    public String getDepartmentId() {

        return departmentId;
    }

    public void setDepartmentId(String departmentId) {

        this.departmentId = departmentId;
    }

    private String departmentId;

    public String getDepartmentName() {

        return departmentName;
    }

    public void setDepartmentName(String departmentName) {

        this.departmentName = departmentName;
    }

    private String departmentName;

}
