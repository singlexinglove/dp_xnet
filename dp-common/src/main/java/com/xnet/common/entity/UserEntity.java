package com.xnet.common.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Date;

/**
 * 用户实体
 *
 * @author Xingyc
 * @date 2018-1-5 09:38:30
 */
public class UserEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //------------字段总数：52
    private  String userId;
    private  String enCode;
    private  String account;
    private  String password;
    private  String secretkey;
    private  String realName;
    private  String nickName;
    private  String headIcon;
    private  String quickQuery;
    private  String simpleSpelling;
    private  Integer gender;
    private  Date birthday;
    private  String mobile;
    private  String telephone;
    private  String email;
    private  String oICQ;
    private  String weChat;
    private  String mSN;
    private  String managerId;
    private  String manager;
    private  String organizeId;
    private  String departmentId;
    private  String roleId;
    private  String jobId;
    private  String jobName;
    private  String postId;
    private  String postName;
    private  String workGroupId;
    private  Integer securityLevel;
    private  Integer userOnLine;
    private  Integer openId;
    private  String question;
    private  String answerQuestion;
    private  Integer checkOnLine;
    private  Date allowStartTime;
    private  Date allowEndTime;
    private  Date lockStartDate;
    private  Date lockEndDate;
    private  Date firstVisit;
    private  Date previousVisit;
    private  Date lastVisit;
    private  Integer logOnCount;
    private  Integer sortCode;
    private  Integer deleteMark;
    private  Integer enabledMark;
    private  String description;
    private  Date createDate;
    private  String createUserId;
    private  String createUserName;
    private  Date modifyDate;
    private  String modifyUserId;
    private  String modifyUserName;
//------------属性列表：52
    /**
     *用户主键
     *@return
     */
    public String getUserId(){

        return userId;
    }

    /**
     *用户主键
     *@param userId
     */
    public void setUserId(String userId){

        this.userId=userId;
    }

    /**
     *用户编码
     *@return
     */
    public String getEnCode(){

        return enCode;
    }

    /**
     *用户编码
     *@param enCode
     */
    public void setEnCode(String enCode){

        this.enCode=enCode;
    }

    /**
     *登录账户
     *@return
     */
    public String getAccount(){

        return account;
    }

    /**
     *登录账户
     *@param account
     */
    public void setAccount(String account){

        this.account=account;
    }

    /**
     *登录密码
     *@return
     */
    public String getPassword(){

        return password;
    }

    /**
     *登录密码
     *@param password
     */
    public void setPassword(String password){

        this.password=password;
    }

    /**
     *密码秘钥
     *@return
     */
    public String getSecretkey(){

        return secretkey;
    }

    /**
     *密码秘钥
     *@param secretkey
     */
    public void setSecretkey(String secretkey){

        this.secretkey=secretkey;
    }

    /**
     *真实姓名
     *@return
     */
    public String getRealName(){

        return realName;
    }

    /**
     *真实姓名
     *@param realName
     */
    public void setRealName(String realName){

        this.realName=realName;
    }

    /**
     *呢称
     *@return
     */
    public String getNickName(){

        return nickName;
    }

    /**
     *呢称
     *@param nickName
     */
    public void setNickName(String nickName){

        this.nickName=nickName;
    }

    /**
     *头像
     *@return
     */
    public String getHeadIcon(){

        return headIcon;
    }

    /**
     *头像
     *@param headIcon
     */
    public void setHeadIcon(String headIcon){

        this.headIcon=headIcon;
    }

    /**
     *快速查询
     *@return
     */
    public String getQuickQuery(){

        return quickQuery;
    }

    /**
     *快速查询
     *@param quickQuery
     */
    public void setQuickQuery(String quickQuery){

        this.quickQuery=quickQuery;
    }

    /**
     *简拼
     *@return
     */
    public String getSimpleSpelling(){

        return simpleSpelling;
    }

    /**
     *简拼
     *@param simpleSpelling
     */
    public void setSimpleSpelling(String simpleSpelling){

        this.simpleSpelling=simpleSpelling;
    }

    /**
     *性别
     *@return
     */
    public Integer getGender(){

        return gender;
    }

    /**
     *性别
     *@param gender
     */
    public void setGender(Integer gender){

        this.gender=gender;
    }

    /**
     *生日
     *@return
     */
    public Date getBirthday(){

        return birthday;
    }

    /**
     *生日
     *@param birthday
     */
    public void setBirthday(Date birthday){

        this.birthday=birthday;
    }

    /**
     *手机
     *@return
     */
    public String getMobile(){

        return mobile;
    }

    /**
     *手机
     *@param mobile
     */
    public void setMobile(String mobile){

        this.mobile=mobile;
    }

    /**
     *电话
     *@return
     */
    public String getTelephone(){

        return telephone;
    }

    /**
     *电话
     *@param telephone
     */
    public void setTelephone(String telephone){

        this.telephone=telephone;
    }

    /**
     *电子邮件
     *@return
     */
    public String getEmail(){

        return email;
    }

    /**
     *电子邮件
     *@param email
     */
    public void setEmail(String email){

        this.email=email;
    }

    /**
     *QQ号
     *@return
     */
    public String getOICQ(){

        return oICQ;
    }

    /**
     *QQ号
     *@param oICQ
     */
    public void setOICQ(String oICQ){

        this.oICQ=oICQ;
    }

    /**
     *微信号
     *@return
     */
    public String getWeChat(){

        return weChat;
    }

    /**
     *微信号
     *@param weChat
     */
    public void setWeChat(String weChat){

        this.weChat=weChat;
    }

    /**
     *MSN
     *@return
     */
    public String getMSN(){

        return mSN;
    }

    /**
     *MSN
     *@param mSN
     */
    public void setMSN(String mSN){

        this.mSN=mSN;
    }

    /**
     *主管主键
     *@return
     */
    public String getManagerId(){

        return managerId;
    }

    /**
     *主管主键
     *@param managerId
     */
    public void setManagerId(String managerId){

        this.managerId=managerId;
    }

    /**
     *主管
     *@return
     */
    public String getManager(){

        return manager;
    }

    /**
     *主管
     *@param manager
     */
    public void setManager(String manager){

        this.manager=manager;
    }

    /**
     *机构主键
     *@return
     */
    public String getOrganizeId(){

        return organizeId;
    }

    /**
     *机构主键
     *@param organizeId
     */
    public void setOrganizeId(String organizeId){

        this.organizeId=organizeId;
    }

    /**
     *部门主键
     *@return
     */
    public String getDepartmentId(){

        return departmentId;
    }

    /**
     *部门主键
     *@param departmentId
     */
    public void setDepartmentId(String departmentId){

        this.departmentId=departmentId;
    }

    /**
     *角色主键
     *@return
     */
    public String getRoleId(){

        return roleId;
    }

    /**
     *角色主键
     *@param roleId
     */
    public void setRoleId(String roleId){

        this.roleId=roleId;
    }

    /**
     *职位主键
     *@return
     */
    public String getJobId(){

        return jobId;
    }

    /**
     *职位主键
     *@param jobId
     */
    public void setJobId(String jobId){

        this.jobId=jobId;
    }

    /**
     *职位名称
     *@return
     */
    public String getJobName(){

        return jobName;
    }

    /**
     *岗位名称
     *@param jobName
     */
    public void setJobName(String jobName){

        this.jobName=jobName;
    }

    /**
     *岗位主键
     *@return
     */
    public String getPostId(){

        return postId;
    }

    /**
     *岗位主键
     *@param postId
     */
    public void setPostId(String postId){

        this.postId=postId;
    }

    /**
     *岗位名称
     *@return
     */
    public String getPostName(){

        return postName;
    }

    /**
     *岗位名称
     *@param postName
     */
    public void setPostName(String postName){

        this.postName=postName;
    }

    /**
     *工作组主键
     *@return
     */
    public String getWorkGroupId(){

        return workGroupId;
    }

    /**
     *工作组主键
     *@param workGroupId
     */
    public void setWorkGroupId(String workGroupId){

        this.workGroupId=workGroupId;
    }

    /**
     *安全级别
     *@return
     */
    public Integer getSecurityLevel(){

        return securityLevel;
    }

    /**
     *安全级别
     *@param securityLevel
     */
    public void setSecurityLevel(Integer securityLevel){

        this.securityLevel=securityLevel;
    }

    /**
     *在线状态
     *@return
     */
    public Integer getUserOnLine(){

        return userOnLine;
    }

    /**
     *在线状态
     *@param userOnLine
     */
    public void setUserOnLine(Integer userOnLine){

        this.userOnLine=userOnLine;
    }

    /**
     *单点登录标识
     *@return
     */
    public Integer getOpenId(){

        return openId;
    }

    /**
     *单点登录标识
     *@param openId
     */
    public void setOpenId(Integer openId){

        this.openId=openId;
    }

    /**
     *密码提示问题
     *@return
     */
    public String getQuestion(){

        return question;
    }

    /**
     *密码提示问题
     *@param question
     */
    public void setQuestion(String question){

        this.question=question;
    }

    /**
     *密码提示答案
     *@return
     */
    public String getAnswerQuestion(){

        return answerQuestion;
    }

    /**
     *密码提示答案
     *@param answerQuestion
     */
    public void setAnswerQuestion(String answerQuestion){

        this.answerQuestion=answerQuestion;
    }

    /**
     *允许多用户同时登录
     *@return
     */
    public Integer getCheckOnLine(){

        return checkOnLine;
    }

    /**
     *允许多用户同时登录
     *@param checkOnLine
     */
    public void setCheckOnLine(Integer checkOnLine){

        this.checkOnLine=checkOnLine;
    }

    /**
     *允许登录时间开始
     *@return
     */
    public Date getAllowStartTime(){

        return allowStartTime;
    }

    /**
     *允许登录时间开始
     *@param allowStartTime
     */
    public void setAllowStartTime(Date allowStartTime){

        this.allowStartTime=allowStartTime;
    }

    /**
     *允许登录时间结束
     *@return
     */
    public Date getAllowEndTime(){

        return allowEndTime;
    }

    /**
     *允许登录时间结束
     *@param allowEndTime
     */
    public void setAllowEndTime(Date allowEndTime){

        this.allowEndTime=allowEndTime;
    }

    /**
     *暂停用户开始日期
     *@return
     */
    public Date getLockStartDate(){

        return lockStartDate;
    }

    /**
     *暂停用户开始日期
     *@param lockStartDate
     */
    public void setLockStartDate(Date lockStartDate){

        this.lockStartDate=lockStartDate;
    }

    /**
     *暂停用户结束日期
     *@return
     */
    public Date getLockEndDate(){

        return lockEndDate;
    }

    /**
     *暂停用户结束日期
     *@param lockEndDate
     */
    public void setLockEndDate(Date lockEndDate){

        this.lockEndDate=lockEndDate;
    }

    /**
     *第一次访问时间
     *@return
     */
    public Date getFirstVisit(){

        return firstVisit;
    }

    /**
     *第一次访问时间
     *@param firstVisit
     */
    public void setFirstVisit(Date firstVisit){

        this.firstVisit=firstVisit;
    }

    /**
     *上一次访问时间
     *@return
     */
    public Date getPreviousVisit(){

        return previousVisit;
    }

    /**
     *上一次访问时间
     *@param previousVisit
     */
    public void setPreviousVisit(Date previousVisit){

        this.previousVisit=previousVisit;
    }

    /**
     *最后访问时间
     *@return
     */
    public Date getLastVisit(){

        return lastVisit;
    }

    /**
     *最后访问时间
     *@param lastVisit
     */
    public void setLastVisit(Date lastVisit){

        this.lastVisit=lastVisit;
    }

    /**
     *登录次数
     *@return
     */
    public Integer getLogOnCount(){

        return logOnCount;
    }

    /**
     *登录次数
     *@param logOnCount
     */
    public void setLogOnCount(Integer logOnCount){

        this.logOnCount=logOnCount;
    }

    /**
     *排序码
     *@return
     */
    public Integer getSortCode(){

        return sortCode;
    }

    /**
     *排序码
     *@param sortCode
     */
    public void setSortCode(Integer sortCode){

        this.sortCode=sortCode;
    }

    /**
     *删除标记
     *@return
     */
    public Integer getDeleteMark(){

        return deleteMark;
    }

    /**
     *删除标记
     *@param deleteMark
     */
    public void setDeleteMark(Integer deleteMark){

        this.deleteMark=deleteMark;
    }

    /**
     *有效标志
     *@return
     */
    public Integer getEnabledMark(){

        return enabledMark;
    }

    /**
     *有效标志
     *@param enabledMark
     */
    public void setEnabledMark(Integer enabledMark){

        this.enabledMark=enabledMark;
    }

    /**
     *备注
     *@return
     */
    public String getDescription(){

        return description;
    }

    /**
     *备注
     *@param description
     */
    public void setDescription(String description){

        this.description=description;
    }

    /**
     *创建日期
     *@return
     */
    public Date getCreateDate(){

        return createDate;
    }

    /**
     *创建日期
     *@param createDate
     */
    public void setCreateDate(Date createDate){

        this.createDate=createDate;
    }

    /**
     *创建用户主键
     *@return
     */
    public String getCreateUserId(){

        return createUserId;
    }

    /**
     *创建用户主键
     *@param createUserId
     */
    public void setCreateUserId(String createUserId){

        this.createUserId=createUserId;
    }

    /**
     *创建用户
     *@return
     */
    public String getCreateUserName(){

        return createUserName;
    }

    /**
     *创建用户
     *@param createUserName
     */
    public void setCreateUserName(String createUserName){

        this.createUserName=createUserName;
    }

    /**
     *修改日期
     *@return
     */
    public Date getModifyDate(){

        return modifyDate;
    }

    /**
     *修改日期
     *@param modifyDate
     */
    public void setModifyDate(Date modifyDate){

        this.modifyDate=modifyDate;
    }

    /**
     *修改用户主键
     *@return
     */
    public String getModifyUserId(){

        return modifyUserId;
    }

    /**
     *修改用户主键
     *@param modifyUserId
     */
    public void setModifyUserId(String modifyUserId){

        this.modifyUserId=modifyUserId;
    }

    /**
     *修改用户
     *@return
     */
    public String getModifyUserName(){

        return modifyUserName;
    }

    /**
     *修改用户
     *@param modifyUserName
     */
    public void setModifyUserName(String modifyUserName){

        this.modifyUserName=modifyUserName;
    }

    /**
     * 角色id列表
     */
    private List<String> roleIdList;

    public List<String> getRoleIdList() {

        return roleIdList;
    }

    public void setRoleIdList(List<String> roleIdList) {

        this.roleIdList = roleIdList;
    }
    /**
     * 机构名称属性
     */
    private String  organizeName;

    public String getOrganizeName() {

        return organizeName;
    }


    public void setOrganizeName(String organizeName) {

        this.organizeName = organizeName;
    }
    /**
     * 部门名称属性
     */
    private String  departmentName;

    public String getDepartmentName() {

        return departmentName;
    }

    public void setDepartmentName(String departmentName) {

        this.departmentName = departmentName;
    }
}
