package com.xnet.common.entity;

import java.util.Date;

/**
 * 实体基类
 */
public class BaseEntity {
    private Date createDate;
    private  String createUserId;
    private  String createUserName;
    private  Date modifyDate;
    private  String modifyUserId;
    private  String modifyUserName;

    public Date getCreateDate() {

        return createDate;
    }

    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }

    public String getCreateUserId() {

        return createUserId;
    }

    public void setCreateUserId(String createUserId) {

        this.createUserId = createUserId;
    }

    public String getCreateUserName() {

        return createUserName;
    }

    public void setCreateUserName(String createUserName) {

        this.createUserName = createUserName;
    }

    public Date getModifyDate() {

        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {

        this.modifyDate = modifyDate;
    }

    public String getModifyUserId() {

        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {

        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {

        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {

        this.modifyUserName = modifyUserName;
    }
}
