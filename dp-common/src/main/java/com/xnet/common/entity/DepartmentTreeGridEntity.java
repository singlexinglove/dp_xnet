package com.xnet.common.entity;

/**
 * 机构部门树形表格实体类
 */

public class DepartmentTreeGridEntity extends TreeGridEntity {
    private String departmentId;
    private String sort ;


    public String getDepartmentId() {

        return departmentId;
    }

    public void setDepartmentId(String departmentId) {

        this.departmentId = departmentId;
    }

    public String getSort() {

        return sort;
    }

    public void setSort(String sort) {

        this.sort = sort;
    }
}
