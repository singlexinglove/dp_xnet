package com.xnet.common.entity;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Date;

/**
 * 日志实体
 *
 * @author Xingyc
 * @date 2018-1-5 09:38:30
 */
public class LogEntity  implements Serializable {

    private static final long serialVersionUID = 1L;

    //------------字段总数：20
    private  String logId;
    private  Integer categoryId;
    private  String sourceObjectId;
    private  String sourceContentJson;
    private  Date operateTime;
    private  String operateUserId;
    private  String operateAccount;
    private  String operateTypeId;
    private  String operateType;
    private  String moduleId;
    private  String module;
    private  String iPAddress;
    private  String iPAddressName;
    private  String host;
    private  String browser;
    private  Integer executeResult;
    private  String executeResultJson;
    private  String description;
    private  Integer deleteMark;
    private  Integer enabledMark;
//------------属性列表：20
    /**
     *日志主键
     *@return
     */
    public String getLogId(){

        return logId;
    }

    /**
     *日志主键
     *@param logId
     */
    public void setLogId(String logId){

        this.logId=logId;
    }

    /**
     *分类Id 1-登陆2-访问3-操作4-异常
     *@return
     */
    public Integer getCategoryId(){

        return categoryId;
    }

    /**
     *分类Id 1-登陆2-访问3-操作4-异常
     *@param categoryId
     */
    public void setCategoryId(Integer categoryId){

        this.categoryId=categoryId;
    }

    /**
     *来源对象主键
     *@return
     */
    public String getSourceObjectId(){

        return sourceObjectId;
    }

    /**
     *来源对象主键
     *@param sourceObjectId
     */
    public void setSourceObjectId(String sourceObjectId){

        this.sourceObjectId=sourceObjectId;
    }

    /**
     *来源日志内容
     *@return
     */
    public String getSourceContentJson(){

        return sourceContentJson;
    }

    /**
     *来源日志内容
     *@param sourceContentJson
     */
    public void setSourceContentJson(String sourceContentJson){

        this.sourceContentJson=sourceContentJson;
    }

    /**
     *操作时间
     *@return
     */
    public Date getOperateTime(){

        return operateTime;
    }

    /**
     *操作时间
     *@param operateTime
     */
    public void setOperateTime(Date operateTime){

        this.operateTime=operateTime;
    }

    /**
     *操作用户Id
     *@return
     */
    public String getOperateUserId(){

        return operateUserId;
    }

    /**
     *操作用户Id
     *@param operateUserId
     */
    public void setOperateUserId(String operateUserId){

        this.operateUserId=operateUserId;
    }

    /**
     *操作用户
     *@return
     */
    public String getOperateAccount(){

        return operateAccount;
    }

    /**
     *操作用户
     *@param operateAccount
     */
    public void setOperateAccount(String operateAccount){

        this.operateAccount=operateAccount;
    }

    /**
     *操作类型Id
     *@return
     */
    public String getOperateTypeId(){

        return operateTypeId;
    }

    /**
     *操作类型Id
     *@param operateTypeId
     */
    public void setOperateTypeId(String operateTypeId){

        this.operateTypeId=operateTypeId;
    }

    /**
     *操作类型
     *@return
     */
    public String getOperateType(){

        return operateType;
    }

    /**
     *操作类型
     *@param operateType
     */
    public void setOperateType(String operateType){

        this.operateType=operateType;
    }

    /**
     *系统功能主键
     *@return
     */
    public String getModuleId(){

        return moduleId;
    }

    /**
     *系统功能主键
     *@param moduleId
     */
    public void setModuleId(String moduleId){

        this.moduleId=moduleId;
    }

    /**
     *系统功能
     *@return
     */
    public String getModule(){

        return module;
    }

    /**
     *系统功能
     *@param module
     */
    public void setModule(String module){

        this.module=module;
    }

    /**
     *IP地址
     *@return
     */
    public String getIPAddress(){

        return iPAddress;
    }

    /**
     *IP地址
     *@param iPAddress
     */
    public void setIPAddress(String iPAddress){

        this.iPAddress=iPAddress;
    }

    /**
     *IP地址所在城市
     *@return
     */
    public String getIPAddressName(){

        return iPAddressName;
    }

    /**
     *IP地址所在城市
     *@param iPAddressName
     */
    public void setIPAddressName(String iPAddressName){

        this.iPAddressName=iPAddressName;
    }

    /**
     *主机
     *@return
     */
    public String getHost(){

        return host;
    }

    /**
     *主机
     *@param host
     */
    public void setHost(String host){

        this.host=host;
    }

    /**
     *浏览器
     *@return
     */
    public String getBrowser(){

        return browser;
    }

    /**
     *浏览器
     *@param browser
     */
    public void setBrowser(String browser){

        this.browser=browser;
    }

    /**
     *执行结果状态
     *@return
     */
    public Integer getExecuteResult(){

        return executeResult;
    }

    /**
     *执行结果状态
     *@param executeResult
     */
    public void setExecuteResult(Integer executeResult){

        this.executeResult=executeResult;
    }

    /**
     *执行结果信息
     *@return
     */
    public String getExecuteResultJson(){

        return executeResultJson;
    }

    /**
     *执行结果信息
     *@param executeResultJson
     */
    public void setExecuteResultJson(String executeResultJson){

        this.executeResultJson=executeResultJson;
    }

    /**
     *备注
     *@return
     */
    public String getDescription(){

        return description;
    }

    /**
     *备注
     *@param description
     */
    public void setDescription(String description){

        this.description=description;
    }

    /**
     *删除标记
     *@return
     */
    public Integer getDeleteMark(){

        return deleteMark;
    }

    /**
     *删除标记
     *@param deleteMark
     */
    public void setDeleteMark(Integer deleteMark){

        this.deleteMark=deleteMark;
    }

    /**
     *有效标志
     *@return
     */
    public Integer getEnabledMark(){

        return enabledMark;
    }

    /**
     *有效标志
     *@param enabledMark
     */
    public void setEnabledMark(Integer enabledMark){

        this.enabledMark=enabledMark;
    }


}
