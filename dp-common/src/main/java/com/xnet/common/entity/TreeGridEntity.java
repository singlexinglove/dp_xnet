package com.xnet.common.entity;
import java.util.List;

/**
 * 通用树形结构实体
 * @author Xingyc
 * @date 2018-3-7 17:59:10
 */
public class TreeGridEntity {

    public  TreeGridEntity() {};

    private String entityJson;
    private boolean expanded;
    private boolean hasChildren ;
    private String id ;
    private String parentId ;
    private String text ;

    public String getEntityJson() {

        return entityJson;
    }

    public void setEntityJson(String entityJson) {

        this.entityJson = entityJson;
    }

    public boolean isExpanded() {

        return expanded;
    }

    public void setExpanded(boolean expanded) {

        this.expanded = expanded;
    }

    public boolean getHasChildren() {

        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {

        this.hasChildren = hasChildren;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getParentId() {

        return parentId;
    }

    public void setParentId(String parentId) {

        this.parentId = parentId;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }
}
