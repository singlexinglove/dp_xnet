package com.xnet.quartz.dao;

import com.xnet.quartz.entity.QuartzJobEntity;
import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.dao.BaseMapper;

/**
 * 定时任务
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月20日 下午11:19:55
 */
@MapperScan
public interface QuartzJobMapper extends BaseMapper<QuartzJobEntity> {

}
