package ${package}.${module}.service.${areaName}.impl;

import java.util.Map;
import java.util.UUID;
import com.xnet.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;
import com.xnet.common.entity.R;
import com.xnet.common.utils.CommonUtils;
import ${package}.${module}.entity.${areaName}.${className}Entity;
import ${package}.${module}.manager.${areaName}.${className}Manager;
import ${package}.${module}.service.${areaName}.${className}Service;

/**
 * ${comments}
 *
 * @author ${author}
 * @email ${email}
 * @url ${url}
 * @date ${datetime}
 */
@Service("${objName}Service")
public class ${className}ServiceImpl  extends BaseServiceImpl<${className}Entity> implements ${className}Service {

	@Autowired
	private ${className}Manager ${objName}Manager;

	@Override
	public Page<${className}Entity> list${className}(Map<String, Object> params) {
		Query query = new Query(params);
		Page<${className}Entity> page = new Page<>(query);
		${objName}Manager.list${className}(page, query);
		return page;
	}

	@Override
	public R save${className}(${className}Entity entity) {
        create(entity);
		int count = ${objName}Manager.save${className}(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R get${className}ById(String id) {
		${className}Entity ${objName} = ${objName}Manager.get${className}ById(id);
		return CommonUtils.msg(${objName});
	}

	@Override
	public R update${className}(${className}Entity entity) {
	    modify(entity);
		int count = ${objName}Manager.update${className}(entity);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(String[] id) {
		int count = ${objName}Manager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}
    /**
     * 创建默认方法
     *
     * @param entity
     */
    @Override
    public void create(${className}Entity entity) {
        super.create(entity);
        entity.setRuleId(UUID.randomUUID().toString());
        entity.setDeleteMark(0);
        entity.setEnabledMark(1);
    }

    /**
     * 修改默认方法
     *
     * @param entity
     */
    @Override
    public void modify(${className}Entity entity) {
        super.modify(entity);
    }
}
