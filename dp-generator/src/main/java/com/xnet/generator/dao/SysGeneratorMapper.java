package com.xnet.generator.dao;

import java.util.List;

import com.xnet.generator.entity.ColumnEntity;
import com.xnet.generator.entity.TableEntity;
import org.mybatis.spring.annotation.MapperScan;

import com.xnet.common.entity.Page;
import com.xnet.common.entity.Query;

/**
 * 代码生成器
 *
 * @author ZhouChenglin
 * @email yczclcn@163.com
 * @url www.chenlintech.com
 * @date 2017年8月28日 下午8:47:12
 */
@MapperScan
public interface SysGeneratorMapper {

	List<TableEntity> listTable(Page<TableEntity> page, Query query);
	
	TableEntity getTableByName(String tableName);
	
	List<ColumnEntity> listColumn(String tableName);
	
}
